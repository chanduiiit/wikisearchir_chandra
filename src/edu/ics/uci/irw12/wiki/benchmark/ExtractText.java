package edu.ics.uci.irw12.wiki.benchmark;

import net.htmlparser.jericho.*;
import java.util.*;
import java.io.*;
import java.net.*;

public class ExtractText {
	static int i = 0;
	
	public void parseHtml(String args) throws Exception {
		String sourceUrlString="data/test.html";
		URL ul=null;
		Properties prop = new Properties();
		String parseInputDir = "";
		String parseOutputDir = "";
		try {
	        //load a properties file
	 		prop.load(new FileInputStream("config.properties"));
	 	} catch (IOException ex) {
	 		System.out.println("Exception loading properties file");
	 		ex.printStackTrace();
	    }
		
		//Error checks
		if (prop.isEmpty()) {
			System.out.println("Properties file is empty or not defined");
			return;
		} else if (!prop.containsKey("html.parse.input.dir") && !prop.containsKey("html.parse.output.dir")){
			System.out.println("parser.input/output.dir is not defined");
			return;
		}
		
		//assign property values.
		parseInputDir = prop.getProperty("html.parse.input.dir");
		parseOutputDir = prop.getProperty("html.parse.output.dir");
		
		if (args == null) {
			sourceUrlString="file:"+sourceUrlString;
		  	System.err.println("Using default argument of \""+sourceUrlString+'"');
		} else {
			sourceUrlString="file:" + parseInputDir + "/" + args + ".html" ;
			//ul = new URL("file:////home/anirudh/git/IR/IR_Wiki_Project/html/"+args+".html");
			//ul = new URL("file:////home/anirudh/git/IR/IR_Wiki_Project/html/Apple_IIc.html");
		}
		
		if (new File(parseOutputDir + "/" +args).exists()) return;
		
		MicrosoftConditionalCommentTagTypes.register();
		PHPTagTypes.register();
		PHPTagTypes.PHP_SHORT.deregister();
		MasonTagTypes.register();
		//Source source=new Source(new URL(sourceUrlString));
		Source source=null;
		try {
			source=new Source(new URL(sourceUrlString));
		}catch (Exception e) {
			BufferedWriter bw = new BufferedWriter(new FileWriter("error"+i+".txt"));
			bw.write(e.getMessage());
			bw.close();
			//System.out.println(e.getMessage());
			//e.printStackTrace();
		} if(source!=null) {
			source.fullSequentialParse();
			FileWriter fstream = null;
			try {
				//Add this to config props
				fstream = new FileWriter(parseOutputDir + "/" +args);
			} catch (Exception e) {
				// TODO: handle exception
				fstream = new FileWriter("out.txt");
			}
		
			TextExtractor te = new TextExtractor(source);
			te.excludeElement(source.getFirstStartTag("a"));
			te.writeTo(fstream);
		}
  }

}