package edu.ics.uci.irw12.wiki.benchmark;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.CharsetDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

class Page{
	String title;
	String link;
	boolean top10=false;
	int relevance;
}

public class Extracter {
	FileInputStream fis = null;
	DataInputStream dis = null;
	InputStreamReader isr = null;
	HashMap <String, HashMap<String,Page>> hm = new HashMap<String, HashMap<String,Page>>();
	void initialize() {
		;
	}
	@SuppressWarnings("deprecation")
	public HashMap<String,HashMap<String, Page>> iterateFiles() {
		try {
			int relavanceMatrix[] = {6,5,4,3,2,1,1,1,1,1,
									 0,0,0,0,0,0,0,0,0,0,
									 0,0,0,0,0,0,0,0,0,0};
			Properties prop = new Properties();
			try {
		        //load a properties file
		 		prop.load(new FileInputStream("config.properties"));
		 	} catch (IOException ex) {
		 		System.out.println("Exception loading properties file");
		 		ex.printStackTrace();
		    }
			
			//Error checks
			if (prop.isEmpty()) {
				System.out.println("Properties file is empty or not defined");
				return null;
			} else if (!prop.containsKey("google.query.results.dir")){
				System.out.println("google.query.results.dir is not defined");
				return null;
			}
			for(int i=1;i<31;i++) {
				
				fis=new FileInputStream(prop.getProperty("google.query.results.dir")+"/out"+i+".txt");
				
				/*isr = new InputStreamReader(fis, "UTF-8");
				BufferedReader br = new BufferedReader(isr);*/
				
				dis = new DataInputStream(fis);
				String link=null,query=null;
				int j=0,n=0,in=-1;
				HashMap<String,Page> temp = new HashMap<String,Page>();
				boolean t=false,l=false;
				Page tempPage=null;
				
				//while(( (link=br.readLine())!=null ) && (temp.size()<30)){
				while(( (link=dis.readLine())!=null ) && (temp.size()<30)){
					if(!t&&!l)
						tempPage=new Page();
					if(n==0) {
						query=link.toLowerCase();
						//System.out.println("storing "+query);
					} else {
						/*in=-1;
						if( (in = (link.indexOf("http://"))) ==-1) {
							t=true;
							tempPage.title=link.substring(0, in).trim();
							System.out.println(" index is "+in + " string is "+link.substring(0, in).trim() + " title being stored is"+tempPage.title);
							
						}*/	
						if((link.indexOf("http"))!=-1) {
							tempPage.relevance=relavanceMatrix[j];
							l=true;
							tempPage.link=link;
							in=link.indexOf("/wiki/");
							tempPage.title=link.substring(in+6).replaceAll("/", " ").trim();
							tempPage.title=tempPage.title.replaceAll("_", " ");
							t=true;
							if(j<10)
								tempPage.top10=true;
							else
								tempPage.top10=false;
							System.out.println("link "+ link+" has number "+j +" and title "+tempPage.title);
						}
						if(t&&l) {
							//System.out.println("putting "+(j));
							//System.out.println("link inside is "+link+" title is "+tempPage.title + " rel is "+tempPage.relevance);
							temp.put(tempPage.title, tempPage);
							//System.out.println("Obj is "+temp.get(link).title);
							t=l=false;
							j++;
						}
					}
					n++;
				}
				hm.put(query, temp);
				dis.close();
				fis.close();
			}
			
			//System.out.println("Iterating waste\n\n\n");
			Iterator<String> it = hm.keySet().iterator();
			int k=0;
			String pageElement=null;
			while(it.hasNext()) {
				pageElement = it.next();
				//System.out.println("Element"+k+++" :"+pageElement);
				HashMap <String,Page> hmm = hm.get(pageElement);
				//Iterating on links
				Iterator<String> et = hmm.keySet().iterator();
				//System.out.println("DNA ?"+hm.get("DnA".toLowerCase()));
				//Iterator<Integer> et = hm.get("DNA").values().iterator();
				String link=null;
				while(et.hasNext()) {
					link = et.next();
					Page tt= hmm.get(link);
					//System.out.println("Link "+link+" Number is"+tt.relevance);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hm;
	}
	
	/*public static void main(String args[]) {
		Extracter et = new Extracter();
		et.iterateFiles();
	}*/
}
