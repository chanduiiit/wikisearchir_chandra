package edu.ics.uci.irw12.wiki.benchmark;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.jsoup.Jsoup;
import edu.ics.uci.irw12.wiki.benchmark.ExtractText;

public class GetWikiPages {
	BufferedWriter outError = null;
	File f = new File("./html/error.txt");
	static int i=0;
    
	@SuppressWarnings("static-access")
	public void getPage(String title, String pageName) throws IOException {
		//Fetch properties
		Properties prop = new Properties();
		String wikiDownloadDir = "";
		try {
	        //load a properties file
	 		prop.load(new FileInputStream("config.properties"));
	 	} catch (IOException ex) {
	 		System.out.println("Exception loading properties file");
	 		ex.printStackTrace();
	    }
		
		//Error checks
		if (prop.isEmpty()) {
			System.out.println("Properties file is empty or not defined");
			return;
		} else if (!prop.containsKey("html.parse.input.dir")){
			System.out.println("parser.input.dir is not defined");
			return;
		}
		
		wikiDownloadDir = prop.getProperty("html.parse.input.dir");
		
		File f = new File(wikiDownloadDir + "/" + title + ".html");
		if(f.exists())
			;
		else {
			try {
				Thread.currentThread().sleep(3000);
				//System.out.println("about to page parse "+pageName);
				String html = Jsoup.connect(pageName).get().html();
				//System.out.println("page parsed "+pageName);
				BufferedWriter out = new BufferedWriter(new FileWriter(wikiDownloadDir + "/" + title + ".html"));
				//System.out.println(" Here inside "+pageName+" title is "+title);
				out.write(html);
			    out.close();
			} catch (Exception e) {
				outError= new BufferedWriter(new FileWriter("error"+i+".txt"));
				outError.write(title);
				outError.close();
				i++;
				e.printStackTrace();
			}
		}
	}
	
	public void writePage(String title) throws Exception {
		//Called from benchmark fetchWikiBulk

		ExtractText et = new ExtractText();
		et.parseHtml(title);
	}
}
