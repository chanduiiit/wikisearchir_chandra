package edu.ics.uci.irw12.wiki.benchmark;

import java.util.HashMap;
import java.util.HashMap;
import java.util.Vector;

import edu.ics.uci.irw12.wiki.benchmark.Page;

//NDCG and MAP per query

public class NDCGAndMAP {

	//HashMap is for google results
	HashMap<String, Page> hm=null;
	//Vector is to store our search results
	Vector<String> sv=null;

	// performance might increase if instead of instantiating objects of ndcpmap we use static class and use only reference passing
	// Vector stores our result set and HashMap hm stores <link, pageObject>
	
	public NDCGAndMAP(Vector<String> sv, HashMap<String,Page> hm){
		this.hm=hm;
		this.sv=sv;
		//System.out.println("Initialized "+hm +"  "+sv);
	}

	private double averagePrecision() {
		double precision = 0;
		double number_relevant_matches=0;
		// calculates the average precision
		for (int i=0;i<10;i++){
			//sv.get(i) fetches ith link our search is showing and i'm checking against hm if it exists
			if(hm==null)
				System.out.println("HM NUll");
			if(sv==null)
				System.out.println("SV NULL");
			System.out.println("SC is null "+sv.get(i));
			if(hm.containsKey(sv.get(i)) && hm.get((sv.get(i))).top10==true)
			{
				number_relevant_matches++;
				precision = precision + (((double) number_relevant_matches)/(i+1));
				System.out.println(" precision "+sv.get(i));
			}			
		}			
		if(number_relevant_matches==0)
			return 0;
		else
			return precision/number_relevant_matches;
	}

	private double ndcgAt5() {
		double ndcg = 0, dcg=0;
		Integer value=0;
		double []log = {1,1,1.5849625,2,2.321928};
		for (int i=0;i<5;i++){
			Page p = hm.get(sv.get(i));
			if(p==null)
				continue;
			value = p.relevance;
			if(value!=null){
				dcg = dcg + (((double)value)/log[i]);
			}
		}
		//15.877792236 is Ideal DCG obtained from Google values {6,5,4,3,2}
		ndcg = dcg/(15.877792236);
		return ndcg;
	}

	public double[] computeNDCGAndMAP() {
		double average_precision=0, ndcg=0;
		double retValue[] = new double[2];
			try {
				average_precision=average_precision+this.averagePrecision();
				ndcg=this.ndcgAt5();
			} catch (Exception e) {
				e.printStackTrace();
			}
		retValue[0]=ndcg;
		retValue[1]=average_precision;
		return retValue;
	}
}