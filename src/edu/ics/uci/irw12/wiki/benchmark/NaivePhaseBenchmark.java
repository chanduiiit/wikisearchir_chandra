package edu.ics.uci.irw12.wiki.benchmark;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import edu.ics.uci.irw12.wiki.benchmark.Extracter;
import edu.ics.uci.irw12.wiki.benchmark.NDCGAndMAP;
import edu.ics.uci.irw12.wiki.indexer.impl.NaivePhaseIndexer;
import edu.ics.uci.irw12.wiki.search.NaiveBenchmarkSearchIndex;

public class NaivePhaseBenchmark {
	static Extracter et = new Extracter();
	static HashMap<String,HashMap<String, Page>> hm = new HashMap<String,HashMap<String, Page>>();
	
	private static void fetchWikiBulk(HashMap<String,HashMap<String, Page>> hm ){
		Iterator<String> it = hm.keySet().iterator();
		int k=0;
		GetWikiPages gwp = new GetWikiPages();
		String pageElement=null;
		try {
			while(it.hasNext()) {
				pageElement = it.next();
				//System.out.println("Element"+k+++" :"+pageElement);
				HashMap <String,Page> hmm = hm.get(pageElement);
				//Iterating on links
				Iterator<String> et = hmm.keySet().iterator();
				String link=null;
				while(et.hasNext()) {
					link = et.next();
					Page tt= hmm.get(link);
					//System.out.println("Link to be fetched is "+link);
					gwp.getPage(tt.title, link);
					//System.out.println("Fetched from wiki");
					gwp.writePage(tt.title);
					//System.out.println("Written onto disk");
					//System.out.println("Link "+link+" :"+tt.title + " Number is"+tt.relevance);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void readyPages(){
		hm=et.iterateFiles();
		//ANIRUDH COMMENT: NOT NEEDED TO BE FETCHED EVERY TIME ONCE IS SUFFICIENT
		NaivePhaseBenchmark.fetchWikiBulk(hm);
		System.out.println("Got all pages. Init done");
	}
	
	public static void setupBenchmark() throws Exception {
		Logger logger = Logger.getLogger("edu.ics.uci.irw12.wiki.bechmark.Benchmark.class");
		NaivePhaseBenchmark.readyPages();
		NaivePhaseIndexer indexer = new NaivePhaseIndexer();
		//logger.info("NaivePhaseIndexer created");
		long start=System.currentTimeMillis();
		indexer.createIndex(logger);
		long end=System.currentTimeMillis();
		//logger.info("Time needed to index is - "+(end-start)+" milliseconds");
		System.out.println("Time needed to index is - "+(end-start)+" milliseconds");
	}
	
	public static void main(String args[]) throws Exception {
		NaivePhaseBenchmark.setupBenchmark();
		//Commented on 8:11 pm 15th feb to test why it is being called in readypages also
		//Seems like we don't need this
		//hm=et.iterateFiles();
		
		
		NaiveBenchmarkSearchIndex bsi = new NaiveBenchmarkSearchIndex();
		PropertyConfigurator.configure("log4j.properties");
		Logger logger = Logger.getLogger("edu.ics.uci.irw12.wiki");
		if(hm!=null)
			;//System.out.println("HM is "+hm);
		else 
			System.out.println("NULL HM");
		//This needs to be removed
		Vector<String> sv = new Vector<String>();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("list");
			DataInputStream dis = new DataInputStream(fis);
			String s=null;
			//THIS IS FOR RUNNING QUERY LIST ON SEARCH AND GETTING SEARCH LIST
			while((s=dis.readLine())!=null) {
				sv.add(s);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fis.close();
		}
		int counter=0;
		int size=sv.size();
		double []namResult=new double[2];
		double mean_average_precision=0,mean_ndcg=0;
		NDCGAndMAP nam = null;
		
		while(counter<size){
			try{
				String query = sv.get(counter);
				System.out.println("Query is "+query);
				if(bsi!=null)
					;//System.out.println("Search result is "+bsi.searchIndex(query, logger));
				else
					System.out.println("BSI Is null");
				nam = new NDCGAndMAP(bsi.searchIndex(query, logger), hm.get(query.toLowerCase()));
				namResult = nam.computeNDCGAndMAP();
				
				mean_ndcg+=namResult[0];
				
				mean_average_precision+=namResult[1];
				System.out.println("Individual ndcg is "+namResult[0]);
				
				//+ " Individual AP is "+namResult[1]);
			} catch (Exception e) {
				e.printStackTrace();
			}
			counter++;
		}
		mean_ndcg/=size;
		mean_average_precision/=size;
		System.out.println("Final MAP is "+mean_average_precision +" and Average NDCG is "+mean_ndcg);

		
		
	}

}
