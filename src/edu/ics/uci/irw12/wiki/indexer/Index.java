package edu.ics.uci.irw12.wiki.indexer;

import org.apache.log4j.Logger;

public interface Index {
	
	void createIndex(Logger logger) throws Exception;

}
