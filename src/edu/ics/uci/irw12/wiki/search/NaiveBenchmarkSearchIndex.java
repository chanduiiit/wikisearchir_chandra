package edu.ics.uci.irw12.wiki.search;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Vector;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.FileWriter;
import org.apache.log4j.*;

import edu.ics.uci.irw12.wiki.benchmark.NDCGAndMAP;


public class NaiveBenchmarkSearchIndex {

	public NaiveBenchmarkSearchIndex() {
		// TODO Auto-generated constructor stub
	}
	
	private Vector<String> googleComparisonList=null;
	//private double mean_precision=0,mean_ndcg=0;
	
    @SuppressWarnings("unused")
	public Vector<String> searchIndex(String qTxt, Logger logger) throws Exception {
    	googleComparisonList=new Vector<String>();
	    String index = "index";
	    String field = "contents";
	    String queries = null;
	    int repeat = 0;
	    boolean raw = false;
	    String queryString = null;
	    int hitsPerPage = 30;
	    	    
	    IndexReader reader = IndexReader.open(FSDirectory.open(new File(index)));
	    IndexSearcher searcher = new IndexSearcher(reader);
	    Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_35);

	    BufferedReader in = null;
	    if (queries != null) {
	      in = new BufferedReader(new InputStreamReader(new FileInputStream(queries), "UTF-8"));
	    } else {
	      in = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
	    }
	    QueryParser parser = new QueryParser(Version.LUCENE_35, field, analyzer);
	   	try {
	    	Query query = parser.parse(qTxt);
			//System.out.println("Searching for: " + query.toString());				        
			/*if (repeat > 0) {                           // repeat & time as benchmark
					Date start = new Date();
					for (int i = 0; i < repeat; i++) {
						searcher.search(query, null, 100);
					}
				Date end = new Date();
				System.out.println("Time: "+(end.getTime()-start.getTime())+"ms");
			}*/
			this.doUnpagedSearch(in, searcher, query, hitsPerPage, raw, queries == null && queryString == null,googleComparisonList);
	    }
	    catch (Exception e) {
	    	logger.error("There seems to be an error. Please query again");
	    	logger.info(e.getMessage());
	    	logger.trace(e.getStackTrace());
	    }
	   searcher.close();
	   reader.close();
	   return this.googleComparisonList;
	 }

	  private void doUnpagedSearch(BufferedReader in, IndexSearcher searcher, Query query, int hitsPerPage, boolean raw, boolean interactive, Vector<String> googleComparisonList) throws IOException {
	    // Collect enough docs to show 30 results
	    TopDocs results = searcher.search(query, 1 * hitsPerPage);
	    ScoreDoc[] hits = results.scoreDocs;
	    int numTotalHits = results.totalHits;
	    //System.out.println(numTotalHits + " total matching documents");
	    //System.out.println("Printing top 30 with NDCG @ and its MAP score");
	    hits = searcher.search(query, numTotalHits).scoreDocs;
	    File ff=new File("pp");
	    FileWriter fw = new FileWriter(ff);
	    double ndcg_map[] = new double[2];
	    for (int i = 0; i < 30; i++) {    
	    	Document doc = searcher.doc(hits[i].doc);
	    	String title = doc.get("title");
	        if (title != null) {
	        	String temp = doc.get("title");
	        	System.out.println("Title: " + temp);
	        	googleComparisonList.add("http://en.wikipedia.org/wiki/"+temp);
	        }
	    } 
	    fw.close();
	 }	

	
}
