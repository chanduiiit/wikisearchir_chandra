package edu.ics.uci.irw12.wiki.xml.impl;

import com.ximpleware.*;

import edu.ics.uci.irw12.wiki.xml.Parser;

import java.io.File;
import java.io.FileWriter;
import info.bliki.wiki.filter.PlainTextConverter;
import info.bliki.wiki.model.WikiModel;
import org.apache.log4j.*;

// xpath without name space
public class VtdXMLParser implements Parser
{
	public void parseXML(Logger logger) throws Exception{
            VTDGen vg = new VTDGen();
            vg.selectLcDepth(5);
            AutoPilot ap = new AutoPilot();
            AutoPilot ap1 = new AutoPilot();
            AutoPilot ap2 = new AutoPilot();
		FileWriter fw = null;
            	int i;
		long start = System.currentTimeMillis();
		long middle=0;
            	ap.selectXPath("/mediawiki/page/text()");
		ap1.selectXPath("title");
		ap2.selectXPath("revision/text");
		WikiModel wikiModel = new WikiModel("http://www.mywiki.com/wiki/${image}","http://www.mywiki.com/wiki/${title}");
		PlainTextConverter ptc = new PlainTextConverter();
	try {
            if (vg.parseFile("wiki.xml",true)){
                VTDNav vn = vg.getNav();
                ap.bind(vn); // apply XPath to the VTDNav instance, you can associate ap to different vns
                // AutoPilot moves the cursor for you, as it returns the index value of the evaluated node
		ap1.bind(vn);
		ap2.bind(vn);
		ap.selectElementNS("*","page");
		int count = 0;
		File f = null;
		String sbt = null;
		String sbtt = null;
		long fileCounter =0;
		long counter =0;
		middle=System.currentTimeMillis();	
		boolean nextFile=true;
                while(ap.iterate()){
			//System.out.print(""+vn.getCurrentIndex()+"  ");     
			//System.out.print("Element name ==> "+vn.toString(vn.getCurrentIndex()));
			//int t = vn.getText(); // get the index of the text (char data or CDATA)
			//if (t!=-1)
		  		//System.out.println(" Text  ==> "+vn.toNormalizedString(t));
			//System.out.println("\n ============================== ");
			sbt =  ap1.evalXPathToString();
			sbtt =  ap2.evalXPathToString();
			f = new File("parsed/",sbt.replace("/",""));
			/*if(counter>10000) {
				fw.close();
				f=null;
				nextFile=true;
			}*/
			//if(nextFile) {
			//	f = new File("parsed/output"+fileCounter);
			//	fileCounter++;
				fw = new FileWriter(f);
			//	counter-=10000;
			//	nextFile=false;	
			//}
			fw.write(sbt);
			//fw.write(sbtt);
			fw.write(wikiModel.render(ptc,sbtt));
			//System.out.println("Title is " + ap1.evalXPathToString());
			//System.out.println("Text is " + ap2.evalXPathToString());
			//counter++;
                }
		
	    }
	} catch (Exception e) {
		e.printStackTrace();
	}
		
		long end = System.currentTimeMillis();
		try {
		fw = new FileWriter("time");
		System.out.println("Parse load time = "+(middle-start));
		System.out.println("Total time = "+(end-start));
		String r = "total "+(end-start);
		String m = "parse load time "+(middle-start);
		fw.write(r);
		fw.write(m);
		fw.close();
	 	} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
