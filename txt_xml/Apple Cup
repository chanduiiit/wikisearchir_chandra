title

The Apple Cup 
Number of meetings 104  
First meeting UW 5, WSU 5 (1900) 
Last meeting UW 38, WSU 21 (2011)  
Next meeting November 23, 2012  
All-time series UW leads 67–31–6   
Largest victory UW, 51-3 (2000)  
Current streak UW, 3 (2009–2011)  
Longest UW win streak 8 (1959–66 &#38; 1974–81)  
Longest WSU win streak 2 (8 times)  
Trophy The Apple Cup 
The Apple Cup is the trophy awarded to the winner of an American college football rivalry game played annually by the teams of the two largest universities in the U.S. state of Washington: the University of Washington (UW) Huskies and the Washington State University (WSU) Cougars.  It is traditionally the final game of the regular season and takes place on the Saturday preceding Thanksgiving at Husky Stadium in Seattle during odd years, and WSU's Martin Stadium in Pullman during even years, but with college football season extending, the game is often played at a later date.
Series historyFrom 1950 to 1980 (except for 1954), the WSU home games in the series were played at Spokane's Joe Albi Stadium. The WSU Cougars won three of these fifteen games in Spokane (1958, 1968, 1972), and have won six of the fourteen played at Martin Stadium (1982, 1988, 1992, 1994, 2004, 2008).Before 1962, the teams played for the "Governor's Trophy." The game was renamed the "Apple Cup" in 1962 because of Washington's famous apple crop.  The Apple Cup trophy has been awarded to the winning team ever since.With the lengthening of the college football regular season schedule to 12 games in 2006, there was a movement to change the date of the Apple Cup from the weekend before Thanksgiving to the weekend following.  This would have allowed a bye week during the season. In the 2006 season, both teams played 12 straight weeks without a break, leaving the two teams noticeably fatigued.  For the first time, the 2007 edition of the Apple Cup was played two days after Thanksgiving.The Apple Cup has been sponsored by Boeing since 2007."Boeing To Present Apple Cup." Gohuskies.com. September 6, 2007.  The first rivalry game was held in 1900 and resulted in a tie between UW and WSU.  From 1900 to 2011, there have been 104 games between the schools. The Huskies were the home team 58 times (all in Seattle). The Cougars were the home team 46 times (31 in Pullman, 15 in Spokane). The Huskies hold a 67–31–6 advantage. The UW's longest winning streak has been eight games, achieved twice: (1959–66 and 1974–81). WSU has never won more than two consecutive Apple Cups, but the Cougars have won back-to-back games on eight separate occasions: (1929–30, 1953–54, 1957–58, 1967–68, 1972–73, 1982–83, 2004–05 and 2007–08).Washington currently holds the trophy for three straight years. In 2009, the Cougars were shut out 30–0 at Husky Stadium, UW's first shutout since 1964. In 2010, the Huskies defeated the Cougars 35-28.  The 104th and most recent Apple Cup on November 26, 2011 was won 38-21 by UW, playing at CenturyLink Field in Seattle due to renovations at Husky Stadium.
Game results

56 won by the home team*  42 won by the visiting team  6 tied games 
UW*38 inSeattle  WSU*14 inPullman  WSU*3 inSpokane WSU14 inSeattle  UW16 inPullman  UW12 inSpokane Tie5 inSeattle Tie1 inPullman Tie0 inSpokane 
38 home*  17 home*  14 away  28 away  9 years with No game 
31 WSU victories  1 year (1945) with two games 
67 UW victories  104 games in 112 years (1900–2011)  

Annual resultsEach entry includes the winning school (* if at home), score, location, and attendance.

11/30/00Tie5–5Seattle1,500 11/01/01WSU*10–0Pullman500 11/27/02UW*16–0Seattle1,000 10/30/03UW10–0Pullman700 10/29/04UW*12–6Seattle1,000 1905Nogame 1906Nogame 11/21/07WSU11–5Seattle3,000 11/07/08Tie6–6Seattle4,000 1909Nogame SeriesUW:3WSU:2Tie:2 
11/12/10UW*16–0SeattlePossibly Spokane2,800 11/30/11UW*30–6Seattle6,000 11/28/12UW*19–0Seattle7,000 11/27/13UW*20–0Seattle6,000 11/26/14UW*45–0Seattle6,000 1915Nogame 1916Nogame 11/29/17WSU14–0Seattle6,000 1918Nogame 11/15/19UW13–7Pullman3,000 Series:UW:9WSU:3Tie:2 
1920Nogame 11/24/21WSU*14–0PullmanPossibly Seattle13,263 10/28/22UW16–13Pullman8,800 11/24/23UW*24–7Seattle13,059 11/22/24UW*14–0Seattle8,978 10/31/25UW23–0Pullman2,500 10/23/26WSU9–6Seattle24,486 10/22/27UW*14–0Seattle35,000 11/29/28UW*6–0Seattle22,437 10/19/29WSU*20–13Pullman16,000 Series:UW:15WSU:6Tie:2 
11/15/30WSU3–0Seattle42,000 11/14/31UW*12–0Seattle20,000 11/12/32Tie0–0Seattle17,333 11/25/33WSU*17–6Pullman10,994 11/24/34Tie0–0Seattle32,876 10/14/35UW21–0Pullman11,900 11/26/36UW*40–0Seattle40,735 10/16/37Tie7–7Pullman14,581 11/26/38UW*26–0Seattle25,356 10/14/39WSU*6–0Pullman18,552 Series:UW:19WSU:9Tie:5 
11/30/40UW*33–9Seattle25,000 10/11/41UW23–13Pullman20,000 11/28/42Tie0–0Seattle13,000 19431944Nogame 10/13/45UW*6–0Seattle38,000 11/24/45WSU*7–0Pullman15,000 10/12/46UW21–7Pullman26,000 11/22/47UW*20–0Seattle31,500 10/16/48WSU*10–0Pullman25,000 11/19/49UW*34–21Seattle35,676 Series:UW:25WSU:11Tie:6 
11/25/50UW52–21Spokane28,433 11/24/51WSU27–25Possibly 25-2Seattle51,221 11/29/52UW33–27Spokane28,000 11/21/53WSU25–20Seattle39,534 11/20/54WSU*26–7Pullman15,000 11/19/55UW*27–7Seattle33,023 11/24/56UW40–26Spokane20,600 11/23/57WSU27–7Seattle47,352 11/22/58WSU*18–14Spokane24,051 11/21/59UW*20–0Seattle55,782 SeriesUW:30WSU:16Tie:6 
11/19/60UW8–7Spokane28,760 11/25/61UW*21–17Seattle49,676 11/24/62UW26–21Spokane35,494 11/30/63UW*16–0Seattle57,300 11/21/64UW14–0Spokane33,635 11/20/65UW*27–9Seattle57,395 11/19/66UW19–7Spokane33,800 11/25/67WSU9–7Seattle49,041 11/23/68WSU*24–0Spokane31,986 11/22/69UW*30–21Seattle55,677 Series:UW:38WSU:18Tie:6 
11/21/70UW43–25Spokane33,200 11/20/71UW*28–20Seattle60,497 11/18/72WSU*27–10Spokane34,100 11/24/73WSU52–26Seattle56,500 11/23/74UW24–17Spokane27,800 11/22/75UW*28–27Seattle57,100 11/20/76UW51–32Spokane35,800 11/19/77UW*35–15Seattle60,964 11/25/78UW38–8Spokane35,187 11/17/79UW*17–7Seattle56,110 Series:UW:46WSU:20Tie:6 
11/22/80UW30–23Spokane34,577 11/21/81UW*23–10Seattle60,052 11/20/82WSU*24–20Pullman36,571 11/29/83WSU17–6Seattle59,220 11/17/84UW38–29Pullman40,000 11/23/85WSU21–20Seattle59,887 11/22/86UW44–23Pullman40,000 11/21/87UW*34–19Seattle74,038 11/19/88WSU*32–31Pullman40,000 11/18/89UW*20–9Seattle73,527 Series:UW:52WSU:24Tie:6 
11/17/90UW55–10Possibly 55-19Pullman37,600 11/23/91UW*56–21Seattle72,581 11/21/92WSU*42–23Pullman37,600 11/20/93UW*26–3Seattle72,688 11/19/94WSU*23–6Pullman37,600 11/18/95UW*33–30Seattle74,144 11/23/96UW31–24Pullman37,600 11/22/97WSU41–35Seattle74,268 11/21/98UW16–9Pullman37,251 11/20/99UW*24–14Seattle72,973 Series:UW:59WSU:27Tie:6 
11/18/00UW51–3Pullman33,010 11/17/01UW*26–14Seattle74,442 11/23/02UW29–26Pullman37,600 11/22/03UW*27–19Seattle74,549 11/20/04WSU*28–25Pullman34,334 11/19/05WSU26–22Seattle70,713 11/18/06UW35–32Pullman35,117 11/24/07WSU42–35Seattle72,888 11/22/08WSU*16–13Pullman32,211 11/28/09UW*30–0Seattle68,697 Series:UW:65WSU:31Tie:6 
12/04/10UW35–28Pullman30,157 11/26/11UW*38-21Seattle64,559 11/23/12  Pullman 2013  Seattle 2014  Pullman 2015  Seattle 2016  Pullman 2017  Seattle 2018  Pullman 2019  Seattle Series:UW:67WSU:31Tie:6  

Other sportsThe annual softball game in Washington, D.C., between the teams affiliated with the Washington State GOP and the Washington State Democrats is also commonly referred to as the "Apple Cup."  The teams compete in the House of Representatives softball league, and the GOP team has won the Apple Cup for the past 3 years.
References{{Reflist}}
External links

University of Washington Huskies Football
Washington State University Cougars Football

{{Washington Huskies football navbox}}
{{Washington State Cougars football navbox}}
{{Pacific-12 Conference football rivalry navbox}}


zh:蘋果盃