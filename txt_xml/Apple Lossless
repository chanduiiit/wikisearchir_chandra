title{{Infobox software}}
{{Infobox file format}}
Apple Lossless (also known as ALAC (Apple Lossless Audio Codec), or ALE (Apple Lossless Encoder) is an audio codec developed by Apple Inc. for lossless data compression of digital music. After initially being proprietary for many years, in late 2011 Apple open sourced and royalty-freed the Apple Lossless codec. It is worth noting that Apple does not use the ALAC abbreviation, always using the name Apple Lossless when presenting or discussing this codec, but it is more commonly referred to as ALAC outside of Apple.
CodecApple Lossless data is stored within an MP4 container with the filename extension .m4a. This extension is also used by Apple for lossy AAC audio data in an MP4 container (same container, different audio encoding), though lossy AAC files, as sold through the iTunes Store, are typically in their own .aac container extension. However, Apple Lossless is not a variant of AAC (which is a lossy format), but rather a distinct lossless format that uses linear prediction similar to other lossless codecs. These other lossless codecs, such as FLAC and Shorten, are not natively supported in Apple's iTunes software, either on computers (Mac or Windows) or iDevices, so users of iTunes software who want to use a lossless format (which allows the addition of metadata; unlike WAV/AIF, which metadata is commonly ignored, and other PCM-type formats) have to use ALAC.{{cite web}} All current iDevices can play ALAC–encoded files. ALAC also does not use any DRM scheme, but by the nature of the MP4 container, it is thought that DRM could be applied to ALAC much the same way it can with files in other QuickTime containers.Apple claims that audio files compressed with its lossless codec will use up "about half the storage space" that the uncompressed data would require. Testers using a selection of music have found that compressed files are about 40% to 60% the size of the originals depending on the kind of music, similar to other lossless formats.{{cite web}}http://www.mcelhearn.com/2011/11/05/an-overview-of-apple-lossless-compression-results/ Furthermore, the speed at which it can be decoded makes it useful for limited-power devices such as iOS devices.{{cite web}}
HistoryThe Apple Lossless Encoder (the software for encoding into ALAC files) was introduced into the Mac OS X Core Audio framework on April 28, 2004 together with the QuickTime 6.5.1 update, and thus available in iTunes as of version 4.5 and above. The codec is also used in the AirPort Express's AirPlay implementation.The Apple Lossless Encoder (and decoder) was released as open source software under the Apache License version 2.0 on October 27, 2011.{{cite web}}{{cite news}}{{cite news}}
Other playersDavid Hammerton and Cody Brocious have analyzed and decoded this codec without any documents on the format. On March 5, 2005, Hammerton published a simple open source decoder in the programming language C on the basis of the reverse engineering work{{cite web}}.The open source library libavcodec incorporates both a decoder and an encoder for Apple Lossless format which means that media players based on that library, including VLC media player and MPlayer, are able to play Apple Lossless files, as well as many media centre applications for home theatre computers, such as Plex, XBMC, and Boxee. The library was subsequently optimized for ARM processors and included in Rockbox.
See also{{Portal}}

Comparison of audio codecs
FLAC
Monkey's Audio
TTA
WavPack
WMA Lossless


References{{reflist}}
External links

Apple Lossless Audio Codec Project
Technical Features of ALAC
Apple - iTunes - Import
Article showing different rates of compression for different types of music

{{Compression Formats}}
{{Compression Software Implementations}}
cs:Apple Lossless
da:Apple Lossless
de:Apple Lossless
es:Apple Lossless
fr:Apple Lossless
ko:애플 무손실
it:Apple Lossless Encoding
ja:Apple Lossless
no:Apple Lossless
pl:Apple Lossless
ru:Apple Lossless
sk:Apple Lossless
fi:Apple Lossless
sv:Apple Lossless
uk:Apple Lossless
yo:Apple Lossless
zh:Apple Lossless