title{{About}}
{{primary sources}}
{{Infobox Computer}} The Apple Remote is a remote control made for use with Apple products with infrared capabilities released after October 2005. The device was announced by Steve Jobs on October 12, 2005. The remote is largely based on the interface of the first generation iPod Shuffle and has only seven buttons. The seven buttons on the remote are for Menu, Play/Pause, Volume Up, Volume Down, Previous/Rewind, Next/Fast-forward, and a central select button. The remote was originally designed to interact with Front Row in the iMac G5 and is also compatible with the MacBook and MacBook Pro. The Mac Mini with Apple Remote support was announced on February 26, 2006.  The Apple TV also ships with and utilizes the Apple Remote.The remote was designed to attach magnetically to the side of the late G5 and early Intel iMacs. These models integrated several hidden magnets in the bottom right corner which attract the remote's battery. This is also possible with the frame of MacBook screens.  As of February 2008, the MacBook and MacBook Pro no longer include an Apple Remote in the package, though it remains compatible and available as an option for the MacBook Pro. As of March 2009, the iMac and Mac Mini also no longer ship with an Apple Remote in the package.In October 2009 the original white plastic remote was replaced with a thinner and longer aluminum version. The new aluminum remote was released along with the 27&nbsp;inch aluminum iMacs and multi-touch Magic Mouse. It also introduced a few changes in the six-button layout. The Play/Pause button was moved out of the center of the directional buttons, placing it aside the Menu button (under the directional buttons). The symbols for the Volume Up/Down and Next/Fast-forward buttons were replaced with small dots. This was to make it more obvious that the buttons were also used to move up, down, left, and right within menus. All of the buttons became black and embossed within the aluminum. Along with the new design, the price was dropped to $19.99.The Front Row application allows users to browse and play music, view videos (DVDs and downloaded files) and browse photos. The Apple Remote is also compatible with the iPod Hi-Fi{{Citation}} and the Universal Dock.{{Citation}} The functions for the iPod Universal Dock allow for music and media control, though the remote is not able to control the menus within the iPod. The battery is accessed by pushing a small, blunt object, such as a paper clip or a 3.5&nbsp;mm headphone plug, into a tiny indent at the bottom right edge of the remote, revealing the compartment which houses the CR2032 lithium 3.0 V button cell (1st Gen). Use a coin to access the battery on the back of the remote (2nd Gen){{Citation}}
Shortcuts
PairingA device can be configured to respond only to a certain remote. This can be achieved by holding the Apple Remote close enough to the device with which it is to be paired, and then pressing and holding the "Menu" and "Next" (or "Play"; this option also increments the remote ID before pairing) buttons for five seconds. Pairing can be removed by holding the "Menu" and "Previous" buttons for five seconds or deactivating it under the Mac OS X "Security" System Preference pane. Only users with administrative privileges are allowed to pair their remote; in a non-administrator account, pressing the buttons will have no effect and nothing will be displayed.{{Citation}} Pairing can be very useful because some users who have both an iMac and Apple TV nearby experience issues with remotes working with both devices.
SleepUsers can put iMacs, MacBook Pros, MacBooks, Intel Mac Minis, Apple TVs, or docked iPods into sleep mode by holding down the Play/Pause button on the Apple Remote. Devices can also be awakened by pressing any button on the remote.
Boot optionsHolding down the Menu button on the remote while starting up an Intel Macintosh enters the Startup Manager{{Citation}} (same as holding the Option key at startup). The remote can then be used to cycle through all bootable partitions and can then confirm them by pressing the Play/Pause button. This can be especially useful for Boot Camp users who might frequently use this feature to boot into Windows partitions on the Intel Macs. The remote can also eject CDs or DVDs in this menu by selecting the disc and then pressing the + (Volume Up) button on the remote.
iOS App{{Main}}
Apple also offers a free 'Remote' app for iOS devices (available in the iTunes App Store) which allows for wireless control of iTunes on Mac/Windows computers or the Apple TV.
Application compatibilityThe remote can be used to control presentations in Apple Keynote (on both Intel Macs & PowerPC Macs), presentations in Microsoft PowerPoint 2008 or in OpenOffice.org Impress, picture slide shows in iPhoto and Aperture, QuickTime, DVD Player, and audio in iTunes.{{Citation needed}}  
VLC compatibilityIn response to the new Intel processors, a small piece of software called the Apple Remote Helper has surfaced to allow remote use in the VLC media player. Play, pause, volume, and skip buttons all work normally. It should be noted, however, that the volume buttons change VLC's volume, and not the system volume. Starting with release 0.8.6-test1 VLC itself supports the Apple Remote and no secondary software installation is required.{{Citation needed}}The latest version of VLC (1.0.5) enables all functionality with the latest Apple Remote. (Except the function of sleep).
Hardware compatibility{{Unreferenced section}}
Compatibility with newer MacsThe Apple remote is not supported on the 2010 or 2011 Macbook Air laptops. The Apple remote is supported on all older editions of the Macbook Air. The Apple Remote is not compatible with certain models of the White Unibody MacBook, which is now discontinued. It works with all MacBook Pros and all iMacs, as well as Mac Minis.
Compatibility with older MacsUsing the third-party remote software mira (from Twisted Melon) or Remote Buddy (from IOSPIRIT GmbH) users of older Macs can use the Apple Remote with a USB-based IR receiver. Most new Mac models come equipped with a built-in infrared receiver, but previous generation products lack any such IR device. Even the Mac Pro desktops released in the summer of 2006 lack built-in IR. Using Remote Buddy or mira, it is possible to connect an external USB receiver such as the Windows Media Center Edition eHome receiver, and use the Apple Remote on older machines with full support for sleep, pairing, low battery detection, and Front Row. In addition, Remote Buddy is able to emulate events of an Apple Remote on these systems, enabling users to use software written for the Apple Remote in exactly the same way as with newer Macs.
iPod compatibilityAn iPod placed in a dock featuring an IR sensor can be controlled via the Apple Remote. However, the remote's menu functionality does not work on the iPod.
Compatibility with other devicesThe Apple Remote can also be used to control the iPod Hi-Fi or third party devices tailored to it.
Boot Camp compatibilityAs of Boot Camp 1.2, the remote has been given some compatibility when a user is running Windows. If the user has iTunes installed on the Windows partition, pressing the Menu button on the remote will load the program. As well as this, the remote has the ability to control both Windows Media Player and iTunes, as well as system Volume Control. Additionally, the remote also has the ability to control the audio program foobar2000 and the freeware media program Media Player Classic. Programs must have focus for the remote to control them. Skipping tracks and pausing/playing functionality are available under the programs. Since Boot Camp is now at version 4.0, this information is most likely very out of date. But still very useful.
MacBook and iMac resting placeEarlier models of the iMac (Polycarbonate iMac) featured a magnetic rest for the remote,{{Citation}} which was later (Aluminum iMac) taken out.The MacBook features magnets in its lid (primarily intended for the magnetic latch) which also act as rests for the Apple Remote.
Infrared interferenceBecause many electrical appliances use infrared remote (IR) controls, concurrent use of the Apple Remote with other IR remotes may scramble communications and generate interference, preventing stable use. Remotes should be used individually to circumvent the problem.{{Citation}}
Technical detailsThe Apple Remote uses an NEC IR protocolhttp://wiki.altium.com/display/ADOH/NEC+Infrared+Transmission+Protocol which consists of a differential PPM encoding on a 1:3 duty cycle 38kHz 950 nm infrared carrier. There are 32 bits of encoded data between the AGC leader and the stop bit:{{Citation}}

Protocol  on (µs)  off (µs)  total (µs) 
leader 9000  4500  13500 
0 bit  560  560  1120 
1 bit  560  1690  2250 
stop  560  {{N/a}}  560 
The first two bytes sent are the Apple custom code ID (0xEE followed by 0x87), which are followed by a one byte command and a one byte remote ID (0-255) making a total of 32 bits of data. All bytes are sent least significant bit first. The commands consist of:

Value  Command  Button 
0x16  Menu  Menu 
0x17  Play  Play/Pause 
0x18  Right  Next/Fast-Forward 
0x19  Left  Previous/Rewind 
0x1f  Up  Volume Up 
0x20  Down  Volume Down 

See also

Apple TV
Front Row
iMac
iPod
iPod Hi-Fi
MacBook Air
MacBook Pro
Mac Mini
iTunes Remote
Remote control


References{{Reflist}}
External links
{{Apple hardware since 1998}}
{{Apple hardware}}

de:Apple Remote
es:Apple Remote
fr:Apple Remote
it:Apple Remote
nl:Apple Remote
ja:Apple Remote
ko:애플 리모트
no:Apple Remote
pl:Apple remote
pt:Apple Remote
ru:Apple Remote
sk:Apple Remote
fi:Apple Remote
sv:Apple Remote
zh:Apple Remote