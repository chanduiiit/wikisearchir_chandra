title{{Use dmy dates}}
{{Use British English}}
{{Infobox person}}
Billie Paul Piper (born Lianne Piper,Piper, Billie (2007), p.74. 22 September 1982, in Swindon, Wiltshire) is an English singer and actress.She began her career in the late 1990s as a pop singer and then switched to acting. She started in acting and dancing and was talent spotted at the Sylvia Young stage school by Smash Hits magazine who wanted a "face" for their magazine. A recording contract was the route given to her. She later decided to retire from singing and refocus on her original ambition, acting. Her most famous role is as Rose Tyler, companion to the Doctor, in the television series Doctor Who from 2005 to 2006, a role she reprised in 2008 and 2010.{{cite news}} In 2007, Broadcast magazine listed Piper at no. 6 in its "Hot 100" list of influential on-screen performers, the top woman on the list.{{cite news}}{{cite news}} From 2007 until 2011, she starred as the high-flying escort Belle de Jour in the TV series Secret Diary of a Call Girl. 
Singing career{{ref improve section}}
1998–2000: Honey to the BWhen she was selected to appear on the Saturday morning children's television show Scratchy & Co. Piper later landed a role in a television commercial promoting the pop magazine Smash Hits. Piper was offered a record deal at the age of fifteen, and in 1998 became the youngest artist ever to debut at number one in the UK singles chart with "Because We Want To", released under the stage mononym "Billie". Her follow-up single "Girlfriend" also debuted at number one.Her debut album Honey to the B was released immediately afterwards, and debuted and peaked at Number 14 in the UK album charts, but was still a commercial success, gaining a Platinum certification in the UK, and a 2x Platinum certification in New Zealand, where it reached No. 3. However, Honey to the B found limited success in other territories, such as Australia where it debuted and peaked at No. 31 despite the success of "Honey to the Bee", and in the US it almost went completely unnoticed, peaking at No. 17 on the Heatseekers. This is mostly like due to Britney Spears's album ...Baby One More Time being released one month earlier.At the 1998 Smash Hits Poll Winners' party, she was nominated for Best New Act (She came 2nd. It was won by B*Witched) and won Princess of Pop (Additionally she was the first to win this award as 1998 was the first year to host this award). She then released "She Wants You" as the third single from the album. The song reached No. 3. "Honey to the Bee" was released as the fourth single from the album, like the previous single, it reached No. 3. At the same date, "She Wants You" was released in the USA. It reached No. 9 on the "Hot Club Dance Play" chart.In 1999, Piper was nominated for two BRIT Awards and won 2 awards at the 1999 Smash Hits Poll Winners' party, although at the latter ceremony she was reduced to tears after being booed by fans of Ritchie Neville, member of boy band Five, whom she was dating at the time.Piper then started to tour and release in Asia. The singles and the album were released during mid to late 1999. The Taiwanese edition was notable as it had a completely different cover for unknown reasons. Then On 10 August that same year, the follow-up to "Because We Want To" was released in Japan, a single comprising "Girlfriend" and "She Wants You" combined. Her debut album was released in Japan on 25 April 2000.
2000–2001: Walk of Life
During that time, she recorded her second album. She decided to release further records under her full name, Billie Piper. She returned to the Singles Chart in May 2000 with a new, sexier sound. She hit the Number 1 spot with "Day & Night". She waited until the following September to release "Something Deep Inside", which reached No. 4, but her success wasn't to continue. 
In October 2000, Piper released her second album, Walk of Life, which reached Number 14 in the UK Album Chart, but quickly fell off the charts and was certified Silver in the UK. The album only charted in two other countries, New Zealand where it reached No. 17 only, and in Australia where it was a minor success, peaking at No. 23. In Piper's autobiography, she states the album was a 'commercial bomb'. The song "Walk of Life", the final single off this album, was released in December 2000 and reached Number 25 in the UK Singles Chart.On 17 February 2001, Piper appeared in court to testify against a woman named Juliet Peters. Peters was charged with, and eventually convicted of, stalking as well as making a number of threats against Piper and members of her family.{{cite news}} Peters received psychiatric treatment as part of her sentence. According to her autobiography, Piper was reluctant about the court case, but was pushed by her parents and her label. She also stated in the book that this was why "The Tide Is High" wasn't released as a single, writing "The court case succeeded in doing what I alone could not: cutting the ties. Without it I might have been tempted back." On 15 January 2007, BBC Radio 1 DJ Chris Moyles started a campaign to get "Honey to the Bee" back into the Top 100 on download sales as a way of testing out new chart rules that favour download sales.{{cite web}} The campaign was highly successful, with "Honey to the Bee" re-entering the official UK singles chart at No. 17, eight years after it was first released.

Notes



Despite having limited success outside the UK, all of Billie Piper&#39;s singles reached the top ten in New Zealand except for &#34;Something Deep Inside&#34; which peaked at No. 19 and &#34;Walk of Life&#34; which failed to chart in the top 40. Also, while Piper&#39;s debut album reached No. 14 in the UK it reached No. 3 in New Zealand. Piper&#39;s 2nd album reached No. 18 in New Zealand.
Piper preferred lip syncing during performances. She backs this up with the reason that she is afraid she would not be able to sing live. However, in 1999 she decided to try actually singing during her tour in America. The audience loved it,{{Citation needed}} but Piper said it was too nerve-racking to do in Britain, where her much larger fan base lives.
She recorded a song for Pokémon: The First Movie titled &#34;Makin' My Way (Any Way That I Can)&#34;.[{{Allmusic}} allmusic&nbsp;– Pokemon: The First Movie > Overview]. Retrieved 2009-10-08.
Piper is currently the youngest artist to ever debut at number one, she did this aged 15 with &#34;Because We Want To&#34;.{{cite news}}
She is also currently the second youngest artist to get a number one, behind Jimmy Osmond.
Billie Piper is the youngest and first female solo singer to reach the top spot with her first two singles, &#34;Because We Want To&#34; and &#34;Girlfriend&#34;.


Acting career
After an extended break, Piper decided to end her pop career in 2003 and return to her original ambition: acting. She took acting lessons while living in London.
Doctor Who{{main}}
Doctor Who originally ran from 1963 to 1989. In May 2004, it was announced that the series would be resurrected beginning in 2005, and that Piper was to play the character Rose Tyler, a travelling companion to The Doctor (to be played by Christopher Eccleston).
Piper won the Most Popular Actress category at the 2005 and 2006 National Television Awards for her work on Doctor Who.{{cite news}}
BBC News named Piper as one of its "Faces of the Year" for 2005, primarily due to her success in Doctor Who.  At The South Bank Show Awards on 27 January 2006 Piper was awarded The Times Breakthrough Award for her successful transition from singing to acting. In March 2006, the Television and Radio Industries Club named Piper as best new TV talent at their annual awards ceremony. In September 2006, Piper was named Best Actress at the TV Quick and TV Choice Awards.{{cite news}}After the completion of the very successful first series of the revamped Doctor Who, the British media regularly released conflicting reports about how long Piper would be staying with the programme. In March 2006, she claimed that she would continue on Doctor Who into its third season in 2007.{{cite news}}
On 10 May 2006, however, she was reported to be considering quitting the series, although she did express an interest in playing a female version of the Doctor in the future (possibly related to a proposed Doctor Who spin-off series about Rose which was later dropped).{{cite news}}
On 15 June 2006, the BBC announced that she was to depart in the final episode of the second series, "Doomsday".{{cite news}}
Piper's decision to leave had been taken a year previously, but remained a secret until news of her departure became public.{{cite news}}On 27 November 2007, the BBC confirmed that she would reprise her role as Rose Tyler in the fourth Doctor Who series for three episodes. Later, it was confirmed by Russell T. Davies in Doctor Who Magazine that this return had been planned since she left. It was also revealed in the "Turn Left" Doctor Who Confidential that Billie had made arrangements to return as Rose since she decided to leave.The series began in April 2008, and after several cameos, Piper made her official return as Rose in the series four final episodes "Turn Left", "The Stolen Earth" and "Journey's End". She did not initially state whether she would be reprising the role again. Interviewed on Doctor Who Confidential, she commented that "it's never really the end for the Doctor and Rose", but "it's certainly the end for the foreseeable future"."The End of An Era". Doctor Who Confidential. BBC. Episode 56 (Season 4, No. 13), BBC Three, 2008-07-05. She reprised her role as Rose Tyler in The End of Time, the last of the 2009–10 Doctor Who specials.{{cite news}}
Secret Diary of a Call Girl{{main}}
Piper also starred as Hannah Baxter in Secret Diary of a Call Girl, an ITV2 adaptation of Brooke Magnanti's The Intimate Adventures of a London Call Girl, a memoir detailing the life of a high-class prostitute who adopted "Belle de Jour" as her pseudonym. The series, which aired from 27 September 2007, saw Piper in several semi-nude scenes, including one scene featuring her saddling a client and riding him like a horse.{{cite news}} As part of her preparation for the role Piper met the memoire's author some two years before her identity as a research scientist was revealed in a Sunday newspaper: "I absolutely had to meet the person behind the words to be able to take the part... People did ask me about her and I just had to smile, to avoid giving anything away...".Radio Times, 23–29 January 2010 A second series, with Piper in the starring role, started filming in May 2008, during which two body doubles were hired in order to hide Piper's pregnancy during the sex scenes.{{cite web}}{{cite web}} The third series began airing in January 2010, with Piper dispensing with the body doubles, performing all her own sex scenes.{{cite news}}In January 2010, tying in with the broadcast of the third series and following on from the real Belle de Jour confirming her real identity, ITV2 broadcast an interview special, Billie and the Real Belle Bare All which saw Piper meeting with Dr Brooke Magnanti on-camera for the first time.{{cite news}}
Other acting workIn 2004, Piper appeared in the films The Calcium Kid, as the romantic interest of Orlando Bloom's character, and Things to do Before You're Thirty. Shortly before starting work on Doctor Who, she filmed a starring role in the horror film Spirit Trap alongside Russian pop star Alsou, released in August 2005 to generally poor reviews.In November 2005, Piper starred as Hero in a BBC adaptation of Much Ado About Nothing, updated for the modern day in a similar manner to the Canterbury Tales series in which she featured, with Hero now being a weather presenter in a television station.Piper has completed work on two stand-alone television productions. In the first, a BBC adaptation of Philip Pullman's historical novel The Ruby in the Smoke broadcast in December 2006, Piper played protagonist Sally Lockhart, a Victorian orphan along with Matt Smith playing Jim Taylor. The BBC plans to film all four of Pullman's Sally Lockhart novels, with Piper continuing in the role in The Shadow in the North which was shown in December 2007.Piper made her stage debut in a touring production of Christopher Hampton's play Treats, which opened in early 2007 in Windsor. Treats was to have ended its tour in the West End, at the Garrick Theatre, starting on 28 February 2007 with previews from 20 February. The play officially finished as of 26 May.{{cite news}}In 2007 she appeared as the main character, Fanny Price, in an adaptation of Jane Austen's novel Mansfield Park, screened on ITV1.{{cite news}} This was her first acting role on television for a broadcaster other than the BBC.Piper has provided voiceovers for various television commercials, including one for Comfort Fabric Softener airing in June 2007, and Debenhams currently airing 2011.Piper has also shared the role of Betty with Sue Johnston in the TV adaptation of A Passionate Woman, screened on BBC 1 on 11 and 18 April 2010.Fletcher, Alex (29 September 2009). "Billie Piper to star in 'Passionate Woman'". Digitalspy. Retrieved 2009-10-03.Cawley, Christian (30 September 2009). "Passionate Billie". Kasterborous. Retrieved 2009-10-03.Billie Piper had been confirmed to join the cast of an upcoming romance-comedy film directed by Robin Sheppard called Truth about Lies. Filming is expected to start in October 2011.Wiseman, Andreas (25 May 2011). "Revolver moves with Ghosted; Billie Piper joins Truth About Lies".Screen Daily. {{subscription required}}Piper had been confirmed to play Carly in the UK Premiere of Reasons to be Pretty at the Almeida Theatre, running from November 2011 to January 2012. It opened on 17 November to press, and has since received critical acclaim with reviewers claiming it was 'one of the best theatre productions they'd seen'. {{cite news}} The Guardian, The Observer, Daily Mail, London Evening Standard, Metro, The Times, The Telegraph, Time Out, The Arts Desk, Daily Express and The Financial Times all gave the production rave reviews with a minimum of four stars. {{cite news}} BBC Radio 4 reviewed the show live applauding Billie Piper as "Fantastic, completely brilliant. Her performance is so convincing and moving, an absolutely terrific performance" {{cite web}} The Jewish Chronicle hailed Billie Piper's performance as second to none, being the best of the night, and stating that "No actor can cry more convincingly than Piper", giving the show four stars. {{cite web}}
Personal life
Piper was born in Swindon, Wiltshire, England, to Paul Piper and Mandy Kent. Piper has one younger brother, Charlie, and two younger sisters, Harley and Elle.{{cite news}}Piper married businessman, DJ and television presenter Chris Evans in a secret ceremony in May 2001 in Las Vegas after six months of dating. Their marriage attracted much comment due to the sixteen-year age gap between the two.{{cite news}}
The couple separated in 2004 and later divorced in May 2007."Divorce given to Piper and Evans", BBC News, 2007-05-27. Retrieved on 2007. They have remained friends.Knight, Kathryn & Moodie, Clemmie (2007-06-04). "Chris Evans and Billie: A very bizarre divorce". Daily Mail. Retrieved 2009-09-23.A story in The Independent on 27 June 2006 stated that Piper has declared that she does not wish to claim any money from Evans's reported £30m wealth or his £540,000 salary from Radio Two. "I'm not taking a penny from him," she told the Radio Times, "I think that's disgusting." Piper also revealed in her interview with Radio Times that she left her pop star career with very little money.{{cite news}}
Evans has admitted that the age gap was a reason in seeking the divorce.{{cite news}}Piper dated and lived with law student Amadu Sowe from 2004 to 2006.Piper married actor Laurence Fox, son of actor James Fox, on 31 December 2007 at St Mary's Church in Easebourne, West Sussex.{{cite news}} They live in Easebourne, Midhurst in West Sussex.{{cite news}}
Their first child, Winston James Fox, was born by an emergency Caesarean section in October 2008, following a 26-hour labour.{{cite news}}{{cite news}} In a later interview Piper revealed that medical staff were concerned that Winston would suffer from oxygen starvation if the birth was to continue as planned. Piper said, "they were worried about him losing oxygen to his brain. I was like, 'Cut him out now!'"
On 28 October 2011, Billie and husband Laurence Fox announced that she was 15 weeks pregnant with their second child due mid April.Piper is close friends with her former Doctor Who co-star David Tennant."Billie gets a visit from the Doctor", Evening Standard, 2007-03-15. Retrieved 2011-06-23. She is also close to Matt Smith, who took over playing the title character following his departure. After it was announced that Smith would be replacing Tennant in the role, she stated "Yes, I hate that he's left! But then I know Matt Smith really well and I love him and I think he's such a brilliant actor. The role couldn't have gone to a better guy. He's capable of that. He's got big shoes to fill, but I'm certain he can do it.""Billie Piper ('Secret Diary')", Digital Spy, 2010-01-25. Retrieved on 2011-06-23. Smith also reportedly asked her for advice after learning he'd won the part."Billie Piper: Doctor Who actor Matt Smith phoned me for advice", Now, 2009-02-24. Retrieved 2011-06-23.
Filmography

Film
Year Film Role Notes 
1996 Evita uncredited bit-part  
1996 {{sortname}} uncredited bit-part  
2004 {{sortname}} Angel  
2005 Things To Do Before You're 30 Vicky  
2005 Spirit Trap Jenny  
2010 Animals United Bonnie the Meerkat  
2010 The Raven"The Raven : New A/W 2010 fashion film launched". ShowStudio.com. 17 December 2010. Raven UK Short Film 
2012 Truth About Lies  Unknown  


Television
Year TV series Role Notes 
2003 {{sortname}}: The Miller's Tale (BBC One) Alison Crosby  
2004 Bella and the Boys (BBC Two) Bella  
2005–06, 2008, 2010 Doctor Who (BBC One) Rose Tyler Regular until 2006. Recurring character until 2010.BBC&#39;s Best Actress of 2005, 2006BBC&#39;s Most Desirable Star of 2005National Television Award for Most Popular Actress Actress (2005, 2006)SFX Award for Best TV Actress (2005, 2007)TV Quick Award for Best Actress (2006)TRIC Award for New TV Talent (2006, also for ShakespeaRe-Told)Nominated—BAFTA Cymru Award for Best Actress (2005, 2006)Nominated—Broadcasting Press Guild Award for Best Actress (2006, also for ShakespeaRe-Told) 
2005 ShakespeaRe-Told: Much Ado About Nothing (BBC One) Hero TRIC Award for New TV Talent (2006, also for Doctor Who)Nominated—Broadcasting Press Guild Award for Best Actress (2006, also for Doctor Who) 
2006 {{sortname}} (BBC One) Sally Lockhart From the Sally Lockhart mysteries. 
2007 Mansfield Park (ITV, 2007) Fanny Price Nominated for a TV Quick Award for Best Actress 
2007 {{sortname}} (BBC One, 2007) Sally Lockhart From the Sally Lockhart mysteries. 
2007–11 Secret Diary of a Call Girl (ITV2) Hannah Baxter Principal character until 2011. Nominated for an Ewwy Award for Best Actress 
2010 {{sortname}} (BBC One) Betty Two-part TV mini-series. 
2012 Tom & Jenny (BBC Three) Jenny Pilot episode. 
2012 Love Life (BBC One){{cite web}} Holly 

Stage

Year Title Role Notes 
2007 Treats Ann Garrick Theatre 
2011-2012 Reasons to be Pretty Carly Almeida Theatre 

Awards and nominations


Awards



1998 – Smash Hits Awards: Princess of Pop
1999 – Smash Hits Awards: Best Female
1999 – Smash Hits Awards: Best Dressed Female
1999 – Smash Hits Awards: Best Female Act
2005 – The National Television Awards: Most Popular Actress
2005 – BBC Face Of The Year
2005 – BBC Drama Awards: Best Actress
2006 – The South Bank Show Awards: The Times Breakthrough Award – Rising British Talent
2006 – TV Choice/TV Quick Awards: Best Actress
2006 – The National Television Awards: Most Popular Actress
2006 – BBC Drama Awards: Best Actress
2006 – Tric Awards: Best New Talent
2006 – GQ Magazine Awards: Woman of the Year
2006 – BBC Drama Awards: Exit of the year
2011 -Glamour&#39;s UK Actress of the Year



Nominations



1998 – Smash Hits Awards: Best New Act
2006 – Broadcasting Press Guild Awards: Best Actress(role in Doctor Who &#38; ShakespeaRe-Told: Much Ado About Nothing)
2006 – BAFTA Cymru Awards: Best Actress
2007 – TV Choice/TV Quick Awards:Best Actress Mansfield Park
2008 – Rose d'Or: Special Award for Best Entertainer (for Secret Diary of a Call Girl).{{cite news}}
2009 – Ewwy Award: Best Actress in a Comedy Series (for Secret Diary of a Call Girl)
2011 - Hello Magazines Most Attractive Woman


Discography{{Main}}

Honey to the B (1998)
Walk of Life (2000)


References


{{cite news}}
{{cite news}}
Piper, Billie (2007). Growing Pains. London: Hodder &#38; Stoughton. ISBN 9780340932803
{{cite news}}
Searchable index of UK chart positions, including Piper's hits
List of all #1 songs (including Piper's) from The Official UK Charts Company
Chris Evans Saved My Life


Footnotes
{{Reflist}}
External links{{Commons category}}
{{wikiquote}}

BBC Drama Faces: Billie Piper
{{IMDb name}}
Audio interview at BBC Wiltshire (October 2006)
Audio interview at BBC Somerset (February 2007)

{{BilliePiper}}{{Persondata}}
{{DEFAULTSORT:Piper, Billie}}












bg:Били Пайпър
de:Billie Piper
es:Billie Piper
eu:Billie Piper
fr:Billie Piper
it:Billie Piper
he:בילי פייפר
la:Gulielma Piper
nl:Billie Piper
no:Billie Piper
pl:Billie Piper
pt:Billie Piper
ksh:Billie Piper
ru:Пайпер, Билли
simple:Billie Piper
fi:Billie Piper
sv:Billie Piper
tr:Billie Piper
uk:Біллі Пайпер