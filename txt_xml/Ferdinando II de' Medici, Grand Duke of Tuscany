title{{Infobox royalty}}
Ferdinando II de' Medici (14 July 1610 &ndash; 23 May 1670) was grand duke of Tuscany from 1621 to 1670. He was the eldest child of Cosimo II de' Medici and Maria Maddalena of Austria. His 49 year rule was punctuated by the terminations of the remaining operations of the Medici Bank, and the beginning of Tuscany's long economic decline.Strathern, p 381 He married Vittoria della Rovere, with whom he had two children: Cosimo III de' Medici, his eventual successor, and Francesco Maria de' Medici, a Prince of the Church.
Biography
Ferdinando was the eldest son of Cosimo II de' Medici, Grand Duke of Tuscany and Maria Maddalena of Austria. Cosimo II died when he was 10; because he had not yet reached legal maturity, his mother and paternal grandmother, Christina of Lorraine, acted as joint regents.Hale, p 178 Dowager Grand Duchess Christina brought Tuscany into the Pope’s sphere of influence.
In his seventeenth year, Ferdinando embarked on a tour of Europe. One year later, his regency ended and his personal rule began.Strathern, p 375 Dowager Grand Duchess Christina was the power behind the throne until her demise in 1636.The first calamity of Ferdinando’s reign was in 1630, when a plague swept through Florence and took 10% of the population with it.Hale, p 179 Unlike the Tuscan nobility, Ferdinando and his brothers stayed in the city to try to ameliorate the general suffering.Acton, p 29 His mother and grandmother arranged a marriage with Vittoria della Rovere, a granddaughter of the then incumbent Duke of Urbino, in 1634. Together they had two children: Cosimo, in 1642, and Francesco Maria de' Medici, in 1660. The latter was the fruit of a brief reconciliation, as after the birth of Cosimo, the two became estranged; Vittoria caught Ferdinando in bed with a page, Count Bruto della Molera.Acton, p 30
Grand Duke Ferdinando was obsessed with new technology, and had several hygrometers, barometers, thermometers, and telescopes installed in the Pitti.Acton, p 27 In 1657, Leopoldo de' Medici, the Grand Duke’s youngest brother, established the Accademia del Cimento. It was set up to attract scientists from all over Tuscany to Florence for mutual study.Acton, p 38Tuscany participated in the Wars of Castro (the last time Medicean Tuscany was involved in a conflict) and inflicted a defeat on the forces of Urban VIII in 1643.Hale, p 180 The treasury was so empty that when the Castro mercenaries were paid for the state could no longer afford to pay interest on government bonds. The interest rate was lowered by 0.75%.Hale, p 181 The economy was so decrepit that barter trade became prevalent in rural market places.Ferdinando died on 23 May 1670 of apoplexy and dropsy. He was interred in the Basilica of San Lorenzo, the Medici's necropolis.Acton, p 108 At the time of his death, the population of the grand duchy was 730,594 souls; the streets were lined with grass and the buildings on the verge of collapse in Pisa.Acton, p 112
Issue


Cosimo de' Medici, Grand Prince of Tuscany (19 December 1639 - 21 December 1639) died in infancy.
Unnamed son (1640)
Cosimo III de' Medici, Grand Duke of Tuscany (14 August 1642 – 31 October 1723) married Marguerite Louise d'Orléans and had issue.
Francesco Maria de' Medici (12 November 1660 – 3 February 1711) married Eleonora Luisa Gonzaga, no issue.


Ancestors{{ahnentafel top}}
{{ahnentafel-compact5}}
{{ahnentafel bottom}}
Titles, styles, honours and arms{{infobox hrhstyles}}
Titles and styles


14 July 1610 &ndash; 28 February 1621 His Highness The Grand Prince of Tuscany
28 February 1621 &ndash; 23 May 1670 His Highness The Grand Duke of Tuscany


Citations{{Reflist}}
See also

Grand Duchy of Tuscany


References

Acton, Harold: The Last Medici, Macmillan, London, 1980, ISBN 0-333-29315-0
Strathern, Paul: The Medici: Godfathers of the Renaissance, Vintage books, London, 2003, ISBN 978-0-099-52297-3
Hale, J.R.: Florence and the Medici, Orion books, London, 1977, ISBN 1-84212-456-0


External links{{Commonscat-inline}}
See also{{tuscan princes}}
{{Grand Dukes of Tuscany}}{{Persondata}}
{{DEFAULTSORT:Medici, Ferdinando 2}}













ar:فرديناندو الثاني دي ميديشي
br:Ferdinando II de' Medici
ca:Ferran II de Mèdici
de:Ferdinando II. de’ Medici
es:Fernando II de Toscana
fr:Ferdinand II de Médicis
it:Ferdinando II de' Medici
he:פרדיננדו השני דה מדיצ'י, הדוכס הגדול של טוסקנה
la:Ferdinandus II Medices
nl:Ferdinando II de' Medici
ja:フェルディナンド2世・デ・メディチ
no:Ferdinando II de' Medici
pl:Ferdinando II de' Medici
pt:Fernando II de Médici
ro:Ferdinando al II-lea de' Medici, Mare Duce de Toscana
ru:Фердинандо II Медичи
sv:Ferdinand II av Medici
uk:Фердинандо ІІ Медічі
zh:斐迪南二世·德·美第奇