title{{US Constitution article series}}The Fifteenth Amendment (Amendment XV) to the United States Constitution prohibits each government in the United States from denying a citizen the right to vote based on that citizen's "race, color, or previous condition of servitude" (i.e., slavery). It was ratified on February 3, 1870.The Fifteenth Amendment is one of the Reconstruction Amendments.
Text{{quote}}
History{{Ref improve section}}
The Fifteenth Amendment is the third of the Reconstruction Amendments. This amendment prohibits the states and the federal government from using a citizen's race (this applies to all races),Rice v. Cayetano, {{ussc}} color or previous status as a slave as a voting qualification. The North Carolina Supreme Court upheld this right of free men of color to vote; in response, amendments to the North Carolina Constitution removed the right in 1835.{{Cite document}} Granting free men of color the right to vote could be seen as giving them the rights of citizens, an argument explicitly made by Justice Curtis's dissent in Dred Scott v. Sandford:{{cite document}}{{quote}}
{{stack}}Both the final House and Senate versions of the amendment broadly protected the right of citizens to vote and to hold office.The Fifteenth Amendment And "Political Rights" The final House version read:{{quote}} Likewise, the final Senate version read:{{quote}}However, both versions of the amendment posed serious challenges in being ratified by three-fourths of the states.{{cite book}} A handful of states still required voters and candidates to be Christian. Southern Republicans were reluctant to undermine loyalty tests, which the Reconstruction state governments used to limit the influence of ex-Confederates, and partly because some Northern and Western politicians wanted to continue disenfranchising non-native Irish and Chinese.{{cite book}}To increase chances of ratification, the amendment was revised in a conference committee to remove any reference to holding office and only prohibited discrimination based on race, color or previous condition of servitude. Yet, despite these changes, ratification of the amendment was still in some doubt.Virginia, Mississippi, Texas and Georgia, were required to ratify the amendment as a precondition for having congressional representation. Nevada ratified the amendment, but only after being reassured by Senator William Morris Stewart that the amendment would not preclude prohibiting the Chinese and Irish immigrants from voting.15thamendment.harpweek.com California and Oregon both opposed the measure out of fear of Chinese immigrants, New York initially ratified the amendment, but legislators later attempted to rescind the ratification, a controversial decision that might have resulted in a court challenge, but for the fact that on March 30, 1870, enough states had ratified the amendment for it to become part of the Constitution.Thomas Mundy Peterson was the first African American to vote after the amendment's adoption. Peterson cast his ballot in an  election over whether to revise the city of Perth Amboy's charter or abandon it for a township form of government. His vote was cast at City Hall in Perth Amboy, New Jersey on March 31, 1870.[McGinnis, William C. "Thomas Mundy Peterson, First Negro Voter in the United States Under the Fifteenth Amendment to the Constitution." Privately published, 1960] On a per capita and absolute basis, more blacks were elected to public office during the period from 1865 to 1880 than at any other time in American history including a number of state legislatures which were effectively under the control of a strong African American caucus. These legislatures brought in programs that are considered part of government's role now, but at the time were radical, such as universal public education. They also set all racially biased laws aside, including anti-miscegenation laws (laws prohibiting interracial marriage).{{Dubious}}

Despite the efforts of groups like the Ku Klux Klan to intimidate black voters and white Republicans, assurance of federal support for democratically elected southern governments meant that most Republican voters could both vote and rule in confidence. For example, an all-white mob in the Battle of Liberty Place attempted to take over the interracial government of New Orleans. President Ulysses S. Grant sent in federal troops to restore the elected mayor.However, in order to appease the South after his close election, Rutherford B. Hayes agreed to withdraw federal troops. He also overlooked rampant fraud and electoral violence in the Deep South, despite several attempts by the Republicans to pass laws protecting the rights of black voters and to punish intimidation. Without the restrictions, voting place violence against blacks and Republicans increased, including instances of murder. Most of this was done without any intervention by law enforcement and often even with their cooperation.By the 1890s, many Southern states had strict voter eligibility laws, including literacy tests and poll taxes. Some states even made it difficult to find a place to register to vote.
AdoptionThe Congress proposed the Fifteenth Amendment on February 26, 1869.{{cite web}} The final vote in the Senate was 39 to 13, with 14 not voting.{{cite book}} Several fierce advocates of equal rights, such as Massachusetts Senator Charles Sumner, abstained from voting because the amendment did not prohibit devices which states might use to restrict black suffrage, such as literacy tests and poll taxes.{{cite book}} The vote in the House was 144 to 44, with 35 not voting. The House vote was almost entirely along party lines, with no Democrats supporting the bill and only 3 Republicans voting against it.{{cite book}}States that ratified pre-certification

Nevada (1 March 1869)
West Virginia (3 March 1869)
Illinois (5 March 1869)
Louisiana (5 March 1869)
Michigan (5 March 1869)
North Carolina (5 March 1869)
Wisconsin (5 March 1869)
Maine (11 March 1869)
Massachusetts (12 March 1869)
Arkansas (15 March 1869)
South Carolina (15 March 1869)
Pennsylvania (25 March 1869)
New York (14 April 1869)Rescinded ratification on 5 January 1870, and re-ratified on 30 March 1970
Indiana (14 May 1869)
Connecticut (19 March 1869)
Florida (14 June 1869)
New Hampshire (1 July 1869)
Virginia (8 October 1869)
Vermont (20 October 1869)
Alabama (16 November 1869)
Missouri (7 January 1870)
Minnesota (13 January 1870)
Mississippi (17 January 1870)
Rhode Island (18 January 1870)
Kansas (19 January 1870)
Ohio (27 January 1870)
Georgia (2 February 1870)
Iowa (3 February 1870)

States that ratified post-certification:

Nebraska (17 February 1870)
Texas (18 February 1870)
New Jersey (15 February 1871)
Delaware (12 February 1901)
Oregon (24 February 1959)
California (3 April 1962)
Maryland (7 May 1973)
Kentucky (18 March 1976)
Tennessee (8 April 1997)


See also

40 acres and a mule
Ballot access
Voting Rights Act of 1965
Voting rights in the United States
Voter suppression


References{{Reflist}}
External links

Fifteenth Amendment and related resources at the Library of Congress
National Archives: Fifteenth Amendment
CRS Annotated Constitution: Fifteenth Amendment

{{US Constitution}}{{DEFAULTSORT:15}}




de:15. Zusatzartikel zur Verfassung der Vereinigten Staaten
es:Decimoquinta Enmienda a la Constitución de los Estados Unidos
fa:اصلاحیه پانزدهم قانون اساسی ایالات متحده آمریکا
fr:XVe amendement de la Constitution des États-Unis
it:XV emendamento della Costituzione degli Stati Uniti d'America
he:התיקון ה-15 לחוקת ארצות הברית
ja:アメリカ合衆国憲法修正第15条
pl:15. poprawka do Konstytucji Stanów Zjednoczonych
ru:Пятнадцатая поправка к Конституции США
zh:美國憲法第十五修正案