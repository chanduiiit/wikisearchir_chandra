title{{Other uses}}
In biology and specifically, genetics, the term hybrid has several meanings, all referring to the offspring of sexual reproduction.Rieger, R.; Michaelis A.; Green, M. M. (1991). Glossary of Genetics (5th ed.). Springer-Verlag. ISBN 0-387-52054-6 page 256

In general usage, hybrid is synonymous with heterozygous: any offspring resulting from the mating of two distinctly homozygous individuals
a genetic hybrid carries two different alleles of the same gene
a structural hybrid results from the fusion of gametes that have differing structure in at least one chromosome, as a result of structural abnormalities
a numerical hybrid results from the fusion of gametes having different haploid numbers of chromosomes
a permanent hybrid is a situation where only the heterozygous genotype occurs, because all homozygous combinations are lethal.

From a taxonomic perspective, hybrid refers to offspring resulting from the interbreeding between two animals or plants of different taxa.Keeton, William T. 1980. Biological science. New York: Norton. ISBN 0-393-95021-2 page A9.

Hybrids between different subspecies within a species (such as between the Bengal tiger and Siberian tiger) are known as intra-specific hybrids. Hybrids between different species within the same genus (such as between lions and tigers) are sometimes known as interspecific hybrids or crosses. Hybrids between different genera (such as between sheep and goats) are known as intergeneric hybrids. Extremely rare interfamilial hybrids have been known to occur (such as the guineafowl hybrids).Ghigi A. 1936. "Galline di faraone e tacchini" Milano (Ulrico Hoepli) No interordinal (between different orders) animal hybrids are known.
The second type of hybrid consists of crosses between populations, breeds or cultivars within a single species. This meaning is often used in plant and animal breeding, where hybrids are commonly produced and selected because they have desirable characteristics not found or inconsistently present in the parent individuals or populations. This flow of genetic material between populations or races is often called hybridization.


EtymologyAccording to the Oxford English Dictionary, the word is derived from Latin hybrida, meaning the "offspring of a tame sow and a wild boar", "child of a freeman and slave", etc.askoxford.com The term entered into popular use in English in the 19th century, though examples of its use have been found from the early 17th century.Oxford English Dictionary Online, Oxford University Press 2007.
Types of hybridsDepending on the parents, there are a number of different types of hybrids;Wricke, Gunter, and Eberhard Weber. 1986.
Quantitative genetics and selection in plant breeding. Berlin: W. de Gruyter. Page 257.

Single cross hybrids — result from the cross between two true breeding organisms and produces an F1 generation called an F1 hybrid (F1 is short for Filial 1, meaning &#34;first offspring&#34;). The cross between two different homozygous lines produces an F1 hybrid that is heterozygous; having two alleles, one contributed by each parent and typically one is dominant and the other recessive. The F1 generation is also phenotypically homogeneous, producing offspring that are all similar to each other.
Double cross hybrids — result from the cross between two different F1 hybrids.J. O. Rawlings, C. Clark Cockerham Analysis of Double Cross Hybrid Populations. J. O. Rawlings, C. Clark Cockerham Biometrics, Vol. 18, No. 2 (Jun., 1962), pp. 229-244 doi:10.2307/2527461
Three-way cross hybrids — result from the cross between one parent that is an F1 hybrid and the other is from an inbred line.Roy, Darbeshwar. 2000. Plant breeding analysis and exploitation of variation. Pangbourne, UK: Alpha Science International. Page 446.
Triple cross hybrids — result from the crossing of two different three-way cross hybrids.
Population hybrids — result from the crossing of plants or animals in a population with another population. These include crosses between organisms such as interspecific hybrids or crosses between different races.
Hybrid species - results from hybrid populations evolving reproductive barriers against their parent species through hybrid speciation.{{cite book}}


Interspecific hybridsInterspecific hybrids are bred by mating two species, normally from within the same genus. The offspring display traits and characteristics of both parents. The offspring of an interspecific cross are very often sterile; thus, hybrid sterility prevents the movement of genes from one species to the other, keeping both species distinct.Keeton, William T. 1980. Biological science. New York: Norton. ISBN 0-393-95021-2 Page 800 Sterility is often attributed to the different number of chromosomes the two species have, for example donkeys have 62 chromosomes, while horses have 64 chromosomes, and mules and hinnies have 63 chromosomes. Mules, hinnies, and other normally sterile interspecific hybrids cannot produce viable gametes, because differences in chromosome structure prevent appropriate pairing and segregation during meiosis, meiosis is disrupted, and viable sperm and eggs are not formed. However, fertility in female mules has been reported with a donkey as the father.{{cite web}}Most often other processes occurring in plants and animals keep gametic isolation and species distinction. Species often have different mating or courtship patterns or behaviors, the breeding seasons may be distinct and even if mating does occur antigenic reactions to the sperm of other species prevent fertilization or embryo development. Hybridisation is much more common among that spawn indiscriminately, like soft corals and among plants.While it is possible to predict the genetic composition of a backcross on average, it is not possible to accurately predict the composition of a particular backcrossed individual, due to random segregation of chromosomes. In a species with two pairs of chromosomes, a twice backcrossed individual would be predicted to contain 12.5% of one species' genome (say, species A). However, it may, in fact, still be a 50% hybrid if the chromosomes from species A were lucky in two successive segregations, and meiotic crossovers happened near the telomeres. The chance of this is fairly high: \left(\frac{1}{2}\right)^{(2 \times 2)} = \frac{1}{16} (where the "two times two" comes about from two rounds of meiosis with two chromosomes); however, this probability declines markedly with chromosome number and so the actual composition of a hybrid will be increasingly closer to the predicted composition.Hybrids are often named by the portmanteau method, combining the names of the two parent species. For example, a zeedonk is a cross between a zebra and a donkey. Since the traits of hybrid offspring often vary depending on which species was mother and which was father, it is traditional to use the father's species as the first half of the portmanteau. For example, a liger is a cross between a male lion and a female tiger, while a tiglon is a cross between a male tiger and a female lion.
Domestic and wild hybrids{{Unreferenced section}}Hybrids between domesticated and wild animals in particular may be problematic. Breeders of domesticated species discourage crossbreeding with wild species, unless a deliberate decision is made to incorporate a trait of a wild ancestor back into a given breed or strain. Wild populations of animals and plants have evolved naturally over millions of years through a process of natural selection in contrast to human controlled selective breeding or artificial selection for desirable traits from the human point of view. Normally, these two methods of reproduction operate independently of one another. However, an intermediate form of selective breeding, wherein animals or plants are bred by humans, but with an eye to adaptation to natural region-specific conditions and an acceptance of natural selection to weed out undesirable traits, created many ancient domesticated breeds or types now known as landraces.Many times, domesticated species live in or near areas which also still hold naturally evolved, region-specific wild ancestor species and subspecies. In some cases, a domesticated species of plant or animal may become feral, living wild. Other times, a wild species will come into an area inhabited by a domesticated species. Some of these situations lead to the creation of hybridized plants or animals, a cross between the native species and a domesticated one. This type of crossbreeding, termed genetic pollution by those who are concerned about preserving the genetic base of the wild species, has become a major concern. Hybridization is also a concern to the breeders of purebred species as well, particularly if the gene pool is small and if such crossbreeding or hybridization threatens the genetic base of the domesticated purebred population.The concern with genetic pollution of a wild population is that hybridized animals and plants may not be as genetically strong as naturally evolved region specific wild ancestors wildlife which can survive without human husbandry and have high immunity to natural diseases. The concern of purebred breeders with wildlife hybridizing a domesticated species is that it can coarsen or degrade the specific qualities of a breed developed for a specific purpose, sometimes over many generations. Thus, both purebred breeders and wildlife biologists share a common interest in preventing accidental hybridization.
Hybrid species{{Main}}
While not very common, a few animal species have been recognized as being the result of hybridization. The Lonicera fly is an example of a novel animal species that resulted from natural hybridization. The American red wolf too appear to be a hybrid species between gray wolf and coyote.{{cite web}} The European edible frog appear a species, but is actually semi-permanent hybrids between pool frogs and marsh frogs, the edible frog population being dependent on the presence of at least one of the parents species to be maintained.Frost, Grant, Faivovich, Bain, Haas, Haddad, de Sá, Channing, Wilkinson, Donnellan, Raxworthy, Campbell, Blotto, Moler, Drewes, Nussbaum, Lynch, Green, and Wheeler 2006. The amphibian tree of life. Bulletin of the American Museum of Natural History. Number 297. New York. Issued March 15, 2006.Hybrid species of plants are a lot more common than animals. Many of the crop species are hybrids, and hybridization appear to be an important factor in speciation in some plant groups.
Examples of hybrid animals




Hybrid Iguana, a single‐cross hybrid resulting from natural interbreeding between male marine iguanas and female land iguanas since the late 2000s.
Equid hybrids
Mule, a cross of female horse and a male donkey.
Hinny, a cross between a female donkey and a male horse. Mule and hinny are examples of reciprocal hybrids.
Zebroids
Zeedonk or Zonkey, a zebra/donkey cross.
Zorse, a zebra/horse cross
Zony or Zetland, a zebra/pony cross (&#34;zony&#34; is a generic term; &#34;zetland&#34; is specifically a hybrid of the Shetland pony breed with a zebra)
Bovid hybrids
Dzo, zo or yakow; a cross between a domestic cow/bull and a yak.
Beefalo, a cross of an American bison and a domestic cow. This is a fertile breed; this along with genetic evidence has caused them to be recently reclassified into the same genus, Bos.{{citation needed}}
Zubron, a hybrid between wisent (European bison) and domestic cow.
Sheep-goat hybrids, such as the Toast of Botswana.
Ursid hybrids, such as the grizzly-polar bear hybrid, occur between black bears, brown bears, and polar bears.
Felid hybrids
Savannah cats are the hybrid cross between an African serval cat and a domestic cat
A hybrid between a Bengal tiger and a Siberian tiger is an example of an intra-specific hybrid.
Ligers and Tiglons (crosses between a lion and a tiger) and other Panthera hybrids such as the lijagulep. Various other wild cat crosses are known involving the lynx, bobcat, leopard, serval, etc.
Bengal cat, a cross between the Asian leopard cat and the domestic cat, one of many hybrids between the domestic cat and wild cat species. The domestic cat, African wild cat and European wildcat may be considered variant populations of the same species (Felis silvestris), making such crosses non-hybrids.
Fertile canid hybrids occur between coyotes, wolves, dingoes, jackals and domestic dogs.
Hybrids between black and white rhinoceroses have been recognized.
Hybrids between spotted owls and barred owls
Cama, a cross between a camel and a llama, also an intergeneric hybrid.
Wholphin, a fertile but very rare cross between a false killer whale and a bottlenose dolphin.
A fertile cross between a king snake and a corn snake.
At Chester Zoo in the United Kingdom, a cross between an African elephant (male) and an Asian elephant (female). The male calf was named Harjeeven. It died of intestinal infection after twelve days.
Cagebird breeders sometimes breed hybrids between species of finch, such as goldfinch x canary. These birds are known as mules.
Gamebird hybrids, hybrids between gamebirds and domestic fowl, including chickens, guineafowl and peafowl, interfamilial hybrids.
Numerous macaw hybrids are also known.
Red kite x black kite: five bred unintentionally at a falconry center in England. (It is reported{{Weasel-inline}} that the black kite (the male) refused female black kites but mated with two female red kites.)
Hybridization between the endemic Cuban crocodile (Crocodilus rhombifer) and the widely distributed American crocodile (Crocodilus acutus) is causing conservation problems for the former species as a threat to its genetic integrity.http://www.savingwildplaces.com/swp-home/swp-crocodile/8287793?preview=&psid=&ph=class%2525253dawc-148772{{Clarify}}
Saltwater crocodiles (Crocodylus porosus) have mated with Siamese crocodiles (Crocodylus siamensis) in captivity producing offspring which in many cases have grown over {{convert}} in length. It is likely that wild hybridization occurred historically in parts of southeast Asia.
Blood parrot cichlid, which is probably created by crossing a red head cihclid and a Midas cichlid or red devil cichlid
A group of about 50 hybrids between Australian blacktip shark and the larger common blacktip shark was found by Australia&#39;s East Coast in 2012. This is the only known case of hybridization in sharks.{{cite news}}
The mulard duck, hybrid of the domestic pekin duck and domesticated muscovy ducks.
Killer bees were created in an attempt to breed tamer and more manageable bees. This was done by crossing a european honey bee and an african bee, but instead the offspring became more aggressive and highly defensive bees that had escaped into the wild.
In Australia, New Zealand and other areas where the Pacific Black Duck occurs, it is hybridised by the much more aggressive introduced Mallard. This is a concern to wildlife authorities throughout the affected area, as it is seen as Genetic pollution of the Black Duck gene pool.

Hybrids should not be confused with genetic chimeras such as that between sheep and goat known as the geep. Wider interspecific hybrids can be made via in vitro fertilization or somatic hybridization, however the resulting cells are not able to develop into a full organism. An example of interspecific hybrid cell lines is humster (hamster x human) cells.
Hybrid plantsPlant species hybridize more readily than animal species, and the resulting hybrids are more often fertile hybrids and may reproduce, though there still exist sterile hybrids and selective hybrid elimination where the offspring are less able to survive and are thus eliminated before they can reproduce. A number of plant species are the result of hybridization and polyploidy with many plant species easily cross pollinating and producing viable seeds, the distinction between each species is often maintained by geographical isolation or differences in the flowering period. Since plants hybridize frequently without much work, they are often created by humans in order to produce improved plants. These improvements can include the production of more or improved; seeds, fruits or other plant parts for consumption, or to make a plant more winter or heat hardy or improve its growth and/or appearance for use in horticulture. Much work is now being done with hybrids to produce more disease resistant plants for both agricultural and horticultural crops. In many groups of plants hybridization has been used to produce larger and more showy flowers and new flower colors.

Many plant genera and species have their origins in polyploidy. Autopolyploidy resulting from the sudden multiplication in the number of chromosomes in typical normal populations caused by unsuccessful separation of the chromosomes during meiosis. Tetraploids or plants with four sets of chromosomes are common in a number of different groups of plants and over time these plants can differentiate into distinct species from the normal diploid line. In Oenothera lamarchiana the diploid species has 14 chromosomes, this species has spontaneously given rise to plants with 28 chromosomes that have been given the name Oenthera gigas. Tetraploids can develop into a breeding population within the diploid population and when hybrids are formed with the diploid population the resulting offspring tend to be sterile triploids, thus effectively stopping the intermixing of genes between the two groups of plants (unless the diploids, in rare cases, produce unreduced gametes).
Another form of polyploidy called allopolyploidy occurs when two different species mate and produce hybrids. Usually the typical chromosome number is doubled in successful allopolyploid species, with four sets of chromosomes the genotypes can sort out to form a complete diploid set from the parent species, thus they can produce fertile offspring that can mate and reproduce with each other but can not back-cross with the parent species. Allopolyploidy in plants often gives them a condition called hybrid vigour, which results in plants that are larger and stronger growing than either of the two parent species. Allopolyploids are often more aggressive growing and can be invaders of new habitats.Sterility in a hybrid is often a result of chromosome number; if parents are of differing chromosome pair number, the offspring will have an odd number of chromosomes, leaving them unable to produce chromosomally balanced gametes.University of Colorado Principles of Genetics (MCDB 2150) Lecture 33: Chromosomal changes: Monosomy, Trisomy, Polyploidy, Structural Changes While this is a negative in a crop such as wheat, when growing a crop which produces no seeds would be pointless, it is an attractive attribute in some fruits. Bananas and seedless watermelon, for instance, are intentionally bred to be triploid, so that they will produce no seeds. Many hybrids are created by humans, but natural hybrids occur as well.
Heterosis{{Main}}
Hybrids are sometimes stronger than either parent variety, a phenomenon most common with plant hybrids, which when present is known as hybrid vigor (heterosis) or heterozygote advantage.Evaluating the utility of Arabidopsis thaliana as a model for understanding heterosis in hybrid crops
Journal	Euphytica
Publisher	Springer Netherlands
ISSN	0014-2336 (Print) 1573-5060 (Online)
Issue	Volume 156, Numbers 1-2 / July, 2007
DOI	10.1007/s10681-007-9362-1
Pages	157-171 A transgressive phenotype is a phenotype displaying more extreme characteristics than either of the parent lines.{{cite journal}} Plant breeders make use of a number of techniques to produce hybrids, including line breeding and the formation of complex hybrids. An economically important example is hybrid maize (corn), which provides a considerable seed yield advantage over open pollinated varieties. Hybrid seed dominates the commercial maize seed market in the United States, Canada and many other major maize producing countries.Smith C. Wayne. Corn: Origin, History, Technology, and Production. Wiley Series in Crop Science, 2004, p. 332.
Examples of hybridsIntergeneric plant hybrids include:

Leyland cypress, [x Cupressocyparis leylandii] hybrid between Monterey Cypress and Nootka Cypress.

Interspecific plant hybrids include:

Limequat, lime and kumquat hybrid.
Loganberry, a hybrid between raspberry and blackberry.
London plane, a hybrid between Platanus orientalis Oriental plane and Platanus occidentalis American plane (American sycamore). Thus forming [Platanus x acerifolia]
Magnolia × alba, an interspecific hybrid between Magnolia champaca and Magnolia montana
Peppermint, a hybrid between spearmint and water mint.
Tangelo, a hybrid of a Mandarin orange and a pomelo which may have been developed in Asia about 3,500 years ago.
Triticale, a wheat–rye hybrid.
Wheat; most modern and ancient wheat breeds are themselves hybrids. Bread wheat is a hexaploid hybrid of three wild grasses; durum (pasta) wheat is a tetraploid hybrid of two wild grasses.
Triangle of U: cabbage, mustard etc.
Grapefruit, hybrid between a pomelo and the Jamaican sweet orange.

Some natural hybrids:

Iris albicans, a sterile hybrid which spreads by rhizome division
Evening primrose, a flower which was the subject of famous experiments by Hugo de Vries on polyploidy and diploidy.

Some horticultural hybrids:

Dianthus x allwoodii, is a hybrid between Dianthus caryophyllus × Dianthus plumarius. This is an &#34;interspecific hybrid&#34; or hybrid between two species in the same genus.
x Heucherella tiarelloides, or Heuchera sanguinea × Tiarella cordifolia is an &#34;intergeneric hybrid&#34; a hybrid between two different genera.
Quercus x warei [Quercus robur x Quercus bicolor] Kindred Spirit Hybrid Oak


Hybrids in natureHybridisation between two closely related species is actually a common occurrence in nature but is also being greatly influenced by anthropogenic changes as well.{{cite journal}} Hybridization is a naturally occurring genetic process where individuals from two genetically distinct populations mate.{{cite book}} As stated above, it can occur both intraspecifically, between different distinct populations within the same species, and interspecifically, between two different species. Hybrids can be either sterile/not viable or viable/fertile. This affects the kind of effect that this hybrid will have on its and other populations that it interacts with.{{cite book}} Many hybrid zones are known where the ranges of two species meet, and hybrids are continually produced in great numbers. These hybrid zones are useful as biological model systems for studying the mechanisms of speciation (Hybrid speciation). Recently DNA analysis of a bear shot by a hunter in the North West Territories confirmed the existence of naturally-occurring and fertile grizzly–polar bear hybrids.{{cite news}} There have been reports of similar supposed hybrids, but this is the first to be confirmed by DNA analysis. In 1943, Clara Helgason described a male bear shot by hunters during her childhood. It was large and off-white with hair all over its paws. The presence of hair on the bottom of the feet suggests it was a natural hybrid of Kodiak and Polar bear.
Anthropogenic hybridizationAnthropogenic caused changes to the environment such as fragmentation and Introduced species are becoming more widespread.{{cite journal}} This is allowing another kind of hybridization that is more of the focus of conservation genetics to occur: anthropogenic hybridization. This anthropogenically caused hybridization increases the challenges in managing certain populations that are experiencing introgression.
Introduced species and habitat fragmentationHumans have been introducing species world wide to environments for a long time both directly such as establishing a population to be used as a biological control and indirectly such as accidental escapes of individuals out of agriculture. This causes drastic global effects on various populations with hybridization being one of the reasons introduced species can be so detrimental.{{cite journal}}
When habitats become broken apart, one of two things can occur, genetically speaking. The first is that populations that were once connected can be cut off from one another, preventing their genes from interacting. Occasionally, this will result in a population of one species breeding with a population of another species as a means of surviving such as the case with the red wolves. Their population numbers being so small, they needed another means of survival. Habitat fragmentation also led to the influx of generalist species into areas where they would not have been, leading to competition and in some cases interbreeding/incorporation of a population into another. In this way, habitat fragmentation is essentially an indirect method of introducing species to an area.
The hybridization continuumThere is a kind of continuum with three semi-distinct categories dealing with anthropogenic hybridization: hybridization without Introgression, hybridization with widespread introgression, and essentially a Hybrid swarm. Depending on where a population falls along this continuum, the management plans for that population will change. Hybridization is currently an area of great discussion within Wildlife management and habitat management fields. Global climate change, which is also anthropogenically caused, is creating other changes such as difference in population distributions which are indirect causes for an increase in anthropogenic hybridization.
ConsequencesHybridization can be a less discussed way toward Extinction then within detection of where a population lies along the hybrid continuum. The dispute of hybridization is how to manage the resulting hybrids. When a population experiences hybridization with substantial introgression, there still exists parent types of each set of individuals. When a complete hybrid swarm is created, all the individuals are hybrids.
Management of hybridsConservationists disagree on when is the proper time to give up on a population that is becoming a hybrid swarm or to try and save the still existing pure individuals. Once it becomes a complete mixture, we should look to conserve those hybrids to avoid their loss. Most leave it as a case-by-case basis, depending on detecting of hybrids within the group. It is nearly impossible to regulate hybridization via policy because hybridization can occur beneficially when it occurs "naturally" and there is the matter of protecting those previously mentioned hybrid swarms because if they are the only remaining evidence of prior species, they need to be conserved as well.{{Unreferenced section}}
In some species, hybridisation plays an important role in evolutionary biology. While most hybrids are disadvantaged as a result of genetic incompatibility, the fittest survive, regardless of species boundaries. They may have a beneficial combination of traits allowing them to exploit new habitats or to succeed in a marginal habitat where the two parent species are disadvantaged. This has been seen in experiments on sunflower species. Unlike mutation, which affects only one gene, hybridisation creates multiple variations across genes or gene combinations simultaneously. Successful hybrids could evolve into new species within 50-60 generations. This leads some scientists to speculate that life is a genetic continuum rather than a series of self-contained species.Where there are two closely related species living in the same area, less than 1 in 1000 individuals are likely to be hybrids because animals rarely choose a mate from a different species (otherwise species boundaries would completely break down). In some closely related species there are recognized "hybrid zones".Some species of Heliconius butterflies exhibit dramatic geographical polymorphism of their wing patterns, which act as aposematic signals advertising their unpalatability to potential predators. Where different-looking geographical races abut, inter-racial hybrids are common, healthy and fertile. Heliconius hybrids can breed with other hybrid individuals and with individuals of either parental race. These hybrid backcrosses are disadvantaged by natural selection because they lack the parental form's warning coloration, and are therefore not avoided by predators.A similar case in mammals is hybrid White-Tail/Mule Deer. The hybrids don't inherit either parent's escape strategy. White-tail Deer dash while Mule Deer bound. The hybrids are easier prey than the parent species.In birds, healthy Galapagos Finch hybrids are relatively common, but their beaks are intermediate in shape and less efficient feeding tools than the specialised beaks of the parental species so they lose out in the competition for food. Following a major storm in 1983, the local habitat changed so that new types of plants began to flourish, and in this changed habitat, the hybrids had an advantage over the birds with specialised beaks - demonstrating the role of hybridization in exploiting new ecological niches. If the change in environmental conditions is permanent or is radical enough that the parental species cannot survive, the hybrids become the dominant form. Otherwise, the parental species will re-establish themselves when the environmental change is reversed, and hybrids will remain in the minority.Natural hybrids may occur when a species is introduced into a new habitat. In Britain, there is hybridisation of native European Red Deer and introduced Chinese Sika Deer. Conservationists want to protect the Red Deer, but the environment favors the Sika Deer genes. There is a similar situation with White-headed Ducks and Ruddy Ducks.
Expression of parental traits in hybridsWhen two distinct types of organisms breed with each other, the resulting hybrids typically have intermediate traits (e.g., one parent has red flowers, the other has white, and the hybrid, pink flowers).McCarthy, Eugene M. 2006. Handbook of Avian Hybrids of the World. Oxford: Oxford University Press. Pp. 16-17. Commonly, hybrids also combine traits seen only separately in one parent or the other (e.g., a bird hybrid might combine the yellow head of one parent with the orange belly of the other). Most characteristics of the typical hybrid are of one of these two types, and so, in a strict sense, are not really new. However, an intermediate trait does differ from those seen in the parents (e.g., the pink flowers of the intermediate hybrid just mentioned are not seen in either of its parents). Likewise, combined traits are new when viewed as a combination.In a hybrid, any trait that falls outside the range of parental variation is termed heterotic. Heterotic hybrids do have new traits, that is, they are not intermediate. Positive heterosis produces more robust hybrids, they might be stronger or bigger; while the term negative heterosis refers to weaker or smaller hybrids.McCarthy, Eugene M. 2006. Handbook of Avian Hybrids of the World. Oxford: Oxford University Press. P. 17. Heterosis is common in both animal and plant hybrids. For example, hybrids between a lion and a tigress ("ligers") are much larger than either of the two progenitors, while a tigon (lioness × tiger) is smaller. Also the hybrids between the Common Pheasant (Phasianus colchicus) and domestic fowl (Gallus gallus) are larger than either of their parents, as are those produced between the Common Pheasant and hen Golden Pheasant (Chrysolophus pictus).Darwin, C. 1868. Variation of Animals and Plants under Domestication, vol. II, p. 125 Spurs are absent in hybrids of the former type, although present in both parents.Spicer, J. W. G. 1854. Note on hybrid gallinaceous birds. The Zoologist, 12: 4294-4296 (see p. 4295).When populations hybridize, often the first generation (F1) hybrids are very uniform. Typically, however, the individual members of subsequent hybrid generations are quite variable. High levels of variability in a natural population, then, are indicative of hybridity. Researchers use this fact to ascertain whether a population is of hybrid origin. Since such variability generally occurs only in later hybrid generations, the existence of variable hybrids is also an indication that the hybrids in question are fertile.{{Citation needed}}
Genetic mixing and extinction{{Main}}
Regionally developed ecotypes can be threatened with extinction when new alleles or genes are introduced that alter that ecotype. This is sometimes called genetic mixing.{{cite journal}} Hybridization and introgression of new genetic material can lead to the replacement of local genotypes if the hybrids are more fit and have breeding advantages over the indigenous ecotype or species. These hybridization events can result from the introduction of non native genotypes by humans or through habitat modification, bringing previously isolated species into contact. Genetic mixing can be especially detrimental for rare species in isolated habitats, ultimately affecting the population to such a degree that none of the originally genetically distinct population remains.Rhymer JM and Simberloff, D. (1996) Extinction by Hybridization and Introgression. Annual Review of Ecology and Systematics 27: 83-109 (doi:10.1146/annurev.ecolsys.27.1.83), http://links.jstor.org/sici?sici=0066-4162(1996)27%3C83:EBHAI%3E2.0.CO;2-A#abstractBrad M. Potts, Robert C. Barbour, Andrew B. Hingston (2001) Genetic Pollution from Farm Forestry using eucalypt species and hybrids; A report for the RIRDC/L&WA/FWPRDC; Joint Venture Agroforestry Program; RIRDC Publication No 01/114; RIRDC Project No CPF - 3A; ISBN 0 642 58336 6; ISSN 1440-6845; Australian Government, Rural Industrial Research and Development Corporation
Effect on biodiversity and food security{{Main}}
In agriculture and animal husbandry, the Green Revolution's use of conventional hybridization increased yields by breeding "high-yielding varieties". The replacement of locally indigenous breeds, compounded with unintentional cross-pollination and crossbreeding (genetic mixing), has reduced the gene pools of various wild and indigenous breeds resulting in the loss of genetic diversity.Devinder Sharma “Genetic Pollution: The Great Genetic Scandal”; Bulletin 28. hosted by www.farmedia.org Since the indigenous breeds are often well-adapted to local extremes in climate and have immunity to local pathogens this can be a significant genetic erosion of the gene pool for future breeding. Therefore, commercial plant geneticists strive to breed "widely adapted" cultivars to counteract this tendency.Troyer, A. Forrest. Breeding Widely Adapted Cultivars: Examples from Maize. Encyclopedia of Plant and Crop Science, 27 February 2004.
Limiting factorsA number of conditions exist that limit the success of hybridization, the most obvious is great genetic diversity between most species. But in animals and plants that are more closely related hybridization barriers can include morphological differences, differing times of fertility, mating behaviors and cues, physiological rejection of sperm cells or the developing embryo.{{Citation needed}}In plants, barriers to hybridization include blooming period differences, different pollinator vectors, inhibition of pollen tube growth, somatoplastic sterility, cytoplasmic-genic male sterility and structural differences of the chromosomes.Barriers to hybridization of Solanum bulbocastanumDun. and S. VerrucosumSchlechtd. and structural hybridity in their F1 plants
Journal	Euphytica [googilygoogilygoo..bwahahaa]
Publisher	Springer Netherlands
ISSN	0014-2336 (Print) 1573-5060 (Online)
Issue	Volume 25, Number 1 / January, 1976
Category	Articles
DOI	10.1007/BF00041523
Pages	1-10
Mythical and legendary hybrids{{Unreferenced section}}Ancient folktales often contain mythological creatures, sometimes these are described as hybrids (e.g., Hippogriff as the offspring of a griffin and a horse, and the Minotaur which is the offspring of Pasiphaë and a white bull). More often they are kind of chimera, i.e., a composite of the physical attributes of two or more kinds of animals, mythical beasts, and often humans, with no suggestion that they are the result of interbreeding, e.g., Harpies, mermaids, and centaurs.
See also{{Portal}}



References{{Reflist}}{{commons category}}
External links

Artificial Hybridisation - Artificial Hybridisation in orchids
Domestic Fowl Hybrids
Hybrid Mammals
Hybridisation in animals Evolution Revolution: Two Species Become One, Study Says (nationalgeographic.com)
Hybrids of wildcats with domestic cats
Scientists Create Butterfly Hybrid - Creation of new species through hybridization was thought to be common only in plants, and rare in animals.
What is a human admixed embryo?
Video of Mirror carp & Gold fish spawning at a fishing venue in France

{{evolution}}
{{popgen}}
{{Mammal hybrids}}{{Lysenkoism}}
{{Use dmy dates}}{{DEFAULTSORT:Hybrid (Biology)}}





ar:تهجين الأحياء
bn:সংকর জীব
bg:Хибрид
ca:Híbrid
cs:Křížení (biologie)
da:Krydsning
de:Hybride
et:Hübriid
el:Υβρίδιο
es:Híbrido (biología)
eo:Hibrido
eu:Hibrido (biologia)
fr:Hybride
ko:잡종
hr:Križanec
id:Hibrida
it:Ibrido
kk:Гибрид
ht:Ibrid
hu:Hibrid (biológia)
ms:Kacukan
nl:Hybride (biologie)
ja:雑種
no:Hybrid
pl:Mieszaniec
pt:Híbrido (biologia)
ro:Hibrid
ru:Гибрид
sl:Križanec
sr:Хибрид
fi:Risteymä
sv:Hybrid (biologi)
tr:Melez (biyoloji)
uk:Гібрид
zh:雜交種