titleLeast absolute deviations (LAD), also known as Least Absolute Errors (LAE), Least Absolute Value (LAV), or the L1 norm problem, is a mathematical optimization technique similar to the popular least squares technique that attempts to find a function which closely approximates a set of data. In the simple case of a set of (x,y) data, the approximation function is a simple "trend line" in two-dimensional Cartesian coordinates. The method minimizes the sum of absolute errors (SAE) (the sum of the absolute values of the vertical "residuals" between points generated by the function and corresponding points in the data). The least absolute deviations estimate also arises as the maximum likelihood estimate if the errors have a Laplace distribution.
Formulation of the problem
Suppose that the data set consists of the points (xi, yi) with i = 1, 2, ..., n. We want to find a function f such that  f(x_i)\approx y_i.To attain this goal, we suppose that the function f is of a particular form containing some parameters which need to be determined. For instance, the simplest form would be linear: f(x) = bx + c, where b and c parameters whose values are not known but which we would like to estimate. Less simply, suppose that f(x) is quadratic, meaning that f(x) = ax2 + bx + c, where a, b and c are not yet known. (More generally, there could be not just one explanator x, but rather multiple explanators, all appearing as arguments of the function f.)We now seek estimated values of the unknown parameters that minimize the sum of the absolute values of the residuals:

 S = \sum_{i=1}^n |y_i - f(x_i)|. 


Contrasting Least Squares with Least Absolute Deviations
The following is a table contrasting some properties of the method of least absolute deviations with those of the method of least squares (for non-singular problems).

Least Squares Regression  Least Absolute Deviations Regression 
Not very robust Robust 
Stable solution Unstable solution 
Always one solution Possibly multiple solutions 
The method of least absolute deviations finds applications in many areas, due to its robustness compared to the least squares method. Least absolute deviations is robust in that it is resistant to outliers in the data. This may be helpful in studies where outliers may be safely and effectively ignored. If it is important to pay attention to any and all outliers, the method of least squares is a better choice.The instability property of the method of least absolute deviations means that, for a small horizontal adjustment of a datum, the regression line may jump a large amount. The method has continuous solutions for some data configurations; however, by moving a datum a small amount, one could "jump past" a configuration which has multiple solutions that span a region. After passing this region of solutions, the least absolute deviations line has a slope that may differ greatly from that of the previous line. In contrast, the least squares solutions is stable in that, for any small adjustment of a data point, the regression line will always move only slightly; that is, the regression parameters are continuous functions of the data. Lastly, for a given data set, the method of least absolute deviations may produce multiple solutions, whereas the method of least squares always produces only one solution (the regression line is unique).For a set of applets that demonstrate these differences, see the following site: http://www.math.wpi.edu/Course_Materials/SAS/lablets/7.3/73_choices.html
Other properties
There exist other unique properties of the least absolute deviations line. In the case of a set of (x,y) data, the least absolute deviations line will always pass through at least two of the data points, unless there are multiple solutions. If multiple solutions exist, then the region of valid least absolute deviations solutions will be bounded by at least two lines, each of which passes through at least two data points. More generally, if there are k regressors (including the constant), then at least one optimal regression surface will pass through k of the data points.Branham, R. L., Jr., "Alternatives to least squares", Astronomical Journal 87, June 1982, 928-937. http://adsabs.harvard.edu/full/1982AJ.....87..928B at SAO/NASA Astrophysics Data System (ADS){{rp}}This "latching" of the line to the data points can help to understand the "instability" property: if the line always latches to at least two points, then the line will jump between different sets of points as the data points are altered. The "latching" also helps to understand the "robustness" property: if there exists an outlier, and a least absolute deviations line must latch onto two data points, the outlier will most likely not be one of those two points because that will not minimize the sum of absolute deviations in most cases.One known case in which multiple solutions exist is a set of points symmetric about a horizontal line, as shown in Figure A below.To understand why there are multiple solutions in the case shown in Figure A, consider the pink line in the green region. Its sum of absolute errors is some value S. If one were to tilt the line upward slightly, while still keeping it within the green region, the sum of errors would still be S. It would not change because the distance from each point to the line grows on one side of the line, while the distance to each point on the opposite side of the line diminishes by exactly the same amount. Thus the sum of absolute errors remains the same. Also, since one can tilt the line in infinitely small increments, this also shows that if there is more than one solution, there are infinitely many solutions.
Variations, extensions, specializationsThe least absolute deviation problem may be extended to include constraints and regularization, e.g., a linear model with linear constraints:{{Cite journal}}

minimize S(\mathbf{x}) = \sum_i | \mathbf{a}'_i \mathbf{x} + b_i |
subject to, e.g., \mathbf{a}'_i \mathbf{x} - b_i \leq 0

Regularization with LASSO may also be combined with LAD.{{Cite conference}}
Solving Methods
Though the idea of least absolute deviations regression is just as straightforward as that of least squares regression, the least absolute deviations line is not as simple to compute efficiently. Unlike least squares regression, least absolute deviations regression does not have an analytical solving method. Therefore, an iterative approach is required. The following is an enumeration of some least absolute deviations solving methods.

Simplex-based methods (such as the Barrodale-Roberts algorithm{{Cite journal}})
Iteratively re-weighted least squares{{Cite journal}}
Wesolowsky’s direct descent method{{Cite journal}}
Li-Arce’s maximum likelihood approach{{Cite journal}}
Check all combinations of point-to-point lines for minimum sum of errors

Simplex-based methods are the “preferred” way to solve the least absolute deviations problem. A Simplex method is a method for solving a problem in linear programming. The most popular algorithm is the Barrodale-Roberts modified Simplex algorithm. The algorithms for IRLS, Wesolowsky's Method, and Li's Method can be found in Appendix A of this document,William A. Pfeil, 
Statistical Teaching Aids, Bachelor of Science thesis, Worcester Polytechnic Institute, 2006
among other methods. Checking all combinations of lines traversing any two (x,y) data points is another method of finding the least absolute deviations line. Since it is known that at least one least absolute deviations line traverses at least two data points, this method will find a line by comparing the SAE of each line, and choosing the line with the smallest SAE. In addition, if multiple lines have the same, smallest SAE, then the lines outline the region of multiple solutions. Though simple, this final method is inefficient for large sets of data.
Solving using linear programming
The problem can be solved using any linear programming technique on the following problem specification. We wish to 

 \text{Minimize} \sum_{i=1}^n |y_i - a_0 - a_1x_{i1} - a_2x_{i2} - \cdots - a_kx_{ik}|

with respect to the choice of the values of the parameters a_0,..., a_k, where yi is the value of the ith observation of the dependent variable, and xij is the value of the ith observation of the jth independent variable (j = 1,...,k).   We rewrite this problem in terms of artificial variables ui as 

 \text{Minimize} \sum_{i=1}^n u_i



with respect to a_0,..., a_k and u_1,..., u_n



subject to



 u_i \ge y_i - a_0 - a_1x_{i1} - a_2x_{i2} - \cdots - a_kx_{ik} \,\ \,\ \,\ \,\ \,\ \text{for}  \,\ i=1,...,n



 u_i \ge -[y_i - a_0 - a_1x_{i1} - a_2x_{i2} - \cdots - a_kx_{ik}] \,\ \,\ \text{for} \,\ i=1,...,n.

These constraints have the effect of forcing each u_i to equal |y_i - a_0 - a_1x_{i1} - a_2x_{i2} - \cdots - a_kx_{ik}| upon being minimized, so the objective function is equivalent to the original objective function. Since this version of the problem statement does not contain the absolute value operator, it is in a format that can be solved with any linear programming package.
See also

Regression analysis
Linear regression model
Absolute deviation
Ordinary least squares


References{{Reflist}}
External links

{{Cite journal}}
{{Cite journal}}
{{Cite journal}}
{{Cite journal}}

{{DEFAULTSORT:Least Absolute Deviations}}


ru:Метод наименьших модулей