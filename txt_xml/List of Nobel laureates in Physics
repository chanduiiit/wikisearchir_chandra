titleThe Nobel Prize in Physics ({{lang-sv}}) is awarded annually by the Royal Swedish Academy of Sciences to scientists in the various fields of physics. It is one of the five Nobel Prizes established by the 1895 will of Alfred Nobel (who died in 1896), awarded for outstanding contributions in physics.{{cite web}} As dictated by Nobel's will, the award is administered by the Nobel Foundation and awarded by a committee that consists of five members elected by the Royal Swedish Academy of Sciences.{{cite web}} The first Nobel Prize in Physics was awarded in 1901 to Wilhelm Conrad Röntgen, of Germany. Each recipient receives a medal, a diploma and a monetary award prize that has varied throughout the years.{{cite web}} In 1901, Röntgen received 150,782 SEK, which is equal to 7,731,004 SEK in December 2007. In 2008, the prize was awarded to Makoto Kobayashi, Toshihide Maskawa and Yoichiro Nambu who shared the prize amount of 10,000,000 SEK (slightly more than €1&nbsp;million, or US$1.4&nbsp;million).{{cite web}} The award is presented in Stockholm at an annual ceremony on December 10, the anniversary of Nobel's death.{{cite web}}John Bardeen is the only laureate to win the prize twice—in 1956 and 1972. One other has also won Nobel Prizes in other subjects: Marie Curie (physics in 1903, chemistry in 1911). William Lawrence Bragg is the youngest ever Nobel Laureate; he won the prize in 1915 at the age of 25.{{cite web}} Two women have won the prize: Marie Curie and Maria Goeppert-Mayer (1963), which is the least of any of the original five Nobel Prizes.{{cite web}} As of 2011, the prize has been awarded to 191 individuals. There have been six years in which the Nobel Prize in Physics was not awarded (1916, 1931, 1934, 1940–1942).


Laureates

Year Laureate{{ref}} Country{{ref}} Rationale{{ref}} 
1901  Wilhelm Conrad Röntgen Germany &#34;in recognition of the extraordinary services he has rendered by the discovery of the remarkable rays subsequently named after him&#34;{{cite web}} 
1902  Hendrik Lorentz Netherlands &#34;in recognition of the extraordinary service they rendered by their researches into the influence of magnetism upon radiation phenomena&#34;{{cite web}} 
 Pieter Zeeman Netherlands 
1903  Antoine Henri Becquerel France &#34;[for] his discovery of spontaneous radioactivity&#34;{{cite web}} 
 Pierre Curie France &#34;[for] their joint researches on the radiation phenomena discovered by Professor Henri Becquerel&#34; 
 Marie Curie PolandFrance 
1904  John William Strutt United Kingdom &#34;for his investigations of the densities of the most important gases and for his discovery of argon in connection with these studies&#34;{{cite web}} 
1905  Philipp Eduard Anton von Lenard Germany-Hungary &#34;for his work on cathode rays&#34;{{cite web}} 
1906  Joseph John Thomson United Kingdom &#34;[for] his theoretical and experimental investigations on the conduction of electricity by gases&#34;{{cite web}} 
1907  Albert Abraham Michelson United States &#34;for his optical precision instruments and the spectroscopic and metrological investigations carried out with their aid&#34;{{cite web}} 
1908  Gabriel Lippmann France &#34;for his method of reproducing colours photographically based on the phenomenon of interference&#34;{{cite web}} 
1909  Guglielmo Marconi Italy &#34;[for] their contributions to the development of wireless telegraphy&#34;{{cite web}} 
 Karl Ferdinand Braun Germany 
1910  Johannes Diderik van der Waals Netherlands &#34;for his work on the equation of state for gases and liquids&#34;{{cite web}} 
1911  Wilhelm Wien Germany &#34;for his discoveries regarding the laws governing the radiation of heat&#34;{{cite web}} 
1912  Nils Gustaf Dalén Sweden &#34;for his invention of automatic valves designed to be used in combination with gas accumulators in lighthouses and buoys&#34;{{cite web}} 
1913  Heike Kamerlingh-Onnes Netherlands &#34;for his investigations on the properties of matter at low temperatures which led, inter alia, to the production of liquid helium&#34;{{cite web}} 
1914  Max von Laue Germany &#34;For his discovery of the diffraction of X-rays by crystals&#34;,{{cite web}} an important step in the development of X-ray spectroscopy. 
1915  William Henry Bragg United Kingdom &#34;For their services in the analysis of crystal structure by means of X-rays&#34;,{{cite web}} an important step in the development of X-ray crystallography 
 William Lawrence Bragg United Kingdom 
1916 Not awarded 
1917  Charles Glover Barkla United Kingdom &#34;For his discovery of the characteristic Röntgen radiation of the elements&#34;,{{cite web}} another important step in the development of X-ray spectroscopy 
1918  Max Planck Germany &#34;[for] the services he rendered to the advancement of Physics by his discovery of energy quanta&#34;{{cite web}} 
1919  Johannes Stark Germany &#34;for his discovery of the Doppler effect in canal rays and the splitting of spectral lines in electric fields&#34;{{cite web}} 
1920  Charles Édouard Guillaume Switzerland &#34;[for] the service he has rendered to precision measurements in physics by his discovery of anomalies in nickel-steel alloys&#34;{{cite web}} 
1921  Albert Einstein Germany &#34;for his services to Theoretical Physics, and especially for his discovery of the law of the photoelectric effect&#34;{{cite web}} 
1922  Niels Bohr Denmark &#34;for his services in the investigation of the structure of atoms and of the radiation emanating from them&#34;{{cite web}} 
1923  Robert Andrews Millikan United States &#34;for his work on the elementary charge of electricity and on the photoelectric effect&#34;{{cite web}} 
1924  Manne Siegbahn Sweden &#34;for his discoveries and research in the field of X-ray spectroscopy&#34;{{cite web}} 
1925  James Franck Germany &#34;for their discovery of the laws governing the impact of an electron upon an atom&#34;{{cite web}} 
 Gustav Hertz Germany 
1926  Jean Baptiste Perrin France &#34;for his work on the discontinuous structure of matter, and especially for his discovery of sedimentation equilibrium&#34;{{cite web}} 
1927  Arthur Holly Compton United States &#34;for his discovery of the effect named after him&#34;{{cite web}} 
 Charles Thomson Rees Wilson United Kingdom &#34;for his method of making the paths of electrically charged particles visible by condensation of vapour&#34; 
1928  Owen Willans Richardson United Kingdom &#34;for his work on the thermionic phenomenon and especially for the discovery of the law named after him&#34;{{cite web}} 
1929  Prince Louis-Victor Pierre Raymond de Broglie France &#34;for his discovery of the wave nature of electrons&#34;{{cite web}} 
1930  Chandrasekhara Venkata Raman India &#34;for his work on the scattering of light and for the discovery of the effect named after him&#34;{{cite web}} 
1931 Not awarded 
1932  Werner Heisenberg Germany &#34;for the creation of quantum mechanics, the application of which has, inter alia, led to the discovery of the allotropic forms of hydrogen&#34;{{cite web}} 
1933  Erwin Schrödinger Austria &#34;for the discovery of new productive forms of atomic theory&#34;{{cite web}} 
 Paul Dirac United Kingdom 
1934 Not awarded 
1935  James Chadwick United Kingdom &#34;for the discovery of the neutron&#34;{{cite web}} 
1936  Victor Francis Hess Austria &#34;for his discovery of cosmic radiation&#34;{{cite web}} 
 Carl David Anderson United States &#34;for his discovery of the positron&#34; 
1937  Clinton Joseph Davisson United States &#34;for their experimental discovery of the diffraction of electrons by crystals&#34;{{cite web}} 
 George Paget Thomson United Kingdom 
1938  Enrico Fermi Italy &#34;for his demonstrations of the existence of new radioactive elements produced by neutron irradiation, and for his related discovery of nuclear reactions brought about by slow neutrons&#34;{{cite web}} 
1939  Ernest Lawrence United States &#34;for the invention and development of the cyclotron and for results obtained with it, especially with regard to artificial radioactive elements&#34;{{cite web}} 
1940 Not awarded 
1941 Not awarded 
1942 Not awarded 
1943  Otto Stern United States &#34;for his contribution to the development of the molecular ray method and his discovery of the magnetic moment of the proton&#34;{{cite web}} 
1944  Isidor Isaac Rabi United States &#34;for his resonance method for recording the magnetic properties of atomic nuclei&#34;{{cite web}} 
1945  Wolfgang Pauli Austria &#34;for the discovery of the Exclusion Principle, also called the Pauli principle&#34;{{cite web}} 
1946  Percy Williams Bridgman United States &#34;for the invention of an apparatus to produce extremely high pressures, and for the discoveries he made there within the field of high pressure physics&#34;{{cite web}} 
1947  Edward Victor Appleton United Kingdom &#34;for his investigations of the physics of the upper atmosphere especially for the discovery of the so-called Appleton layer&#34;{{cite web}} 
1948  Patrick Maynard Stuart Blackett United Kingdom &#34;for his development of the Wilson cloud chamber method, and his discoveries therewith in the fields of nuclear physics and cosmic radiation&#34;{{cite web}} 
1949  Hideki Yukawa Japan &#34;for his prediction of the existence of mesons on the basis of theoretical work on nuclear forces&#34;{{cite web}} 
1950  Cecil Frank Powell United Kingdom &#34;for his development of the photographic method of studying nuclear processes and his discoveries regarding mesons made with this method&#34;{{cite web}} 
1951  John Douglas Cockcroft United Kingdom &#34;for their pioneer work on the transmutation of atomic nuclei by artificially accelerated atomic particles&#34;{{cite web}} 
  Ernest Thomas Sinton Walton Ireland 
1952  Felix Bloch United States &#34;for their development of new methods for nuclear magnetic precision measurements and discoveries in connection therewith&#34;{{cite web}} 
 Edward Mills Purcell United States 
1953  Frits Zernike Netherlands &#34;for his demonstration of the phase contrast method, especially for his invention of the phase contrast microscope&#34;{{cite web}} 
1954  Max Born United Kingdom &#34;for his fundamental research in quantum mechanics, especially for his statistical interpretation of the wavefunction&#34;{{cite web}} 
 Walther Bothe West Germany &#34;for the coincidence method and his discoveries made therewith&#34; 
1955  Willis Eugene Lamb United States &#34;for his discoveries concerning the fine structure of the hydrogen spectrum&#34;{{cite web}} 
 Polykarp Kusch United States &#34;for his precision determination of the magnetic moment of the electron&#34; 
1956  John Bardeen United States &#34;for their researches on semiconductors and their discovery of the transistor effect&#34;{{cite web}} 
 Walter Houser Brattain United States 
 William Bradford Shockley United States 
1957  Tsung-Dao Lee China &#34;for their penetrating investigation of the so-called parity laws which has led to important discoveries regarding the elementary particles&#34;{{cite web}} 
 Chen Ning Yang China 
1958  Pavel Alekseyevich Cherenkov Soviet Union &#34;for the discovery and the interpretation of the Cherenkov effect&#34;{{cite web}} 
 Ilya Frank Soviet Union 
 Igor Yevgenyevich Tamm Soviet Union 
1959  Owen Chamberlain United States &#34;for their discovery of the antiproton&#34;{{cite web}} 
 Emilio Gino Segrè Italy 
1960  Donald Arthur Glaser United States &#34;for the invention of the bubble chamber&#34;{{cite web}} 
1961  Robert Hofstadter United States &#34;for his pioneering studies of electron scattering in atomic nuclei and for his thereby achieved discoveries concerning the structure of the nucleons&#34;{{cite web}} 
 Rudolf Ludwig Mössbauer West Germany &#34;for his researches concerning the resonance absorption of gamma radiation and his discovery in this connection of the effect which bears his name&#34; 
1962  Lev Davidovich Landau Soviet Union &#34;for his pioneering theories for condensed matter, especially liquid helium&#34;{{cite web}} 
1963  Eugene Paul Wigner Hungary – United States &#34;for his contributions to the theory of the atomic nucleus and the elementary particles, particularly through the discovery and application of fundamental symmetry principles&#34;{{cite web}} 
 Maria Goeppert-Mayer United States &#34;for their discoveries concerning nuclear shell structure&#34; 
 J. Hans D. Jensen West Germany 
1964  Nicolay Gennadiyevich Basov Soviet Union &#34;for fundamental work in the field of quantum electronics, which has led to the construction of oscillators and amplifiers based on the maser-laser principle&#34;{{cite web}} 
 Alexander Prokhorov Soviet Union 
 Charles Hard Townes United States 
1965  Richard Phillips Feynman United States &#34;for their fundamental work in quantum electrodynamics, with deep-ploughing consequences for the physics of elementary particles&#34;{{cite web}} 
 Julian Schwinger United States 
 Sin-Itiro Tomonaga Japan 
1966  Alfred Kastler France &#34;for the discovery and development of optical methods for studying Hertzian resonances in atoms&#34;{{cite web}} 
1967  Hans Albrecht Bethe United States &#34;for his contributions to the theory of nuclear reactions, especially his discoveries concerning the energy production in stars&#34;{{cite web}} 
1968  Luis Walter Alvarez United States &#34;for his decisive contributions to elementary particle physics, in particular the discovery of a large number of resonance states, made possible through his development of the technique of using hydrogen bubble chamber and data analysis&#34;{{cite web}} 
1969  Murray Gell-Mann United States &#34;for his contributions and discoveries concerning the classification of elementary particles and their interactions&#34;{{cite web}} 
1970  Hannes Olof Gösta Alfvén Sweden &#34;for fundamental work and discoveries in magneto-hydrodynamics with fruitful applications in different parts of plasma physics&#34;{{cite web}} 
 Louis Néel France &#34;for fundamental work and discoveries concerning antiferromagnetism and ferrimagnetism which have led to important applications in solid state physics&#34; 
1971  Dennis Gabor Hungary – United Kingdom &#34;for his invention and development of the holographic method&#34;{{cite web}} 
1972  John Bardeen United States &#34;for their jointly developed theory of superconductivity, usually called the BCS-theory&#34;{{cite web}} 
 Leon Neil Cooper United States 
 John Robert Schrieffer United States 
1973  Leo Esaki Japan &#34;for their experimental discoveries regarding tunneling phenomena in semiconductors and superconductors, respectively&#34;{{cite web}} 
 Ivar Giaever United StatesNorway 
 Brian David Josephson United Kingdom &#34;for his theoretical predictions of the properties of a supercurrent through a tunnel barrier, in particular those phenomena which are generally known as the Josephson effect&#34; 
1974  Martin Ryle United Kingdom &#34;for their pioneering research in radio astrophysics: Ryle for his observations and inventions, in particular of the aperture synthesis technique, and Hewish for his decisive role in the discovery of pulsars&#34;{{cite web}} 
 Antony Hewish United Kingdom 
1975  Aage Bohr Denmark &#34;for the discovery of the connection between collective motion and particle motion in atomic nuclei and the development of the theory of the structure of the atomic nucleus based on this connection&#34;{{cite web}} 
 Ben Roy Mottelson Denmark 
 Leo James Rainwater United States 
1976  Burton Richter United States &#34;for their pioneering work in the discovery of a heavy elementary particle of a new kind&#34;{{cite web}} 
 Samuel Chao Chung Ting United States 
1977  Philip Warren Anderson United States &#34;for their fundamental theoretical investigations of the electronic structure of magnetic and disordered systems&#34;{{cite web}} 
 Nevill Francis Mott United Kingdom 
 John Hasbrouck Van Vleck United States 
1978  Pyotr Leonidovich Kapitsa Soviet Union &#34;for his basic inventions and discoveries in the area of low-temperature physics&#34;{{cite web}} 
 Arno Allan Penzias United States &#34;for their discovery of cosmic microwave background radiation&#34; 
 Robert Woodrow Wilson United States 
1979  Sheldon Lee Glashow United States &#34;for their contributions to the theory of the unified weak and electromagnetic interaction between elementary particles, including, inter alia, the prediction of the weak neutral current&#34;{{cite web}} 
 Abdus Salam Pakistan 
 Steven Weinberg United States 
1980  James Watson Cronin United States &#34;for the discovery of violations of fundamental symmetry principles in the decay of neutral K-mesons&#34;{{cite web}} 
 Val Logsdon Fitch United States 
1981  Nicolaas Bloembergen United States &#34;for their contribution to the development of laser spectroscopy&#34;{{cite web}} 
 Arthur Leonard Schawlow United States 
 Kai Manne Börje Siegbahn Sweden &#34;for his contribution to the development of high-resolution electron spectroscopy&#34; 
1982  Kenneth G. Wilson United States &#34;for his theory for critical phenomena in connection with phase transitions&#34;{{cite web}} 
1983  Subrahmanyan Chandrasekhar IndiaUnited States &#34;for his theoretical studies of the physical processes of importance to the structure and evolution of the stars&#34;{{cite web}} 
 William Alfred Fowler United States &#34;for his theoretical and experimental studies of the nuclear reactions of importance in the formation of the chemical elements in the universe&#34; 
1984  Carlo Rubbia Italy &#34;for their decisive contributions to the large project, which led to the discovery of the field particles W and Z, communicators of weak interaction&#34;{{cite web}} 
 Simon van der Meer Netherlands 
1985  Klaus von Klitzing West Germany &#34;for the discovery of the quantized Hall effect&#34;{{cite web}} 
1986  Ernst Ruska West Germany &#34;for his fundamental work in electron optics, and for the design of the first electron microscope&#34;{{cite web}} 
 Gerd Binnig West Germany &#34;for their design of the scanning tunneling microscope&#34; 
 Heinrich Rohrer Switzerland 
1987  Johannes Georg Bednorz West Germany &#34;for their important break-through in the discovery of superconductivity in ceramic materials&#34;{{cite web}} 
 Karl Alexander Müller Switzerland 
1988  Leon Max Lederman United States &#34;for the neutrino beam method and the demonstration of the doublet structure of the leptons through the discovery of the muon neutrino&#34;{{cite web}} 
 Melvin Schwartz United States 
 Jack Steinberger United States 
1989  Norman Foster Ramsey United States &#34;for the invention of the separated oscillatory fields method and its use in the hydrogen maser and other atomic clocks&#34;{{cite web}} 
 Hans Georg Dehmelt United States &#34;for the development of the ion trap technique&#34; 
 Wolfgang Paul West Germany 
1990  Jerome I. Friedman United States &#34;for their pioneering investigations concerning deep inelastic scattering of electrons on protons and bound neutrons, which have been of essential importance for the development of the quark model in particle physics&#34;{{cite web}} 
 Henry Way Kendall United States 
 Richard E. Taylor Canada 
1991  Pierre-Gilles de Gennes France &#34;for discovering that methods developed for studying order phenomena in simple systems can be generalized to more complex forms of matter, in particular to liquid crystals and polymers&#34;{{cite web}} 
1992  Georges Charpak France &#34;for his invention and development of particle detectors, in particular the multiwire proportional chamber&#34;{{cite web}} 
1993  Russell Alan Hulse United States &#34;for the discovery of a new type of pulsar, a discovery that has opened up new possibilities for the study of gravitation&#34;{{cite web}} 
 Joseph Hooton Taylor, Jr. United States 
1994  Bertram Brockhouse Canada &#34;for the development of neutron spectroscopy&#34; and &#34;for pioneering contributions to the development of neutron scattering techniques for studies of condensed matter&#34;{{cite web}} 
 Clifford Glenwood Shull United States &#34;for the development of the neutron diffraction technique&#34; and &#34;for pioneering contributions to the development of neutron scattering techniques for studies of condensed matter&#34; 
1995  Martin Lewis Perl United States &#34;for the discovery of the tau lepton&#34; and &#34;for pioneering experimental contributions to lepton physics&#34;{{cite web}} 
 Frederick Reines United States &#34;for the detection of the neutrino&#34; and &#34;for pioneering experimental contributions to lepton physics&#34; 
1996  David Morris Lee United States &#34;for their discovery of superfluidity in helium-3&#34;{{cite web}} 
 Douglas D. Osheroff United States 
 Robert Coleman Richardson United States 
1997  Steven Chu United States  &#34;for development of methods to cool and trap atoms with laser light.&#34;{{cite web}} 
 Claude Cohen-Tannoudji France 
 William Daniel Phillips United States 
1998  Robert B. Laughlin United States &#34;for their discovery of a new form of quantum fluid with fractionally charged excitations&#34;{{cite web}} 
 Horst Ludwig Störmer Germany 
 Daniel Chee Tsui United States 
1999  Gerard 't Hooft Netherlands &#34;for elucidating the quantum structure of electroweak interactions in physics&#34;{{cite web}} 
 Martinus J. G. Veltman Netherlands 
2000  Zhores Ivanovich Alferov Russia &#34;for developing semiconductor heterostructures used in high-speed- and optoelectronics&#34;{{cite web}} 
 Herbert Kroemer Germany 
 Jack St. Clair Kilby United States &#34;for his part in the invention of the integrated circuit&#34; 
2001  Eric Allin Cornell United States &#34;for the achievement of Bose-Einstein condensation in dilute gases of alkali atoms, and for early fundamental studies of the properties of the condensates&#34;{{cite web}} 
Carl Edwin Wieman United States 
 Wolfgang Ketterle Germany 
2002  Raymond Davis, Jr. United States &#34;for pioneering contributions to astrophysics, in particular for the detection of cosmic neutrinos&#34;{{cite web}} 
 Masatoshi Koshiba Japan 
 Riccardo Giacconi United States &#34;for pioneering contributions to astrophysics, which have led to the discovery of cosmic X-ray sources&#34; 
2003  Alexei Alexeyevich Abrikosov RussiaUnited States &#34;for pioneering contributions to the theory of superconductors and superfluids&#34;{{cite web}} 
 Vitaly Lazarevich Ginzburg Russia 
 Anthony James Leggett United KingdomUnited States 
2004  David J. Gross United States &#34;for the discovery of asymptotic freedom in the theory of the strong interaction&#34;{{cite web}} 
 Hugh David Politzer United States 
 Frank Wilczek United States 
2005  Roy J. Glauber United States &#34;for his contribution to the quantum theory of optical coherence&#34;{{cite web}} 
 John L. Hall United States &#34;for their contributions to the development of laser-based precision spectroscopy, including the optical frequency comb technique&#34; 
 Theodor W. Hänsch Germany 
2006  John C. Mather United States &#34;for their discovery of the blackbody form and anisotropy of the cosmic microwave background radiation&#34;{{cite web}} 
 George F. Smoot United States 
2007  Albert Fert France &#34;for the discovery of giant magnetoresistance&#34;{{cite web}} 
 Peter Grünberg Germany 
2008  Makoto Kobayashi Japan &#34;for the discovery of the origin of the broken symmetry which predicts the existence of at least three families of quarks in nature&#34;{{cite web}} 
 Toshihide Maskawa Japan 
 Yoichiro Nambu United States &#34;for the discovery of the mechanism of spontaneous broken symmetry in subatomic physics&#34; 
2009  Charles K. Kao Hong KongUnited KingdomUnited States &#34;for groundbreaking achievements concerning the transmission of light in fibers for optical communication&#34;{{cite web}} 
 Willard S. Boyle CanadaUnited States &#34;for the invention of an imaging semiconductor circuit – the CCD sensor&#34; 
 George E. Smith United States 
2010  Andre Geim RussiaNetherlands &#34;for groundbreaking experiments regarding the two-dimensional material graphene&#34;{{cite web}} 
 Konstantin Novoselov RussiaUnited Kingdom 
2011  Saul Perlmutter United States &#34;for the discovery of the accelerating expansion of the Universe through observations of distant supernovae&#34;{{cite web}}The Nobel Prize in Physics 2011. Perlmutter got half the prize, and the other half was shared between Schmidt and Riess. 
 Brian P. Schmidt United StatesAustralia 
 Adam G. Riess United States 

See also

Nobel laureates by country


References

General
{{refbegin}}

{{cite web}}
{{cite web}}
{{refend}}

Specific
{{reflist}}

Notes
{{refbegin}}
{{note}}A. The form and spelling of the names in the name column is according to nobelprize.org, the official website of the Nobel Foundation. Alternative spellings and name forms, where they exist, are given at the articles linked from this column. Where available, an image of each Nobel Laureate is provided. For the official pictures provided by the Nobel Foundation, see the pages for each Nobel Laureate at nobelprize.org.{{note}}B. The information in the country column is according to nobelprize.org, the official website of the Nobel Foundation. This information may not necessarily reflect the recipient's birthplace or citizenship.{{note}}C. The citation for each award is quoted (not always in full) from nobelprize.org, the official website of the Nobel Foundation. The links in this column are to articles (or sections of articles) on the history and areas of physics for which the awards were presented. The links are intended only as a guide and explanation. For a full account of the work done by each Nobel Laureate, please see the biography articles linked from the name column.{{refend}}
External links{{commons category}}

Official website of the Royal Swedish Academy of Sciences
Official website of the Nobel Foundation

{{Nobel Prizes}}{{featured list}}

{{Link FL}}
{{Link FL}}
{{Link FL}}
{{Link GA}}
{{Link FL}}als:Liste der Nobelpreisträger für Physik
de:Liste der Nobelpreisträger für Physik
es:Anexo:Ganadores del Premio Nobel de Física
fa:فهرست برندگان جایزه نوبل فیزیک
io:Nobel-premiarii en fiziko
ku:Xelatwergirên Xelata Nobelê a Fîzîkê
lb:Lëscht Nobelpräis Physik
no:Liste over nobelprisvinnere i fysikk
ro:Premiul Nobel pentru Fizică
sq:Lista e fituesve të Çmimit Nobel për Fizikë
scn:Vincitura dû Nobel pâ fìsica (crunulòggicu)
tr:Nobel Fizik Ödülü sahipleri listesi
uk:Список лауреатів Нобелівської премії в галузі фізики
zh:诺贝尔物理学奖得主列表