title{{Programming language lists}}The aim of this list of programming languages is to include all notable programming languages in existence, both those in current use and historical ones, in alphabetical order, except for dialects of BASIC and esoteric programming languages.

Note: Dialects of BASIC have been moved to the separate List of BASIC dialects.
Note: This page does not list esoteric programming languages.

{{CompactTOC8}}
A{{col-begin}}
{{col-3}}

A# .NET
A# (Axiom)
A-0 System
A+
A++
ABAP
ABC
ABC ALGOL
ABLE
ABSET
ABSYS
Abundance
ACC
Accent
Ace DASL
ACT-III
Action!
{{col-3}}

ActionScript
Ada
Adenine
Agda
Agora
AIMMS
Alef
ALF
ALGOL 58
ALGOL 60
ALGOL 68
Alice
Alma-0
AmbientTalk
Amiga E
AMOS
AMPL
{{col-3}}

APL
AppleScript
Arc
ARexx
Argus
AspectJ
Assembly language
ATS
Ateji PX
AutoHotkey
Autocoder
AutoIt
AutoLISP / Visual LISP
Averest
AWK
Axum
{{col-end}}
B{{col-begin}}
{{col-3}}

B
Babbage
Bash
BASIC
bc
BCPL
BeanShell
Batch (Windows/Dos)
{{col-3}}

Bertrand
BETA
Bigwig
Bistro
BitC
BLISS
Blue
Bon
{{col-3}}

Boo
Boomerang
Bourne shell (including bash and ksh)
BREW
BPEL
BUGSYS
BuildProfessional
{{col-end}}
C{{col-begin}}
{{col-3}}

C
C--
C++ - ISO/IEC 14882
C# - ISO/IEC 23270
C/AL
Caché ObjectScript
C Shell
Caml
Candle
Cayenne
CDuce
Cecil
Cel
Cesil
CFML
Cg
Chapel
CHAIN
Charity
Charm
Chef
CHILL
CHIP-8
chomski
Oxygene (formerly Chrome)
ChucK
CICS
{{col-3}}

Cilk
CL (IBM)
Claire
Clarion
Clean
Clipper
CLIST
Clojure
CLU
CMS-2
COBOL - ISO/IEC 1989
CobolScript
Cobra
CODE
CoffeeScript
Cola
ColdC
ColdFusion
Cool
COMAL
Combined Programming Language (CPL)
Common Intermediate Language (CIL)
{{col-3}}

Common Lisp (also known as CL)
COMPASS
Component Pascal
COMIT
Constraint Handling Rules (CHR)
Converge
Coral 66
Corn
CorVision
Coq
COWSEL
CPL
csh
CSP
Csound
Curl
Curry
Cyclone
Cython
{{col-end}}
D{{col-begin}}
{{col-3}}

D
DASL (Datapoint&#39;s Advanced Systems Language)
DASL (Distributed Application Specification Language)
Dart
DataFlex
Datalog
{{col-3}}

DATATRIEVE
dBase
dc
DCL
Deesel (formerly G)
Delphi
{{col-3}}

DinkC
DIBOL
DL/I
Draco
Dylan
DYNAMO
{{col-end}}
E{{col-begin}}
{{col-3}}

E
E#
Ease
EASY
Easy PL/I
EASYTRIEVE PLUS
ECMAScript
Edinburgh IMP
EGL
{{col-3}}

Eiffel
ELAN
Emacs Lisp
Emerald
Epigram
Erlang
Escapade
Escher
ESPOL
{{col-3}}

Esterel
Etoys
Euclid
Euler
Euphoria
EusLisp Robot Programming Language
CMS EXEC
EXEC 2
{{col-end}}
F{{col-begin}}
{{col-3}}

F
F#
Factor
Falcon
Fancy
Fantom
Felix
Ferite
FFP
Fjölnir
{{col-3}}

FL
Flavors
Flex
FLOW-MATIC
FOCAL
FOCUS
FOIL
FORMAC
@Formula
Forth
{{col-3}}

Fortran - ISO/IEC 1539
Fortress
FoxBase
FoxPro
FP
FPr
Franz Lisp
Frink
F-Script
Fuxi
{{col-end}}
G{{col-begin}}
{{col-3}}

G
Game Maker Language
GameMonkey Script
GAMS
GAP
G-code
Genie
GDL
Gibiane
{{col-3}}

GJ
GLSL
GNU E
GM
Go
Go!
GOAL
Gödel
{{col-3}}

Godiva
GOM (Good Old Mad)
Goo
GOTRAN
GPSS
GraphTalk
GRASS
Groovy
{{col-end}}
H{{col-begin}}
{{col-3}}

HAL/S
Hamilton C shell
Harbour
IBM HAScript
Haskell
{{col-3}}

HaXe
High Level Assembly
HLSL
Hop
{{col-3}}

Hope
Hugo
Hume
HyperTalk
{{col-end}}
I{{col-begin}}
{{col-3}}

IBM Basic assembly language
IBM Informix-4GL
IBM RPG
ICI
Icon
Id
{{col-3}}

IDL
IMP
Inform
Io
Ioke
IPL
{{col-3}}

IPTSCRAE
ISPF
ISWIM
{{col-end}}
J{{col-begin}}
{{col-3}}

J
J#
J++
JADE
Jako
JAL
Janus
{{col-3}}

JASS
Java
JavaScript
JCL
JEAN
Join Java
JOSS
{{col-3}}

Joule
JOVIAL
Joy
JScript
JavaFX Script
{{col-end}}
K{{col-begin}}
{{col-3}}

K
Kaleidoscope
Karel
Karel++
Kaya
{{col-3}}

KEE
KIF
KRC
KRL
KRL (KUKA Robot Language)

{{col-3}}

KRYPTON
ksh
{{col-end}}


L{{col-begin}}
{{col-3}}

L
L# .NET
LabVIEW
Ladder
Lagoona
LANSA
Lasso
LaTeX
Lava
LC-3
Leadwerks Script
Leda
Legoscript
{{col-3}}

LilyPond
Limbo
Limnor
LINC
Lingo
Linoleum
LIS
LISA
Lisaac
Lisp - ISO/IEC 13816
Lite-C Lite-c
Lithe
Little b
{{col-3}}

Logo
Logtalk
LPC
LSE
LSL
Lua
Lucid
Lustre
LYaPAS
Lynx
{{col-end}}
M{{col-begin}}
{{col-3}}

M
M2001
M4
Machine code
MAD (Michigan Algorithm Decoder)
MAD/I
Magik
Magma
make
Maple
MAPPER (Unisys/Sperry) now part of BIS
MARK-IV (Sterling/Informatics) now VISION:BUILDER of CA
Mary
MASM Microsoft Assembly x86
Mathematica
MATLAB
Maxima (see also Macsyma)
{{col-3}}

MaxScript internal language 3D Studio Max
Maya (MEL)
MDL
Mercury
Mesa
Metacard
Metafont
MetaL
Microcode
MicroScript
MIIS
MillScript
MIMIC
Mirah
Miranda
MIVA Script
ML
{{col-3}}

Moby
Model 204
Modelica
Modula
Modula-2
Modula-3
Mohol
MOO
Mortran
Mouse
MPD
MSIL - deprecated name for CIL
MSL
MUMPS
{{col-end}}
N{{col-begin}}
{{col-3}}

Napier88
NASM
NATURAL
NEAT chipset
Neko
Nemerle
NESL
{{col-3}}

Net.Data
NetLogo
NewLISP
NEWP
Newspeak
NewtonScript
NGL
{{col-3}}

Nial
Nice
Nickle
NPL
Not eXactly C (NXC)
Not Quite C (NQC)
Nu
NSIS
{{col-end}}
O{{col-begin}}
{{col-3}}

o:XML
Oak
Oberon
Object Lisp
ObjectLOGO
Object REXX
Object Pascal
Objective-C
Objective Caml
Objective-J
{{col-3}}

Obliq
Obol
occam
occam-π
Octave
OmniMark
Onyx
Opa
Opal
OpenEdge ABL
OPL
{{col-3}}

OPS5
OptimJ
Orc
ORCA/Modula-2
Orwell
Oxygene
Oz
{{col-end}}
P{{col-begin}}
{{col-3}}

P#
PARI/GP
Pascal - ISO 7185
Pawn
PCASTL
PCF
PEARL
PeopleCode
Perl
PDL
PHP
Phrogram
Pico
Pict
Pike
PIKT
PILOT
Pizza
{{col-3}}

PL-11
PL/0
PL/B
PL/C
PL/I - ISO 6160
PL/M
PL/P
PL/SQL
PL360
PLANC
Plankalkül
PLEX
PLEXIL
Plus
POP-11
{{col-3}}

PostScript
PortablE
Powerhouse
PowerBuilder - 4GL GUI appl. generator from Sybase
PPL
Processing
Prograph
PROIV
Prolog
Visual Prolog
Promela
PROTEL
ProvideX
Pro*C
Pure
Python
{{col-end}}
Q{{col-begin}}
{{col-1-of-3}}

Q (equational programming language)
Q (programming language from Kx Systems)
{{col-2-of-3}}

Qi
QtScript
{{col-3-of-3}}

QuakeC
QPL
{{col-end}}
R{{col-begin}}
{{col-3}}

R
R++
Racket
RAPID
Rapira
Ratfiv
Ratfor
{{col-3}}

rc
REBOL
Redcode
REFAL
Reia
Revolution
rex
REXX
{{col-3}}

Rlab
ROOP
RPG
RPL
RSL
RTL/2
Ruby
Rust
{{col-end}}
S{{col-begin}}
{{col-3}}

S
S2
S3
S-Lang
S-PLUS
SA-C
SabreTalk
SAIL
SALSA
SAM76
SAS
SASL
Sather
Sawzall
SBL
Scala
Scheme
Scilab
Scratch
Script.NET
{{col-3}}

Sed
Self
SenseTalk
SETL
Shift Script
SiMPLE
SIMPOL
SIMSCRIPT
Simula
Simulink
SISAL
SLIP
SMALL
Smalltalk
Small Basic
SML
SNOBOL(SPITBOL)
Snowball
SOAP
{{col-3}}

SOL
Span
SPARK
SPIN
SP/k
SPS
Squeak
Squirrel
SR
S/SL
Strand
STATA
Stateflow
Subtext
Suneido
SuperCollider
SuperTalk
SYMPL
SyncCharts
SystemVerilog
{{col-end}}
T{{col-begin}}
{{col-3}}

T
TACL
TACPOL
TADS
TAL
Tcl
Tea
TECO
{{col-3}}

TELCOMP
TeX
TEX
TIE
Timber
Tom
TOM
{{col-3}}

Topspeed
TPU
Trac
T-SQL
TTCN
Turing
TUTOR
TXL
{{col-end}}
U{{col-begin}}
{{col-3}}

Ubercode
Unicon
{{col-3}}

Uniface
UNITY
{{col-3}}

Unix shell
UnrealScript
{{col-end}}
V{{col-begin}}
{{col-3}}

Vala
VBA
VBScript
Verilog
VHDL
Visual Basic
{{col-3}}

Visual Basic .NET
Visual C++
Visual C++ .Net
Visual C#
Visual DataFlex
Visual DialogScript
{{col-3}}

Visual FoxPro
Visual J++
Visual J#
Visual Objects
VSXu
Vvvv
{{col-end}}
W{{col-begin}}
{{col-3}}

WATFIV, WATFOR
{{col-3}}

WebQL
{{col-3}}

Winbatch
{{col-end}}
X{{col-begin}}
{{col-3}}

X++
X10
XBL
{{col-3}}

XC (exploits XMOS architecture)
xHarbour
XL
XOTcl
XPL
{{col-3}}

XPL0
XQuery
XSB
XSLT - See XPath
{{col-end}}
Y{{col-begin}}
{{col-3}}

Yorick
{{col-3}}

YQL
{{col-3}}

Yoix
{{col-end}}
Z{{col-begin}}
{{col-3}}

Z
Z notation
{{col-3}}

Zeno
ZOPL
{{col-3}}

ZPL
ZZT-oop
{{col-end}}{{CompactTOC8}}
See also{{Portal}}

Comparison of programming languages
Lists of programming languages
Hello World in various programming languages
List of BASIC dialects
List of BASIC dialects by platform
List of markup languages
{{Clear}}
{{Sister project links}}{{DEFAULTSORT:List Of Programming Languages}}

af:Lys van programmeertale
ar:ملحق:قائمة لغات البرمجة
bg:Списък на езици за програмиране
bs:Spisak programskih jezika
cs:Seznam programovacích jazyků
de:Liste von Programmiersprachen
eo:Listo de programlingvoj
fr:Liste des langages de programmation
hi:प्रोग्रामन भाषाओं की सूची
hr:Dodatak:Popis programskih jezika
is:Listi yfir forritunarmál í stafrófsröð
it:Lista dei linguaggi di programmazione
he:שפות תכנות
lv:Alfabētisks programmēšanas valodu saraksts
hu:Programozási nyelvek listája betűrendben
ms:Senarai bahasa pengaturcaraan
nl:Lijst van programmeertalen
ja:プログラミング言語一覧
no:Liste over programmeringsspråk
pt:Anexo:Lista de linguagens de programação
ro:Listă alfabetică a limbajelor de programare
ru:Список языков программирования
simple:List of programming languages
sk:Zoznam programovacích jazykov
sl:Seznam programskih jezikov
sr:Списак програмских језика
fi:Luettelo ohjelmointikielistä
ta:நிரலாக்க மொழிகளின் பட்டியல்
tg:Рӯйхати забонҳои барномасозӣ
tr:Programlama dilleri listesi
bug:Daftar basa ma-program-e
uk:Список мов програмування
vi:Danh sách các ngôn ngữ lập trình
zh:程序设计语言列表