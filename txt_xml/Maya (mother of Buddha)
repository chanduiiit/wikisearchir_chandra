title{{Refimprove}}

Queen Māyā of Sakya (Māyādevī) was the birth mother of the historical Gautama Buddha, Siddhārtha of the Gautama gotra,  and sister of Mahāpajāpatī Gotamī the first Buddhist nun ordained by the Buddha. "Māyā" means "illusion" or "enchantment" in Sanskrit and Pāli. Māyā is also called Mahāmāyā ("Great Māyā") and Māyādevī ("Queen, literally a female-deva, 'goddess,' Māyā"). In Tibetan she is called Gyutrulma. Queen Mayadevi was born in Devadaha kingdom of Nepal.
Marriage and childbirthMāyā married king Śuddhodana (Pāli: Suddhodana), the ruler of the Śākya clan of Kapilvastu. She was the daughter of King Śuddhodhana's uncle and therefore his cousin; her father was king of Devadaha.
The birth of the Buddha-to-be

Queen Māyā and King Suddhodhana did not have children for twenty years into their marriage. According to legend, One full moon night, sleeping in the palace, the queen had a vivid dream. She felt herself being carried away by four devas (spirits) to Lake Anotatta in the Himalayas. After bathing her in the lake, the devas clothed her in heavenly cloths, anointed her with perfumes, and bedecked her with divine flowers. Soon after a white elephant, holding a white lotus flower in its trunk, appeared and went round her three times, entering her womb through her right side. Finally the elephant disappeared and the queen awoke, knowing she had been delivered an important message, as the elephant is a symbol of greatness in Nepal.http://www.buddhanet.net/e-learning/buddhism/lifebuddha/1lbud.htm According to Buddhist tradition, the Buddha-to-be was residing as a Bodhisattva, in the {{IAST}} heaven, and decided to take the shape of a white elephant to be reborn on Earth for the last time. Māyā gave birth to Siddharta c. 563 BCE.  The pregnancy lasted ten lunar months. Following custom, the Queen returned to her own home for the birth. On the way, she stepped down from her palanquin to have a walk under the Sal tree (Shorea robusta), often confused with the Ashoka tree (Saraca asoca), in the beautiful flower garden of Lumbini Park, Lumbini Zone, Nepal. Maya Devi was delighted by the park and gave birth standing while holding onto a sal branch. Legend has it that Prince Siddhārtha emerged from her right side. It was the eighth day of April. Some accounts say she gave him his first bath in the Puskarini pond in Lumbini Zone. But legend has it that devas caused it to rain to wash the newborn babe. He was later named Siddhārtha, "He who has accomplished his goals" or "The accomplished goal".Research in Wisdom Quarterly: American Buddhist Journal{{Citation needed}} shows that the details of the legendary account coincide exactly with the existing Indian mythology of fertility goddesses, Salabhanjikas, "breaking a branch of a Sala tree"), often depicted standing against trees with one leg bent up and one hand holding a branch. They are believed to be virginal and capable of making a tree bear flowers. Parallels to this myth may survive in early Christianity: according to the Dead Sea Scrolls, the chaste or "virgin" Mary was a much older woman who miraculously conceived of a child by the intervention of the Holy Spirit of Jehovah (See Luke 1:35).Queen Māyā died seven days after the birth of the Buddha-to-be Bodhisatta and was reborn in the Tavatimsa Heaven, where the Buddha later preached the Abhidharma to her. Her sister Prajāpatī (Pāli: Pajāpatī or Mahāpajāpatī Gotamī) became the child's foster mother.  After Prince Siddhartha had attained perfection and become the Buddha, he visited his mother in heaven for three months to pay respects and to teach the Dharma.
Religious parallels{{See also}}Referring to the prophetic dream Queen Maya had prior to conception, some versions of the life story of the Buddha say that he was conceived without sexual activity. This interpretation has led to parallels being drawn with the birth story of Jesus.  The story of the birth of the Buddha was known in the West, and possibly influenced the story of the birth of Jesus. Saint Jerome (4th century CE) mentions the birth of the Buddha, who he says "was born from the side of a virgin".Against Jovinianus, Book I, chap. 42 Also a fragment of Archelaos of Carrha (278 CE) mentions the Buddha's virgin-birth.Other parallels in the birth stories include:

The similarity in the sounds of the names of Mary (Aramaic: מרים, Maryām) and Maya.
Maya conceived during a dream, Mary conceived around the time of a visitation from an angel.
Both women gave birth &#34;outside&#34; of a home.
Heavenly wonders appeared in the sky.
Heavenly beings (angels or devas; or in some Mahayana traditions, Samantabhadra) announcing the newborn as &#34;savior&#34; of the world.
Sages came to visit the newborn and make prophecies of auspicious careers.

There are also parallels between the Buddha being born of Maya and the Greek messenger god Hermes being born of a mother with a similar name, the goddess Maia, since Hermes is associated with the planet Mercury, a planet called Budha in Sanskrit.
See also


Gautama Buddha
History of Buddhism
Mahapajapati Gotami
The birth of Buddha (Lalitavistara)


References{{Reflist}}
External links
{{Commons category}}{{Buddhism2}}
{{Buddhism topics}}{{Persondata}}
{{DEFAULTSORT:Maya, Queen}}


bo:ལྷ་མོ་སྒྱུ་འཕྲུལ་མ།
cs:Královna Mája
eo:Reĝino Maja
fr:Māyā (bouddhisme)
ko:마야부인 (인도)
it:Māyā (madre di Gautama Buddha)
new:माया (सन् १९९९या संकिपा)
ja:摩耶夫人
km:ព្រះនាងមាយា
sr:Маја (Будина мати)
sh:Maya (Buddhina majka)
sv:Mahamaya
tl:Māyādevī
ta:மாயா
th:พระนางสิริมหามายา
zh:摩耶夫人