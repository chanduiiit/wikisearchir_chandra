titleThe Maya Embedded Language (MEL) is a scripting language used to simplify tasks in Autodesk's 3D Graphics Software Maya. Most tasks that can be achieved through Maya's GUI can be achieved with MEL, as well as certain tasks that are not available from the GUI. MEL offers a method of speeding up complicated or repetitive tasks, as well as allowing users to redistribute a specific set of commands to others that may find it useful.MEL is syntactically similar to Tcl and Perl. It provides some memory management and dynamic array-allocation, and offers direct access to functions specific to Maya.  The majority of standard Maya commands are default MEL scripts, saved in the Maya Program Files directory.  MEL is fairly limited compared to mainstream scripting languages. It is not object oriented, and lacks advanced features such as associative arrays. Very few improvements have been made to it in recent years, and in Maya 8.5, Python was added to Maya as an alternative to MEL.
Applications of MELThe tools designed using MEL Scripts generally come under the following categories.

Data I/O
Motion capture data import
Proprietary game data export
Scene meta-data for production tracking
Geometry creation/modification
Custom primitives
Third-party renderer specific data type (e.g., RenderMan sub-divisional surfaces)
Modeling tools not offered in the base package
Animation tools
Muscle simulators
Rigging/setup controls
Crowd AI behavior
Lighting /rendering tools
Automatic creation of common complex Shader setups
Pre- and post-render effects
Making calls to third-party renderers
Dynamics
Custom particle behavior
Simulated object emergent flocking and swarming
Genetic algorithms
Cloth simulation
File and Folder Scene Setup and Maintenance 
User interface customization
Custom character controls
Removal of invalid Maya commands
Custom UIs


ExampleThis is an example of a script which copies a selected object through its path:
// animated duplicates/instances script
proc animatedDuplication (int $rangeStart, int $rangeEnd, int $numOfDuplicates, int $duplicateOrInstance)
{
    int $range_start = $rangeStart;
    int $range_end = $rangeEnd;
    int $num_of_duplicates = $numOfDuplicates;
    int $step_size = ($range_end - $range_start) / $num_of_duplicates;
    int $i = 0;
    int $temp;

    currentTime $range_start;     // set to range start

    string $selectedObjects[];    // to store selected objects
    $selectedObjects = `ls -sl`;  // store selected objects
    select $selectedObjects;

    while ($i <= $num_of_duplicates)
    {
        $temp = $range_start + ($step_size * $i);
        currentTime ($temp);
        // seleced the objects to duplicate or instance
        select $selectedObjects;
        if($duplicateOrInstance == 0)
        {
            duplicate;
        }
        else
        {
            instance;
        }
        $i++;
    }
}

 // Usage example:
 //  duplicate the current selection 5 times -- 
 //  evenly distributed between frame 1 and 240
 animatedDuplication(1, 240, 5, 0);


Expression Language
Maya also offers an expression language that is a super-set of MEL, and results in nodes that are executed as part of Maya's dependency graph.  Expressions are developed with Maya's expression editor, and allow scripts to trigger while Maya evaluates the scene file at each change in time, to simulate complex behaviors or perform other useful tasks.  
References{{refbegin}}

Mark R. Wilkins, Chris Kazmier: &#34;MEL Scripting for Maya Animators&#34;
1st, Morgan Kaufmann 2002; ISBN 1-55860-841-9
2nd, Morgan Kaufmann 2005; ISBN 0-12-088793-2
Pisca, Nicholas, &#34;YSYT - Maya MEL Basics for Designers&#34;
1st, 0001d Publishing 2009; ISBN 978-0-578-00988-9
{{refend}}
See also

Computer programming


External links

Homepage of the Maya Software
Mel Wiki

{{compu-lang-stub}}