title{{Use mdy dates}}
{{pp-vandalism}}
{{Coord}}
{{Infobox company}}
Microsoft Corporation ({{Nasdaq}}) is an American multinational corporation headquartered in Redmond, Washington, United States that develops, manufactures, licenses, and supports a wide range of products and services predominantly related to computing through its various product divisions. Established on April 4, 1975 to develop and sell BASIC interpreters for the Altair 8800, Microsoft rose to dominate the home computer operating system market with MS-DOS in the mid-1980s, followed by the Microsoft Windows line of operating systems.Microsoft would also come to dominate the office suite market with Microsoft Office. The company has diversified in recent years into the video game industry with the Xbox and its successor, the Xbox 360 as well as into the consumer electronics and digital services market with Zune, MSN and the Windows Phone OS. The ensuing rise of stock in the company's 1986 initial public offering (IPO) made an estimated three billionaires and 12,000 millionaires from Microsoft employees (Forbes 400 list revealed that in March 2011 both Jon Shipley and Nathan Myhrvold lost their billionaire status). In May 2011, Microsoft Corporation acquired Skype Communications for $8.5 billion.{{cite web}}Primarily in the 1990s, critics{{Who}} contend Microsoft used monopolistic business practices and anti-competitive strategies including refusal to deal and tying, put unreasonable restrictions in the use of its software, and used misrepresentative marketing tactics; both the U.S. Department of Justice and European Commission found the company in violation of antitrust laws.{{Citation needed}} Known for its interviewing process with obscure questions, various studies and ratings were generally favorable to Microsoft's diversity within the company as well as its overall environmental impact with the exception of the electronics portion of the business.{{Citation needed}}
History{{Main}}
Early historyPaul Allen and Bill Gates, childhood friends with a passion in computer programming, were seeking to make a successful business utilizing their shared skills. The January 1975 issue of Popular Electronics featured Micro Instrumentation and Telemetry Systems's (MITS) Altair 8800 microcomputer. Allen noticed that they could program a BASIC interpreter for the device; after a call from Gates claiming to have a working interpreter, MITS requested a demonstration. Since they didn't actually have one, Allen worked on a simulator for the Altair while Gates developed the interpreter. Although they developed the interpreter on a simulator and not the actual device, the interpreter worked flawlessly when they demonstrated the interpreter to MITS in Albuquerque, New Mexico in March 1975; MITS agreed to distribute it, marketing it as Altair BASIC.{{Harvnb}} They officially established Microsoft on April 4, 1975, with Gates as the CEO.{{cite news}} Allen came up with the original name of "Micro-Soft," as recounted in a 1995 Fortune magazine article. In August 1977 the company formed an agreement with ASCII Magazine in Japan, resulting in its first international office, "ASCII Microsoft".{{cite journal}} The company moved to a new home in Bellevue, Washington in January 1979.

Microsoft Inc. logo history 
Logo Years 
 
  Microsoft "blibbet" logo, filed August 26, 1982 at the USPTO and used until 1987.{{cite web}} 
 Microsoft "Pac-Man" logo, designed by Scott Baker and used since 1987, with the 1994–2002 slogan "Where do you want to go today?".{{cite news}}{{cite news}} 
  Microsoft logo as of 2006–2011, with the slogan "Your potential. Our passion." 
  Logo by Microsoft with the slogan"Be What's Next." 2011–present. 
Microsoft entered the OS business in 1980 with its own version of Unix, called Xenix.{{cite news}} However, it was DOS (Disk Operating System) that solidified the company's dominance. After negotiations with Digital Research failed, IBM awarded a contract to Microsoft in November 1980 to provide a version of the CP/M OS, which was set to be used in the upcoming IBM Personal Computer (IBM PC).{{cite web}} For this deal, Microsoft purchased a CP/M clone called 86-DOS from Seattle Computer Products, branding it as MS-DOS, which IBM rebranded to PC-DOS. Following the release of the IBM PC in August 1981, Microsoft retained ownership of MS-DOS. Since IBM copyrighted the IBM PC BIOS, other companies had to reverse engineer it in order for non-IBM hardware to run as IBM PC compatibles, but no such restriction applied to the operating systems. Due to various factors, such as MS-DOS's available software selection {{Citation needed}}, Microsoft eventually became the leading PC OS vendor.{{cite journal}}{{Harvnb}} The company expanded into new markets with the release of the Microsoft Mouse in 1983, as well as a publishing division named Microsoft Press.{{Harvnb}} Paul Allen resigned from Microsoft in February after developing Hodgkin's disease.{{Harvnb}}
1984–1994: Windows and OfficeWhile jointly developing a new OS with IBM in 1984, OS/2, Microsoft released Microsoft Windows, a graphical extension for MS-DOS, on November 20.{{Harvnb}} Microsoft moved its headquarters to Redmond on February 26, 1986, and on March 13 the company went public;{{cite news}} the ensuing rise in the stock would make an estimated four billionaires and 12,000 millionaires from Microsoft employees.{{cite news}} Due to the partnership with IBM, in 1990 the Federal Trade Commission set its eye on Microsoft for possible collusion; it marked the beginning of over a decade of legal clashes with the U.S. Government.{{cite news}} Microsoft announced the release of its version of OS/2 to original equipment manufacturers (OEMs) on April 2, 1987;{{Harvnb}} meanwhile, the company was at work on a 32-bit OS, Microsoft Windows NT, using ideas from OS/2; it shipped on July 21, 1993 with a new modular kernel and the Win32 application programming interface (API), making porting from 16-bit (MS-DOS-based) Windows easier. Once Microsoft informed IBM of NT, the OS/2 partnership deteriorated.{{cite web}}Microsoft introduced its office suite, Microsoft Office, in 1990. The software bundled separate office productivity applications, such as Microsoft Word and Microsoft Excel.{{Harvnb}} On May 22 Microsoft launched Windows 3.0 with a streamlined user interface graphics and improved protected mode capability for the Intel 386 processor.{{cite web}} Both Office and Windows became dominant in their respective areas.{{cite news}}{{cite news}} Novell, a Word competitor from 1984–1986, filed a lawsuit years later claiming that Microsoft left part of its APIs undocumented in order to gain a competitive advantage.{{cite news}}On July 27, 1994, the U.S. Department of Justice, Antitrust Division filed a Competitive Impact Statement that said, in part:
"Beginning in 1988, and continuing until July 15, 1994, Microsoft induced many OEMs to execute anticompetitive "per processor" licenses. Under a per processor license, an OEM pays Microsoft a royalty for each computer it sells containing a particular microprocessor, whether the OEM sells the computer with a Microsoft operating system or a non-Microsoft operating system. In effect, the royalty payment to Microsoft when no Microsoft product is being used acts as a penalty, or tax, on the OEM's use of a competing PC operating system. Since 1988, Microsoft's use of per processor licenses has increased."{{cite web}}
1995–2005: Internet and the 32-bit eraFollowing Bill Gates's internal "Internet Tidal Wave memo" on May 26, 1995 Microsoft began to redefine its offerings and expand its product line into computer networking and the World Wide Web.{{cite news}} The company released Windows 95 on August 24, 1995, featuring pre-emptive multitasking, a completely new user interface with a novel start button, and 32-bit compatibility; similar to NT, it provided the Win32 API.{{cite journal}}{{harvnb}} Windows 95 came bundled with the online service MSN, and for OEMs Internet Explorer, a web browser. Internet Explorer was not bundled with the retail Windows 95 boxes because the boxes were printed before the team finished the web browser, and instead was included in the Windows 95 Plus! pack.{{cite news}} Branching out into new markets in 1996, Microsoft and NBC Universal created a new 24/7 cable news station, MSNBC.{{cite web}} Microsoft created Windows CE 1.0, a new OS designed for devices with low memory and other constraints, such as personal digital assistants.{{cite web}} In October 1997, the Justice Department filed a motion in the Federal District Court, stating that Microsoft violated an agreement signed in 1994 and asked the court to stop the bundling of Internet Explorer with Windows.{{Harvnb}}Bill Gates handed over the CEO position on January 13, 2000 to Steve Ballmer, an old college friend of Gates and employee of the company since 1980, creating a new position for himself as Chief Software Architect.{{Harvnb}} Various companies including Microsoft formed the Trusted Computing Platform Alliance in October 1999 to, among other things, increase security and protect intellectual property through identifying changes in hardware and software. Critics decry the alliance as a way to enforce indiscriminate restrictions over how consumers use software, and over how computers behave, a form of digital rights management; for example the scenario where a computer is not only secured for its owner, but also secured against its owner as well.{{cite news}}{{cite journal}} On April 3, 2000, a judgment was handed down in the case of United States v. Microsoft,{{cite web}} calling the company an "abusive monopoly";{{cite web}} it settled with the U.S. Department of Justice in 2004. On October 25, 2001 Microsoft released Windows XP, unifying the mainstream and NT lines under the NT codebase.{{cite news}} The company released the Xbox later that year, entering the game console market dominated by Sony and Nintendo.{{cite news}} In March 2004 the European Union brought antitrust legal action against the company, citing it abused its dominance with the Windows OS, resulting in a judgment of €497{{nbsp}}million ($613{{nbsp}}million) and to produce new versions of Windows XP without Windows Media Player, Windows XP Home Edition N and Windows XP Professional N.{{cite news}}{{cite web}}
2006–present: Windows Vista, mobile, SaaSReleased in January 2007, the next version of Windows, Windows Vista, focused on features, security, and a redesigned user interface dubbed Aero.{{cite web}}{{cite news}} Microsoft Office 2007, released at the same time, featured a "Ribbon" user interface which was a significant departure from its predecessors. Relatively strong sales of both titles helped to produce a record profit in 2007.{{cite news}} The European Union imposed another fine of €899{{nbsp}}million ($1.4{{nbsp}}billion) for Microsoft's lack of compliance with the March 2004 judgment on February 27, 2008, saying that the company charged rivals unreasonable prices for key information about its workgroup and backoffice servers. Microsoft stated that it was in compliance and that "these fines are about the past issues that have been resolved".{{cite news}}Bill Gates retired from his role as Chief Software Architect on June 27, 2008 while retaining other positions related to the company in addition to being an advisor for the company on key projects.{{cite news}} Azure Services Platform, the company's entry into the cloud computing market for Windows, launched on October 27, 2008.{{cite web}} On February 12, 2009, Microsoft announced its intent to open a chain of Microsoft-branded retail stores, and on October 22, 2009 the first retail Microsoft Store opened in Scottsdale, Arizona; the same day the first store opened Windows 7 was officially released to the public. Windows 7's focus was on refining Vista with ease of use features and performance enhancements, rather than a large reworking of Windows.{{cite web}}{{cite news}}{{cite news}}As the smartphone industry boomed beginning in 2007, Microsoft struggled to keep up with its rivals Apple and Google in providing a modern smartphone operating system. As a result, in 2010, Microsoft revamped their aging flagship mobile operating system, Windows Mobile, replacing it with the new Windows Phone OS; along with a new strategy in the smartphone industry that has Microsoft working more closely with smartphone manufactures, such as Nokia, and to provide a consistent user experience across all smartphones using Microsoft's Windows Phone OS.Microsoft is a founding member of the Open Networking Foundation started on March 23, 2011.  Other founding companies include Google, HP Networking, Yahoo, Verizon, Deutsche Telekom and 17 other companies.   The nonprofit organization is focused on providing support for a new cloud computing initiative called Software-Defined Networking.{{cite web}} The initiative is meant to speed innovation through simple software changes in telecommunications networks, wireless networks, data centers and other networking areas.{{cite web}}
Product divisions{{main}}
For the 2010 fiscal year, Microsoft had five product divisions: Windows & Windows Live Division, Server and Tools, Online Services Division, Microsoft Business Division, and Entertainment and Devices Division.
Windows & Windows Live Division, Server and Tools, Online Services DivisionThe company's Client division produces the flagship Windows OS line such as Windows 7; it also produces the Windows Live family of products and services. Server and Tools produces the server versions of Windows, such as Windows Server 2008 R2 as well as a set of development tools called Microsoft Visual Studio, Microsoft Silverlight, a web application framework, and System Center Configuration Manager, a collection of tools providing remote-control abilities, patch management, software distribution and a hardware/software inventory. Other server products include: Microsoft SQL Server, a relational database management system, Microsoft Exchange Server, for certain business-oriented e-mail and scheduling features, Small Business Server, for messaging and other small business-oriented features; and Microsoft BizTalk Server, for business process management.Microsoft provides IT consulting ("Microsoft Consulting Services") and produces a set of certification programs handled by the Server and Tools division designed to recognize individuals who have a minimal set of proficiencies in a specific role; this includes developers ("Microsoft Certified Solution Developer"), system/network analysts ("Microsoft Certified Systems Engineer"), trainers ("Microsoft Certified Trainers") and administrators ("Microsoft Certified Systems Administrator" and "Microsoft Certified Database Administrator"). Microsoft Press, which publishes books, is also managed by the division. The Online Services Business division handles the online service MSN and the search engine Bing. As of December 2009, the company also possesses an 18% ownership of the cable news channel MSNBC without any editorial control; however, the division develops the channel's website, msnbc.com, in a joint venture with the channel's co-owner, NBC Universal.{{cite news}}
Business DivisionThe Microsoft Business Division produces Microsoft Office including Microsoft Office 2010, the company's line of office software. The software product includes Word (a word processor), Access (a relational database program), Excel (a spreadsheet program), Outlook (Groupware, frequently used with Exchange Server), PowerPoint (presentation software), and Publisher (desktop publishing software). A number of other products were added later with the release of Office 2003 including Visio, Project, MapPoint, InfoPath and OneNote. The division also develops enterprise resource planning (ERP) software for companies under the Microsoft Dynamics brand. These include: Microsoft Dynamics AX, Microsoft Dynamics NAV, Microsoft Dynamics GP, and Microsoft Dynamics SL. They are targeted at varying company types and countries, and limited to organizations with under 7,500 employees.{{cite news}} Also included under the Dynamics brand is the customer relationship management software Microsoft Dynamics CRM, part of the Azure Services Platform.
Entertainment and Devices DivisionThe Entertainment and Devices Division produces the Windows CE OS for embedded systems and Windows Phone for smartphones.{{cite web}} Microsoft initially entered the mobile market through Windows CE for handheld devices, eventually developing into the Windows Mobile OS and now, Windows Phone. Windows CE is designed for devices where the OS may not directly be visible to the end user, in particular, appliances and cars. The division also produces computer games that run on Windows PCs and other systems including titles such as Age of Empires, Halo and the Microsoft Flight Simulator series, and houses the Macintosh Business Unit which produces Mac OS software including Microsoft Office 2011 for Mac. Microsoft's Entertainment and Devices Division designs, markets, and manufactures consumer electronics including the Xbox 360 game console, the handheld Zune media player, and the television-based Internet appliance MSN TV. Microsoft also markets personal computer hardware including mice, keyboards, and various game controllers such as joysticks and gamepads.
CultureTechnical reference for developers and articles for various Microsoft magazines such as Microsoft Systems Journal (or MSJ) are available through the Microsoft Developer Network (MSDN). MSDN also offers subscriptions for companies and individuals, and the more expensive subscriptions usually offer access to pre-release beta versions of Microsoft software.{{cite web}}{{cite web}} In April 2004 Microsoft launched a community site for developers and users, titled Channel9, that provides a wiki and an Internet forum.{{cite news}} Another community site that provides daily videocasts and other services, On10.net, launched on March 3, 2006.{{cite web}} Free technical support is traditionally provided through online Usenet newsgroups, and CompuServe in the past, monitored by Microsoft employees; there can be several newsgroups for a single product. Helpful people can be elected by peers or Microsoft employees for Microsoft Most Valuable Professional (MVP) status, which entitles them to a sort of special social status and possibilities for awards and other benefits.{{cite news}}* {{cite web}}Noted for its internal lexicon, the expression "eating our own dog food" is used to describe the policy of using prerelease and beta versions of products inside Microsoft in an effort to test them in "real-world" situations.{{cite news}} This is usually shortened to just "dog food" and is used as noun, verb, and adjective. Another bit of jargon, FYIFV or FYIV ("F**k You, I'm [Fully] Vested"), is used by an employee to indicate they are financially independent and can avoid work anytime they wish.{{cite news}} The company is also known for its hiring process, mimicked in other organizations and dubbed the "Microsoft interview", which is notorious for off-the-wall questions such as "Why is a manhole cover round?".{{cite news}}Microsoft is an outspoken opponent of the cap on H1B visas, which allow companies in the U.S. to employ certain foreign workers. Bill Gates claims the cap on H1B visas makes it difficult to hire employees for the company, stating "I'd certainly get rid of the H1B cap" in 2005.{{cite news}} Critics of H1B visas argue that relaxing the limits would result in increased unemployment for U.S. citizens due to H1B workers working for lower salaries.{{cite news}} The Human Rights Campaign Corporate Equality Index, a report of how progressive the organization deems company policies towards LGBT (lesbian, gay, bisexual and transsexual) employees, rated Microsoft as 87% from 2002 to 2004 and as 100% from 2005 to 2010 after they allowed gender expression.{{cite news}}In a 2012 news story, Sky News reported, "Microsoft is investigating working conditions at a Chinese factory producing the Xbox games system after more than 300 employees reportedly threatened to throw themselves off a building. Chinese news websites said aggrieved staff at the Foxconn factory in Wuhan had asked for a raise before the incident. The employees were apparently told they could keep their jobs with no pay increase or resign and receive a pay-off. But after most opted to leave and take the compensation, the offer was reportedly withdrawn and the workers did not receive their payments.""Microsoft Investigates 'Mass Suicide Threat'". Sky News. January 11, 2012
Criticism{{main}}
Criticism of Microsoft has followed the company's existence because of various aspects of its products and business practices. Ease of use, stability, and security of the company's software are common targets for critics.  More recently, Trojan horses and other exploits have plagued numerous users due to faults in the security of Microsoft Windows and other programs. Microsoft is also accused of locking vendors into their products, and of not following and complying with existing standards in its software.{{cite web}} Total cost of ownership comparisons of Linux as well as Mac OS X to Windows are a continuous point of debate.The company has been in numerous lawsuits by several governments and other companies for unlawful monopolistic practices. In 2004, the European Union found Microsoft guilty in a highly publicized anti-trust case. Additionally, Microsoft's EULA for some of its programs is often criticized as being too restrictive as well as being against open source software. Criticism of the company has resulted in it being deemed "the evil empire" by some.{{cite web}} In a sci-fi allusion, Microsoft has also been called "The Borg" after the fictional race of aliens in the Star Trek universe. It reflects the perception that Microsoft often acquires technology from other companies rather than developing it in-house{{cite web}}, as well as to Microsoft's ability to adapt to and overwhelm its opponents' strategies. {{cite news}} {{cite news}} {{cite news}} {{cite news}} {{cite news}} {{cite news}}
Corporate affairsThe company is run by a board of directors made up of mostly company outsiders, as is customary for publicly traded companies. Members of the board of directors as of June 2010 are: Steve Ballmer, Dina Dublon, Bill Gates (chairman), Raymond Gilmartin, Reed Hastings, Maria Klawe, David Marquardt, Charles Noski, and Helmut Panke.{{cite press release}} Board members are elected every year at the annual shareholders' meeting using a majority vote system. There are five committees within the board which oversee more specific matters. These committees include the Audit Committee, which handles accounting issues with the company including auditing and reporting; the Compensation Committee, which approves compensation for the CEO and other employees of the company; the Finance Committee, which handles financial matters such as proposing mergers and acquisitions; the Governance and Nominating Committee, which handles various corporate matters including nomination of the board; and the Antitrust Compliance Committee, which attempts to prevent company practices from violating antitrust laws.{{cite web}}
When Microsoft went public and launched its initial public offering (IPO) in 1986, the opening stock price was $21; after the trading day, the price closed at $27.75. As of July 2010, with the company's nine stock splits, any IPO shares would be multiplied by 288; if one was to buy the IPO today given the splits and other factors, it would cost about 9{{nbsp}}cents.{{cite news}}{{Harvnb}}{{cite web}} The stock price peaked in 1999 at around $119 ($60.928 adjusting for splits).{{cite web}} The company began to offer a dividend on January 16, 2003, starting at eight cents per share for the fiscal year followed by a dividend of sixteen cents per share the subsequent year, switching from yearly to quarterly dividends in 2005 with eight cents a share per quarter and a special one-time payout of three dollars per share for the second quarter of the fiscal year.{{cite web}} Though the company had subsequent increases in dividend payouts, the price of Microsoft's stock remained steady for years.{{cite web}}* {{cite web}}* {{cite news}}One of Microsoft's business tactics, described by an executive as "embrace, extend and extinguish," initially embraces a competing standard or product, then extends it to produce their own version which is then incompatible with the standard, which in time extinguishes competition that does not or cannot use Microsoft's new version.{{cite news}} Various companies and governments sue Microsoft over this set of tactics, resulting in billions of dollars in rulings against the company.{{cite press release}}* {{cite web}}* {{cite news}}* {{cite news}} Microsoft claims that the original strategy is not anti-competitive, but rather an exercise of its discretion to implement features it believes customers want.{{cite news}}
FinancialStandard and Poor's and Moody's have both given a AAA rating to Microsoft, whose assets were valued at $41&nbsp;billion as compared to only $8.5&nbsp;billion in unsecured debt. Consequently, in February 2011 Microsoft released a corporate bond amounting to $2.25&nbsp;billion with relatively low borrowing rates compared to government bonds.{{cite news}}For the first time in 20 years Apple Inc. surpassed Microsoft in Q1 2011 quarterly profits and revenues due to a slowdown in PC sales and continuing huge losses in Microsoft's Online Services Division (which contains its search engine Bing). Microsoft profits were $5.2 billion, while Apple Inc. profits were $6 billion, on revenues of $14.5 billion and $24.7 billion respectively.{{cite news}}Microsoft's Online Services Division has been continuously loss-making since 2006 and in Q1 2011 it lost $726 million. This follows a loss of $2.5 billion for the year 2010.{{cite web}}
EnvironmentMicrosoft is ranked on the 17th place in Greenpeace’s Guide to Greener Electronics that ranks 18 electronics manufacturers according to their policies on toxic chemicals, recycling and climate change.{{cite web}}
Microsoft’s timeline for phasing out BFRs and phthalates in all products is 2012 but its commitment to phasing out PVC is not clear. As yet (January 2011) it has no products that are completely free from PVC and BFRs.{{cite web}}Microsoft's main U.S. campus received a silver certification from the Leadership in Energy and Environmental Design (LEED) program in 2008, and it installed over 2,000 solar panels on top of its buildings in its Silicon Valley campus, generating approximately 15 percent of the total energy needed by the facilities in April 2005.{{cite news}}Microsoft makes use of alternative forms of transit. It created one of the worlds largest private bus systems, the "Connector", to transport people from outside the company; for on-campus transportation, the "Shuttle Connect" uses a large fleet of hybrid cars to save fuel. The company also subsidises regional public transport as an incentive.{{cite web}}{{dead link}} In February 2010 however, Microsoft took a stance against adding additional public transport and high-occupancy vehicle (HOV) lanes to a bridge connecting Redmond to Seattle; the company did not want to delay the construction any further.{{cite news}}Microsoft was ranked number 1 in the list of the World's Best Multinational Workplaces by the Great Place to Work Institute in 2011.{{cite news}}
MarketingIn 2004, Microsoft commissioned research firms to do independent studies comparing the total cost of ownership (TCO) of Windows Server 2003 to Linux; the firms concluded that companies found Windows easier to administrate than Linux, thus those using Windows would administrate faster resulting in lower costs for their company (i.e. lower TCO).{{cite news}} This spurred a wave of related studies; a study by the Yankee Group concluded that upgrading from one version of Windows Server to another costs a fraction of the switching costs from Windows Server to Linux, although companies surveyed noted the increased security and reliability of Linux servers and concern about being locked into using Microsoft products.{{cite news}} Another study, released by the OSDL, claimed that the Microsoft studies were "simply outdated and one-sided" and their survey concluded that the TCO of Linux was lower due to Linux administrators managing more servers on average and other reasons.{{cite news}}As part of the "Get the Facts" campaign Microsoft highlighted the .NET trading platform that it had developed in partnership with Accenture for the London Stock Exchange, claiming that it provided "five nines" reliability. After suffering extended downtime and unreliability{{cite news}}{{cite news}} the LSE announced in 2009 that it was planning to drop its Microsoft solution and switch to a Linux based one in 2010.{{cite news}}{{cite news}}Microsoft adopted the so-called "Pac-Man Logo", designed by Scott Baker, in 1987. Baker stated "The new logo, in Helvetica italic typeface, has a slash between the o and s&nbsp; to emphasize the "soft" part of the name and convey motion and speed."{{Cite journal}} Dave Norris ran an internal joke campaign to save the old logo, which was green, in all uppercase, and featured a fanciful letter O, nicknamed the blibbet, but it was discarded.{{cite web}} Microsoft's logo with the "Your potential. Our passion." tagline below the main corporate name, is based on a slogan Microsoft used in 2008. In 2002, the company started using the logo in the United States and eventually started a TV campaign with the slogan, changed from the previous tagline of "Where do you want to go today?".{{cite news}} During the private MGX (Microsoft Global Exchange) conference in 2010, Microsoft unveiled the company's next tagline, "Be What's Next.", as well as a new logo scheduled for use sometime in the future.{{cite news}}
See also{{Portal box}}

List of Microsoft topics


References

Infobox statistics, Product divisions
{{refbegin}}

{{cite web}}
{{refend}}

References
{{Reflist}}

Bibliography
{{refbegin}}

{{Cite book}}
{{Cite document}}
{{Cite document}}
{{refend}}
External links{{Sister project links}}

{{Official website}}
{{Microsoft}}
{{Microsoft Executives}}
{{Navboxes}}
{{Featured article}}








{{Link FA}}af:Microsoft
als:Microsoft
am:ማይክሮሶፍት
ang:Microsoft
ar:مايكروسوفت
ast:Microsoft
az:Microsoft
bn:মাইক্রোসফট কর্পোরেশন
zh-min-nan:Microsoft
map-bms:Microsoft Corporation
be:Microsoft
be-x-old:Microsoft
bg:Майкрософт
bar:Microsoft
bs:Microsoft
br:Microsoft
ca:Microsoft
cs:Microsoft
cy:Microsoft
da:Microsoft
de:Microsoft
et:Microsoft
el:Microsoft
es:Microsoft
eo:Mikrosofto
eu:Microsoft
fa:مایکروسافت
fr:Microsoft
fy:Microsoft
ga:Microsoft
gl:Microsoft Corporation
gan:微軟
hak:Mì-ngiôn Kûng-sṳ̂
ko:마이크로소프트
hy:Մայքրոսոֆթ
hi:माइक्रोसॉफ़्ट
hr:Microsoft
ilo:Microsoft
id:Microsoft Corporation
ia:Microsoft Corporation
is:Microsoft
it:Microsoft Corporation
he:מיקרוסופט
jv:Microsoft
ka:Microsoft
kk:Microsoft
sw:Microsoft
ht:Microsoft
ku:Microsoft
ky:Microsoft
la:Microsoft
lv:Microsoft
lt:Microsoft
jbo:maikrosaft
hu:Microsoft
mk:Microsoft
ml:മൈക്രോസോഫ്റ്റ്
mt:Microsoft
mr:मायक्रोसॉफ्ट कॉर्पोरेशन
ms:Microsoft
mn:Microsoft
my:မိုက်ခရိုဆော့ဖ်
na:Microsoft
nl:Microsoft
ne:माइक्रोसफ्ट
ja:マイクロソフト
no:Microsoft
nn:Microsoft
oc:Microsoft
uz:Microsoft
ps:مايکروسافټ
km:Microsoft
pms:Microsoft
nds:Microsoft
pl:Microsoft
pt:Microsoft
kaa:Microsoft
ro:Microsoft
qu:Microsoft
ru:Microsoft
sah:Microsoft
sq:Microsoft
scn:Microsoft
si:මයික්‍රොසොෆ්ට්
simple:Microsoft
sk:Microsoft Corporation
sl:Microsoft
szl:Microsoft
so:Microsoft
ckb:مایکرۆسۆفت
sr:Мајкрософт
sh:Microsoft
fi:Microsoft
sv:Microsoft
tl:Microsoft
ta:மைக்ரோசாப்ட்
tt:Microsoft
te:మైక్రోసాఫ్ట్
th:ไมโครซอฟท์
tg:Microsoft
tr:Microsoft
tk:Microsoft
uk:Microsoft
ur:مائیکروسافٹ
ug:مىكروسوفىت شىركىتى
vi:Microsoft
fiu-vro:Microsoft
wa:Microsoft
wuu:微软公司
yi:מייקראסאפט
yo:Microsoft
zh-yue:微軟
zh:微软