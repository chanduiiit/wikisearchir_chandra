title{{About}}
In statistics, ordinary least squares (OLS) or linear least squares is a method for estimating the unknown parameters in a linear regression model. This method minimizes the sum of squared vertical distances between the observed responses in the dataset and the responses predicted by the linear approximation. The resulting estimator can be expressed by a simple formula, especially in the case of a single regressor on the right-hand side.The OLS estimator is consistent when the regressors are exogenous and there is no multicollinearity, and optimal in the class of linear unbiased estimators when the errors are homoscedastic and serially uncorrelated. Under these conditions, the method of OLS provides minimum-variance mean-unbiased estimation when the errors have finite variances. Under the additional assumption that the errors be normally distributed, OLS is the maximum likelihood estimator. OLS is used in economics (econometrics) and electrical engineering (control theory and signal processing), among many areas of application.
Linear model{{main}}Suppose the data consists of n observations {&thinsp;y{{su}},&thinsp;x{{su}}&thinsp;}{{su}}. Each observation includes a scalar response yi and a vector of predictors (or regressors) xi. In a linear regression model the response variable is a linear function of the regressors:


    y_i = x'_i\beta + \varepsilon_i, \,
  
where β is a p×1 vector of unknown parameters; εis are unobserved scalar random variables (errors) which account for the discrepancy between the actually observed responses yi and the "predicted outcomes" x′iβ; and ′ denotes matrix transpose, so that {{nowrap}} is the dot product between the vectors x and β. This model can also be written in matrix notation as


    y = X\beta + \varepsilon, \,
  
where y and ε are n×1 vectors, and X is an n×p matrix of regressors, which is also sometimes called the design matrix.As a rule, the constant term is always included in the set of regressors X, say, by taking xi1&nbsp;=&nbsp;1 for all {{nowrap}}. The coefficient β1 corresponding to this regressor is called the intercept.There may be some relationship between the regressors. For instance, the third regressor may be the square of the second regressor. In this case (assuming that the first regressor is constant) we have a quadratic model in the second regressor. But this is still considered a linear model because it is linear in the βs.
AssumptionsThere are several different frameworks in which the linear regression model can be cast in order to make the OLS technique applicable. Each of these settings produces the same formulas and same results, the only difference is the interpretation and the assumptions which have to be imposed in order for the method to give meaningful results. The choice of the applicable framework depends mostly on the nature of data at hand, and on the inference task which has to be performed.One of the lines of difference in interpretation is whether to treat the regressors as random variables, or as predefined constants. In the first case (random design) the regressors xi are random and sampled together with the yis from some population, as in an observational study. This approach allows for more natural study of the asymptotic properties of the estimators. In the other interpretation (fixed design), the regressors X are treated as known constants set by a design, and y is sampled conditionally on the values of X as in an experiment. For practical purposes, this distinction is often unimportant, since estimation and inference is carried out while conditioning on X. All results stated in this article are within the random design framework.
Classical linear regression modelThe classical model focuses on the "finite sample" estimation and inference, meaning that the number of observations n is fixed. This contrasts with the other approaches, which study the asymptotic behavior of OLS, and in which the number of observations is allowed to grow to infinity. Some results within this framework can be derived only under the assumption of normally distributed error terms, an assumption which is frequently criticized in practical applications.
 Correct specification. The linear functional form is correctly specified.
 Strict exogeneity. The errors in the regression should have conditional mean zero:{{harvtxt}}
The immediate consequence of the exogeneity assumption is that the errors have mean zero: {{nowrap}}, and that the regressors are uncorrelated with the errors: {{nowrap}}.
The exogeneity assumption is critical for the OLS theory. If it holds then the regressor variables are called exogenous. If it doesn't, then those regressors that are correlated with the error term are called endogenous,{{harvtxt}} and then the OLS estimates become invalid. In such case the method of instrumental variables may be used to carry out inference. No linear dependence. The regressors in X must all be linearly independent. Mathematically it means that the matrix X must have full column rank almost surely:{{harvtxt}}
Usually, it is also assumed that the regressors have finite moments up to at least second. In such case the matrix {{nowrap}} will be finite and positive semi-definite.
When this assumption is violated the regressors are called linearly dependent or perfectly multicollinear. In such case the value of the regression coefficient β cannot be learned, although prediction of y values is still possible for new values of the regressors that lie in the same linearly dependent subspace. Spherical errors:
where In is an n×n identity matrix, and σ2 is a parameter which determines the variance of each observation. This σ2 is considered a nuisance parameter in the model, although usually it is also estimated. If this assumption is violated then the OLS estimates are still valid, but no longer efficient.
It is customary to split this assumption into two parts:

 Normality. It is sometimes additionally assumed that the errors have normal distribution conditional on the regressors:{{harvtxt}}
This assumption is not needed for the validity of the OLS method, although certain additional finite-sample properties can be established in case when it does (especially in the area of hypotheses testing). Also when the errors are normal, the OLS estimator is equivalent to MLE, and therefore it is asymptotically efficient in the class of all regular estimators.


Independent and identically distributedIn some applications, especially with cross-sectional data, an additional assumption is imposed — that all observations are independent and identically distributed (iid). This means that all observations are taken from a random sample which makes all the assumptions listed earlier simpler and easier to interpret. Also this framework allows one to state asymptotic results (as the sample size {{nowrap}}), which are understood as a theoretical possibility of fetching new independent observations from the data generating process. The list of assumptions in this case is:

iid observations: (xi, yi) is independent from, and has the same distribution as, (xj, yj) for all {{nowrap}};
no multicollinearity: Qxx = E[ xix′i ] is a positive-definite matrix;
exogeneity: E[ εi | xi ] = 0;
homoscedasticity: Var[ εi | xi ] = σ2.


Time series model

The stochastic process {xi, yi} is stationary and ergodic;
The regressors are predetermined: E[xiεi] = 0 for all i = 1, …, n;
The p×p matrix Qxx = E[ xix′i ] is of full rank, and hence positive-definite;
{xiεi} is a martingale difference sequence, with a finite matrix of second moments Qxxε² = E[ εi2xix′i ].


EstimationSuppose b is a "candidate" value for the parameter β. The quantity {{nowrap}} is called the residual for the i-th observation, it measures the vertical distance between the data point {{nowrap}} and the hyperplane {{nowrap}}, and thus assesses the degree of fit between the actual data and the model. The sum of squared residuals (SSR) (also called the error sum of squares (ESS) or residual sum of squares (RSS)){{harvtxt}} is a measure of the overall model fit:


    S(b) = \sum_{i=1}^n (y_i - x'_ib)^2 = (y-Xb)'(y-Xb).
  
The value of b which minimizes this sum is called the OLS estimator for β. The function S(b) is quadratic in b with positive-definite Hessian, and therefore this function possesses a unique global minimum, which can be given by an explicit formula:{{harvtxt}}[proof]


    \hat\beta = {\rm arg}\min_{b\in\mathbb{R}^p} S(b) =  \bigg(\frac{1}{n}\sum_{i=1}^n x_ix'_i\bigg)^{\!-1} \!\!\cdot\, \frac{1}{n}\sum_{i=1}^n x_iy_i = (X'X)^{-1}X'y\ .
  

After we have estimated β, the fitted values (or predicted values) from the regression will be 


    \hat{y} = X\hat\beta = Py,
  
where P = X(X′X)−1X′ is the projection matrix onto the space spanned by the columns of X. This matrix P is also sometimes called the hat matrix because it "puts a hat" onto the variable y. Another matrix, closely related to P is the annihilator matrix {{nowrap}}, this is a projection matrix onto the space orthogonal to X. Both matrices P and M are symmetric and idempotent (meaning that {{nowrap}}), and relate to the data matrix X via identities {{nowrap}} and {{nowrap}}.{{harvtxt}} Matrix M creates the residuals from the regression:


    \hat\varepsilon = y - X\hat\beta = My = M\varepsilon.
  

Using these residuals we can estimate the value of σ2:


    s^2 = \frac{\hat\varepsilon'\hat\varepsilon}{n-p} = \frac{y'My}{n-p} = \frac{S(\hat\beta)}{n-p},\qquad
    \hat\sigma^2 = \frac{n-p}{n}\;s^2
  
The denominator, n-p, is the statistical degrees of freedom. The first quantity, s2, is the OLS estimate for σ2, whereas the second, \scriptstyle\hat\sigma^2, is the MLE estimate for σ2. The two estimators are quite similar in large samples; the first one is always unbiased, while the second is biased but minimizes the mean squared error of the estimator. In practice s2 is used more often, since it is more convenient for the hypothesis testing. The square root of s2 is called the standard error of the regression (SER), or standard error of the equation (SEE).It is common to assess the goodness-of-fit of the OLS regression by comparing how much the initial variation in the sample can be reduced by regressing onto X. The coefficient of determination R2 is defined as a ratio of "explained" variance to the "total" variance of the dependent variable y:{{harvtxt}}


    R^2 = \frac{\sum(\hat y_i-\overline{y})^2}{\sum(y_i-\overline{y})^2} = \frac{y'LPy}{y'Ly} = 1 - \frac{y'My}{y'Ly} = 1 - \frac{\rm SSR}{\rm TSS}
  
where TSS is the total sum of squares for the dependent variable, L = In − 11′/n, and 1 is an n×1 vector of ones. (L is a "centering matrix" which is equivalent to regression on a constant; it simply subtracts the mean from a variable.) In order for R2 to be meaningful, the matrix X of data on regressors must contain a column vector of ones to represent the constant whose coefficient is the regression intercept. In that case, R2 will always be a number between 0 and 1, with values close to 1 indicating a good degree of fit.
Simple regression model{{main}}
If the data matrix X contains only two variables: a constant, and a scalar regressor xi, then this is called the "simple regression model".{{harvtxt}} This case is often considered in the beginner statistics classes, as it provides much simpler formulas even suitable for manual calculation. The vectors of parameters in such model is 2-dimensional, and is commonly denoted as {{nowrap}}:


    y_i = \alpha + \beta x_i + \varepsilon_i.
  
The least squares estimates in this case are given by simple formulas


    \hat\beta = \frac{ \sum{x_iy_i} - \frac{1}{n}\sum{x_i}\sum{y_i} }
                     { \sum{x_i^2} - \frac{1}{n}(\sum{x_i})^2 } = \frac{ \mathrm{Cov}[x,y] }{ \mathrm{Var}[x] } , \quad
    \hat\alpha = \overline{y} - \hat\beta\,\overline{x}\ .
  


Alternative derivationsIn the previous section the least squares estimator \scriptstyle\hat\beta was obtained as a value that minimizes the sum of squared residuals of the model. However it is also possible to derive the same estimator from other approaches. In all cases the formula for OLS estimator remains the same: {{nowrap}}, the only difference is in how we interpret this result.
Geometric approach
{{main}}
For mathematicians, OLS is an approximate solution to an overdetermined system of linear equations {{nowrap}}, where β is the unknown. Assuming the system cannot be solved exactly (the number of equations n is much larger than the number of unknowns p), we are looking for a solution that could provide the smallest discrepancy between the right- and left- hand sides. In other words, we are looking for the solution that satisfies


    \hat\beta = {\rm arg}\min_\beta\,\lVert y - X\beta \rVert,
  
where ||·|| is the standard L2&nbsp;norm in the n-dimensional Euclidean space Rn. The predicted quantity Xβ is just a certain linear combination of the vectors of regressors. Thus, the residual vector {{nowrap}} will have the smallest length when y is projected orthogonally onto the linear subspace spanned by the columns of X. The OLS estimator \scriptstyle\hat\beta in this case can be interpreted as the coefficients of vector decomposition of {{nowrap}} along the basis of X.
Maximum likelihoodThe OLS estimator is identical to the maximum likelihood estimator under the normality assumption for the error terms.{{harvtxt}}[proof] This normality assumption has historical importance, as it provided the basis for the early work in linear regression analysis by Yule and Pearson.{{Citation needed}} From the properties of MLE, we can infer that the OLS estimator is asymptotically efficient (in the sense of attaining the Cramér-Rao bound for variance) if the normality assumption is satisfied.{{harvtxt}}
Generalized method of momentsIn iid case the OLS estimator can also be viewed as a GMM estimator arising from the moment conditions


    \mathrm{E}\big[\, x_i(y_i - x_i'\beta) \,\big] = 0.
  
These moment conditions state that the regressors should be uncorrelated with the errors. Since xi is a p-vector, the number of moment conditions is equal to the dimension of the parameter vector β, and thus the system is exactly identified. This is the so-called classical GMM case, when the estimator does not depend on the choice of the weighting matrix.Note that the original strict exogeneity assumption {{nowrap}} implies a far richer set of moment conditions than stated above. In particular, this assumption implies that for any vector-function ƒ, the moment condition {{nowrap}} will hold. However it can be shown using the Gauss–Markov theorem that the optimal choice of function ƒ is to take {{nowrap}}, which results in the moment equation posted above.
Finite sample propertiesFirst of all, under the strict exogeneity assumption the OLS estimators \scriptstyle\hat\beta and s2 are unbiased, meaning that their expected values coincide with the true values of the parameters:{{harvtxt}}[proof]


    \operatorname{E}[\, \hat\beta \,| X \,] = \beta, \quad \operatorname{E}[\,s^2\,|X\,] = \sigma^2.
  
If the strict exogeneity does not hold (as is the case with many time series models, where exogeneity is assumed only with respect to the past shocks but not the future ones), then these estimators will be biased in finite samples.The variance-covariance matrix of \scriptstyle\hat\beta is equal to {{harvtxt}}


    \operatorname{Var}[\, \hat\beta \,| X \,] = \sigma^2(X'X)^{-1}.
  
In particular, the standard error of each coefficient \scriptstyle\hat\beta_j is equal to square root of the j-th diagonal element of this matrix. The estimate of this standard error is obtained by replacing the unknown quantity σ2 with its estimate s2. Thus,


    \widehat{\operatorname{s.\!e}}(\hat{\beta}_j) = \sqrt{s^2 (X'X)^{-1}_{jj}}
  

It can also be easily shown that the estimator \scriptstyle\hat\beta is uncorrelated with the residuals from the model:


    \operatorname{Cov}[\, \hat\beta,\hat\varepsilon \,|X\,] = 0.
  

The Gauss–Markov theorem states that under the spherical errors assumption (that is, the errors should be uncorrelated and homoscedastic) the estimator \scriptstyle\hat\beta is efficient in the class of linear unbiased estimators. This is called the best linear unbiased estimator (BLUE). Efficiency should be understood as if we were to find some other estimator \scriptstyle\tilde\beta which would be linear in y and unbiased, then 


    \operatorname{Var}[\, \tilde\beta \,| X \,] - \operatorname{Var}[\, \hat\beta \,| X \,] \geq 0
  
in the sense that this is a nonnegative-definite matrix. This theorem establishes optimality only in the class of linear unbiased estimators, which is quite restrictive. Depending on the distribution of the error terms ε, other, non-linear estimators may provide better results than OLS.
Assuming normalityThe properties listed so far are all valid regardless of the underlying distribution of the error terms. However if you are willing to assume that the normality assumption holds (that is, that {{nowrap}}), then additional properties of the OLS estimators can be stated.The estimator \scriptstyle\hat\beta is normally distributed, with mean and variance as given before:{{harvtxt}}


    \hat\beta\ \sim\ \mathcal{N}\big(\beta,\ \sigma^2(X'X)^{-1}\big)
  
This estimator reaches the Cramér–Rao bound for the model, and thus is optimal in the class of all unbiased estimators. Note that unlike the Gauss–Markov theorem, this result establishes optimality among both linear and non-linear estimators, but only in the case of normally distributed error terms.The estimator s2 will be proportional to the chi-squared distribution:{{harvtxt}}


    s^2\ \sim\ \frac{\sigma^2}{n-p} \cdot \chi^2_{n-p}
  
The variance of this estimator is equal to {{nowrap}}, which does not attain the Cramér–Rao bound of 2σ4/n. However it was shown that there are no unbiased estimators of σ2 with variance smaller than that of the estimator s2.{{harvtxt}} If we are willing to allow biased estimators, and consider the class of estimators that are proportional to the sum of squared residuals (SSR) of the model, then the best (in the sense of the mean squared error) estimator in this class will be {{nowrap}}, which even beats the Cramér–Rao bound in case when there is only one regressor ({{nowrap}}).{{harvtxt}}Moreover, the estimators \scriptstyle\hat\beta and s2 are independent,{{harvtxt}} the fact which comes in useful when constructing the t- and F-tests for the regression.
Influential observationsAs was mentioned before, the estimator \scriptstyle\hat\beta is linear in y, meaning that it represents a linear combination of the dependent variables yis. The weights in this linear combination are functions of the regressors X, and generally are unequal. The observations with high weights are called influential because they have a more pronounced effect on the value of the estimator.To analyze which observations are influential we remove a specific j-th observation and consider how much the estimated quantities are going to change (similarly to the jackknife method). It can be shown that the change in the OLS estimator for β will be equal to {{harvtxt}}


    \hat\beta^{(j)} - \hat\beta = - \frac{1}{1-h_j} (X'X)^{-1}x'_j\hat\varepsilon_j\,,
  
where {{nowrap}} is the j-th diagonal element of the hat matrix P, and xj is the vector of regressors corresponding to the j-th observation. Similarly, the change in the predicted value for j-th observation resulting from omitting that observation from the dataset will be equal to 


    \hat{y}_j^{(j)} - \hat{y}_j = x'_j\hat\beta^{(j)} - x'_j\hat\beta = - \frac{h_j}{1-h_j}\,\hat\varepsilon_j
  

From the properties of the hat matrix, {{nowrap}}, and they sum up to p, so that on average {{nowrap}}. These quantities hj are called the leverages, and observations with high hjs — leverage points.{{harvtxt}} Usually the observations with high leverage ought to be scrutinized more carefully, in case they are erroneous, or outliers, or in some other way atypical of the rest of the dataset.
Partitioned regressionSometimes the variables and corresponding parameters in the regression can be logically split into two groups, so that the regression takes form


    y = X_1\beta_1 + X_2\beta_2 + \varepsilon,
  
where X1 and X2 have dimensions n×p1, n×p2, and β1, β2 are p1×1 and p2×1 vectors, with {{nowrap}}.The Frisch–Waugh–Lovell theorem states that in this regression the residuals \hat\varepsilon and the OLS estimate \scriptstyle\hat\beta_2 will be numerically identical to the residuals and the OLS estimate for β2 in the following regression:{{harvtxt}}


    M_1y = M_1X_2\beta_2 + \eta\,,
  
where M1 is the annihilator matrix for regressors X1.The theorem can be used to establish a number of theoretical results. For example, having a regression with a constant and another regressor is equivalent to subtracting the means from the dependent variable and the regressor and then running the regression for the demeaned variables but without the constant term.
Constrained estimationSuppose it is known that the coefficients in the regression satisfy a system of linear equations


    H_0\!:\ \ Q'\beta = c, \,
  
where Q is a p×q matrix of full rank, and c is a q×1 vector of known constants, where {{nowrap}}. In this case least squares estimation is equivalent to minimizing the sum of squared residuals of the model subject to the constraint H0. The constrained least squares (CLS) estimator can be given by an explicit formula:{{harvtxt}}


    \hat\beta^c = \hat\beta - (X'X)^{-1}Q\Big(Q'(X'X)^{-1}Q\Big)^{-1}(Q'\hat\beta - c)
  

This expression for the constrained estimator is valid as long as the matrix X′X is invertible. It was assumed from the beginning of this article that this matrix is of full rank, and it was noted that when the rank condition fails, β will not be identifiable. However it may happen that adding the restriction H0 makes β identifiable, in which case one would like to find the formula for the estimator. The estimator is equal to {{harvtxt}}


    \hat\beta^c = R(R'X'XR)^{-1}R'X'y + \Big(I_p - R(R'X'XR)^{-1}R'X'X\Big)Q(Q'Q)^{-1}c,
  
where R is a p×(p−q) matrix such that the matrix {{nowrap}} is non-singular, and {{nowrap}}. Such a matrix can always be found, although generally it is not unique. The second formula coincides with the first in case when X′X is invertible.
Large sample propertiesThe least squares estimators are point estimates of the linear regression model parameters β. However generally we also want to know how close those estimates might be to the true values of parameters. In other words, we want to construct the interval estimates.Since we haven't made any assumption about the distribution of error term εi, it is impossible to infer the distribution of the estimators \hat\beta and \hat\sigma^2. Nevertheless, we can apply the law of large numbers and central limit theorem to derive their asymptotic properties as sample size n goes to infinity. Now of course in practice sample size doesn't go anywhere, however it is customary to pretend that n is "large enough" so that the true distribution of OLS estimator is close to its asymptotic limit, and the former may be approximately replaced by the latter.We can show that under the model assumptions, least squares estimator for β is consistent (that is \hat\beta converges in probability to β) and asymptotically normal:[proof][Q_{xx} is undefined.]

\sqrt{n}(\hat\beta - \beta)\ \xrightarrow{d}\ \mathcal{N}\big(0,\;\sigma^2Q_{xx}^{-1}\big),
where Q_{xx} = X'X.Using this asymptotic distribution, approximate two-sided confidence intervals for the j-th component of the vector \hat\beta can be constructed as

\beta_j \in \bigg[\ 
    \hat\beta_j \pm q^{\mathcal{N}(0,1)}_{1-\alpha/2}\!\sqrt{\tfrac{1}{n}\hat\sigma^2\big[\hat{Q}_{xx}^{-1}\big]_{jj}}
    \ \bigg]   at the 1 − α confidence level,
where q denotes the quantile function of standard normal distribution, and [·]jj is the j-th diagonal element of a matrix.Similarly, the least squares estimator for σ2 is also consistent and asymptotically normal (provided that the fourth moment of εi exists) with limiting distribution

\sqrt{n}(\hat\sigma^2-\sigma^2)\ \xrightarrow{d}\ \mathcal{N}\big(0,\;\operatorname{E}[\varepsilon_i^4]-\sigma^4\big). 

These asymptotic distributions can be used for prediction, testing hypotheses, constructing other estimators, etc.. As an example consider the problem of prediction. Suppose x_0 is some point within the domain of distribution of the regressors, and one wants to know what the response variable would have been at that point. The mean response is the quantity y_0=x'_0\beta, whereas the predicted response is \hat{y}_0=x'_0\hat\beta. Clearly the predicted response is a random variable, its distribution can be derived from that of \hat\beta:

\sqrt{n}(\hat{y}_0 - y_0)\ \xrightarrow{d}\ \mathcal{N}\big(0,\;\sigma^2x'_0Q_{xx}^{-1}x_0\big),
which allows construct confidence intervals for mean  response y_0 to be constructed:

y_0\in\bigg[\ x_0'\hat\beta \pm q^{\mathcal{N}(0,1)}_{1-\alpha/2}\!\sqrt{\tfrac{1}{n}\hat\sigma^2x'_0\hat{Q}_{xx}^{-1}x_0}\ \bigg]   at the 1 − α confidence level.


Hypothesis testing
{{Empty section}}
Example with real data
The following data set gives average heights and weights for American women aged 30–39 (source: The World Almanac and Book of Facts, 1975).


|- style="text-align:right;"
! style="text-align:left;" | &nbsp;Height (m):&nbsp;
| 1.47 || 1.50 || 1.52 || 1.55 || 1.57 || 1.60 || 1.63 || 1.65 || 1.68 || 1.70 || 1.73 || 1.75 || 1.78 || 1.80 || 1.83
|- style="text-align:right;"
! style="text-align:left;" | &nbsp;Weight (kg):&nbsp;
|52.21 ||53.12 ||54.48 ||55.84 ||57.20 ||58.57 ||59.93 ||61.29 ||63.11 ||64.47 ||66.28 ||68.10 ||69.92 ||72.19 ||74.46
|}When only one dependent variable is being modeled, a scatterplot will suggest the form and strength of the relationship between the dependent variable and regressors. It might also reveal outliers, heteroscedasticity, and other aspects of the data that may complicate the interpretation of a fitted regression model.  The scatterplot suggests that the relationship is strong and can be approximated as a quadratic function. OLS can handle non-linear relationships by introducing the regressor HEIGHT2.  The regression model then becomes a multiple linear model:

w_i = \beta_1 + \beta_2 h_i + \beta_3 h_i^2 + \varepsilon_i.

The output from most popular statistical packages will look similar to this:



|colspan="6"| Method: Least SquaresDependent variable: WEIGHTIncluded observations: 15
|-
|colspan="6"| |- align="right"
!align="left" | Variable 
!align="right"| Coefficient
!
!align="right"| Std.Error
!style="padding-left:6pt;"| t-statistic
!style="padding-left:6pt;"| p-value
|-
|colspan="6"| |-
|- align="right"
|align="left"| const               || 128.8128    || || 16.3083   || 7.8986      || 0.0000
|- align="right"
|align="left"| HEIGHT              || –143.1620   || || 19.8332   || –7.2183     || 0.0000
|- align="right"
|align="left"| HEIGHT2  || 61.9603     || || 6.0084    || 10.3122     || 0.0000
|-
|colspan="6"| |-
| R2
|align="right"| 0.9989
|rowspan="6"| &nbsp; &nbsp;
|colspan="2"| S.E. of regression
|align="right"| 0.2516 
|-
| Adjusted R2
|align="right"| 0.9987
|colspan="2"| Model sum-of-sq
|align="right"| 692.61
|-
| Log-likelihood
|align="right"| 1.0890
|colspan="2"| Residual sum-of-sq
|align="right"| 0.7595
|-
| Durbin–Watson stats.
|align="right"| 2.1013
|colspan="2"| Total sum-of-sq
|align="right"| 693.37
|-
| Akaike criterion
|align="right"| 0.2548
|colspan="2"| F-statistic
|align="right"| 5471.2
|-
| Schwarz criterion
|align="right"| 0.3964
|colspan="2"| p-value (F-stat)
|align="right"| 0.0000
|}In this table:

The Coefficient column gives the least squares estimates of parameters βj
The Std. errors column shows standard errors of each coefficient estimate: \hat\sigma_j=\big(\tfrac{1}{n}\hat\sigma^2[\hat{Q}_{xx}^{-1}]_{jj}\big)^{1/2}
The t-statistic and p-value columns are testing whether any of the coefficients might be equal to zero. The t-statistic is calculated simply as t=\hat\beta_j/\hat\sigma_j. If the errors ε approximately follow a normal distribution, t follows a Student-t distribution.  Under weaker conditions, t is asymptotically normal. Large values of t indicate that the null hypothesis can be rejected and that the corresponding coefficient is not zero. The second column, p-value, expresses the results of the hypothesis test as a significance level.  Conventionally, p-values smaller than 0.05 are taken as evidence that the population coefficient is nonzero.
R-squared is the coefficient of determination indicating goodness-of-fit of the regression. This statistic will be equal to one if fit is perfect, and to zero when regressors X have no explanatory power whatsoever. This is a biased estimate of the population R-squared, and will never decrease if additional regressors are added, even if they are irrelevant.
Adjusted R-squared is a slightly modified version of R^2, designed to penalize for the excess number of regressors which do not add to the explanatory power of the regression. This statistic is always smaller than R^2, can decrease as you add new regressors, and even be negative for poorly fitting models:

\overline{R}^2 = 1 - \tfrac{n-1}{n-p}(1-R^2)
Log-likelihood is calculated under the assumption that errors follow normal distribution. Even though the assumption is not very reasonable, this statistic may still find its use in conducting LR tests.
Durbin–Watson statistic tests whether there is any evidence of serial correlation between the residuals. As a rule of thumb, the value smaller than 2 will be an evidence of positive correlation.
Akaike information criterion and Schwarz criterion are both used for model selection. Generally when comparing two alternative models, smaller values of one of these criteria will indicate a better model.{{Cite book}} 
Standard error of regression is an estimate of σ, standard error of the error term.
Total sum of squares, model sum of squared, and residual sum of squares tell us how much of the initial variation in the sample were explained by the regression.
F-statistic tries to test the hypothesis that all coefficients (except the intercept) are equal to zero. This statistic has F(p–1,n–p) distribution under the null hypothesis and normality assumption, and its p-value indicates probability that the hypothesis is indeed true. Note that when errors are not normal this statistic becomes invalid, and other tests such as for example Wald test or LR test should be used.


Ordinary least squares analysis often includes the use of diagnostic plots designed to detect departures of the data from the assumed form of the model.  These are some of the common diagnostic plots:

Residuals against the explanatory variables in the model. A non-linear relation between these variables suggests that the linearity of the conditional mean function may not hold.  Different levels of variability in the residuals for different levels of the explanatory variables suggests possible heteroscedasticity.
Residuals against explanatory variables not in the model. Any relation of the residuals to these variables would suggest considering these variables for inclusion in the model.
Residuals against the fitted values, \hat{y}.
Residuals against the preceding residual.  This plot may identify serial correlations in the residuals.

An important consideration when carrying out statistical inference using regression models is how the data were sampled.  In this example, the data are averages rather than measurements on individual women.  The fit of the model is very good, but this does not imply that the weight of an individual woman can be predicted with high accuracy based only on her height.
BewareThis example also demonstrates that sophisticated calculations will not overcome the use of badly prepared data. The heights were originally given in inches, and have been converted to the nearest centimetre. Since the conversion factor is one inch to 2.54cm, this is not a correct conversion. The original inches can be recovered by Round(x/0.0254) and then re-converted to metric: if this is done, the results become const      height   Height2
128.8128  -143.162   61.96033  incorrectly converted to metric.
119.0205  -131.5076  58.5046   correctly converted.Thus a seemingly small variation in the data has a real effect.
See also

Numerical methods for linear least squares
Non-linear least squares


References{{reflist}}{{refbegin}}

{{cite book}}
{{cite book}}
{{cite book}}
{{cite book}}
{{cite book}}
{{refend}}{{Least Squares and Regression Analysis}}{{DEFAULTSORT:Ordinary Least Squares}}



de:Methode der kleinsten Quadrate
fa:کمینه مربعات خطی
fr:Méthode des moindres carrés ordinaire