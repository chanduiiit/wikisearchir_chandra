titleIn computer programming, a parameter is a special kind of variable, used in a subroutine to refer to one of the pieces of data provided as input to the subroutine.In this article, the term "subroutine" refers to any subroutine-like construct, which have different names and slightly different meanings depending on the programming language being discussed. These pieces of data are called arguments. An ordered list of parameters is usually included in the definition of a subroutine, so that, each time the subroutine is called, its arguments for that call can be assigned to the corresponding parameters.Just as in standard mathematical usage, the argument is thus the actual value passed to a function, procedure, or routine (such as 37 in log(37)), whereas the parameter is a reference to that value inside the implementation of the function (log in this case). See the Parameters and arguments section for more information.In the most common case, call-by-value, a parameter acts within the subroutine as a local (isolated) copy of the argument, but in other cases, e.g. call-by-reference, the argument supplied by the caller can be affected by actions within the called subroutine (as discussed in evaluation strategy).The semantics for how parameters can be declared and how the arguments get passed to the parameters of subroutines are defined by the language, but the details of how this is represented in any particular computer system depend on the calling conventions of that system.

Example
The following program in the C programming language defines a function that is named "sales_tax" and has one parameter named "price". The type of price is "double" (i.e. a double-precision floating point number). The function's return type is also a double.
double sales_tax(double price)
{
    return 0.05 * price;
}

After the function has been defined, it can be invoked as follows:
sales_tax(10.00);

In this example, the function has been invoked with the number 10.00. When this happens, 10.00 will be assigned to price, and the function begins calculating its result. The steps for producing the result are specified below enclosed in {} "0.05 * price" indicates that the first thing to do is multiply 0.05 by the value of price, which gives 0.50. "return" means the function will produce the result of "0.05 * price". Therefore, the final result is 0.50.
Parameters and arguments
These two terms are sometimes loosely used interchangeably; in particular, "argument" is sometimes used in place of "parameter". Nevertheless, there is a difference. Properly, parameters appear in procedure definitions; arguments appear in procedure calls. A parameter is an intrinsic property of the procedure, included in its definition. For example, in many languages, a minimal procedure to add two supplied integers together and calculate the sum total would need two parameters, one for each expected integer. In general, a procedure may be defined with any number of parameters, or no parameters at all. If a procedure has parameters, the part of its definition that specifies the parameters is called its parameter list.By contrast, the arguments are the values actually supplied to the procedure when it is called. Unlike the parameters, which form an unchanging part of the procedure's definition, the arguments can, and often do, vary from call to call. Each time a procedure is called, the part of the procedure call that specifies the arguments is called the argument list.Although parameters are also commonly referred to as arguments, arguments are more properly thought of as the actual values or references assigned to the parameter variables when the subroutine is called at run-time. When discussing code that is calling into a subroutine, any values or references passed into the subroutine are the arguments, and the place in the code where these values or references are given is the parameter list. When discussing the code inside the subroutine definition, the variables in the subroutine's parameter list are the parameters, while the values of the parameters at runtime are the arguments.Many programmers use parameter and argument interchangeably, depending on context to distinguish the meaning.  The term formal parameter refers to the variable as found in the function definition (parameter), while actual parameter refers to the actual value passed (argument).To better understand the difference, consider the following function written in C:

int sum(int addend1, int addend2)
{
    return addend1 + addend2;
}
The function sum has two parameters, named addend1 and addend2. It adds the values passed into the parameters, and returns the result to the subroutine's caller (using a technique automatically supplied by the C compiler).The code which calls the sum function might look like this:

int sumValue;
int value1 = 40;
int value2 = 2;

sumValue = sum(value1, value2);
The variables value1 and value2 are initialized with values. value1 and value2 are both arguments to the sum function in this context.At runtime, the values assigned to these variables are passed to the function sum as arguments. In the sum function, the parameters addend1 and addend2 are evaluated, yielding the arguments 40 and 2, respectively. The values of the arguments are added, and the result is returned to the caller, where it is assigned to the variable sumValue.Because of the difference between parameters and arguments, it is possible to supply inappropriate arguments to a procedure. The call may supply too many or too few arguments; one or more of the arguments may be a wrong type; or arguments may be supplied in the wrong order. Any of these situations causes a mismatch between the parameter and argument lists, and the procedure will often return an unintended answer or generate a runtime error.
Alternative convention in EiffelWithin the Eiffel software development method and language, the terms argument and parameter have distinct uses established by convention. The term argument is used exclusively in reference to a routine's inputs,Meyer, Bertrand. Object-Oriented Software Construction, 2nd Edition, Prentice Hall, 1997, p 444. and the term parameter is used exclusively in type parameterization for generic classes.Meyer, p. 96. Consider the following routine definition:
    sum (addend1: INTEGER; addend2: INTEGER): INTEGER
        do
            Result := addend1 + addend2
        end

The routine sum takes two arguments addend1 and addend2, which are called the routine's formal arguments.  A call to sum specifies actual arguments, as shown below with value1 and value2.
    sum_value: INTEGER
    value1: INTEGER = 40
    value2: INTEGER = 2
                …
            sum_value := sum (value1, value2)    

Parameters are also thought of as either formal or actual. Formal generic parameters are used in the definition of generic classes. In the example below, the class HASH_TABLE  is declared as a generic class which has two formal generic parameters, G representing data of interest and K representing the hash key for the data:
class HASH_TABLE [G, K -> HASHABLE] 
            …

When a class becomes a client to HASH_TABLE, the formal generic parameters are substituted with actual generic parameters in a generic derivation. In the following attribute declaration, my_dictionary is to be used as a character string based dictionary. As such, both data and key formal generic parameters are substituted with actual generic parameters of type STRING.
    my_dictionary: HASH_TABLE [STRING, STRING] 


DatatypesIn strongly typed programming languages, each parameter's type must be specified in the procedure's declaration.  Languages using type inference attempt to discover the types automatically from the function's body and usage. Dynamically typed programming languages defer type resolution until run-time. Weakly typed languages perform little to no type resolution, relying instead on the programmer for correctness.Some languages use a special keyword (e.g. void) to indicate that the subroutine has no parameters; in formal type theory, such functions take an empty parameter list (whose type is not void, but rather unit).
Argument passingThe exact mechanism for assigning arguments to parameters, called argument passing, depends upon the evaluation strategy used for that parameter (typically call-by-value), which may be specified using keywords.
Default argumentsSome programming languages such as Ada, C++, Clojure, Common Lisp, Fortran 90, Python, Ruby, and Windows PowerShell allow for a default argument to be explicitly or implicitly given in a subroutine's declaration. This allows the caller to omit that argument when calling the subroutine. If the default argument is explicitly given, then that value is used if it is not provided by the caller. If the default argument is implicit (sometimes by using a keyword such as Optional) then the language provides a well-known value (such as null, Empty, zero, an empty string, etc.) if a value is not provided by the caller.PowerShell example:function doc($g = 1.21) {
  "$g gigawatts? $g gigawatts? Great Scott!"
}

PS> doc
1.21 gigawatts? 1.21 gigawatts? Great Scott!

PS> doc 88
88 gigawatts? 88 gigawatts? Great Scott!Default arguments can be seen as a special case of the variable-length argument list.
Variable-length parameter listsSome languages allow subroutines to be defined to accept a variable number of arguments. For such languages, the subroutines must iterate through the list of arguments.PowerShell example:function marty {
  $args | foreach { "back to the year $_" }
}

PS> marty 1985
back to the year 1985

PS> marty 2015 1985 1955
back to the year 2015
back to the year 1985
back to the year 1955

Named parametersSome programming languages allow subroutines to have named parameters. This allows the calling code to be more self-documenting. It also provides more flexibility to the caller, often allowing the order of the arguments to be changed, or for arguments to be omitted as needed.PowerShell example:function jennifer($young, $old) {
  "Young Jennifer: I'm $young!"
  "Old Jennifer: I'm $old!"
}

PS> jennifer 'old' 'young'
Young Jennifer: I'm old!
Old Jennifer: I'm young!

PS> jennifer -old 'young' -young 'old'
Young Jennifer: I'm old!
Old Jennifer: I'm young!

Multiple parameters in functional languagesIn lambda calculus, each function has exactly one parameter. What is thought of as functions with multiple parameters is usually represented in lambda calculus as a function which takes the first argument, and returns a function which takes the rest of the arguments; this is a transformation known as currying. Some programming languages, like ML and Haskell, follow this scheme. In these languages, every function has exactly one parameter, and what may look like the definition of a function of multiple parameters, is actually syntactic sugar for the definition of a function that returns a function, etc. Function application is left-associative in these languages as well as in lambda calculus, so what looks like an application of a function to multiple arguments is correctly evaluated as the function applied to the first argument, then the resulting function applied to the second argument, etc.


See also

Command-line argument
Evaluation strategy
Operator overloading
Free variables and bound variables


References


{{DEFAULTSORT:Parameter (Computer Science)}}



ar:وسيط (برمجة حاسوب)
cs:Parametr funkce
de:Parameter (Informatik)
el:Παράμετρος (υπολογιστές)
es:Argumento (informática)
is:Færibreyta
ja:引数
mhr:Аргумент
pl:Parametr (informatyka)
pt:Parâmetro (ciência da computação)
ru:Параметр (программирование)
sl:Parameter (računalništvo)
fi:Parametri (tietotekniikka)
uk:Параметр (програмування)