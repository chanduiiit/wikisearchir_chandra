title{{Other uses}}
{{Infobox Italian comune}}Pisa ({{IPAc-en}}; {{IPA-it}}http://www.dizionario.rai.it//poplemma.aspx?lid=27983&r=636, Dizionario italiano d'ortografia e di pronunzia) is a city in Tuscany, Central Italy, on the right bank of the mouth of the River Arno on the Tyrrhenian Sea. It is the capital city of the Province of Pisa. Although Pisa is known worldwide for its leaning tower (the bell tower of the city's cathedral), the city of over 88,332 residents (around 200,000 with the metropolitan area) contains more than 20 other historic churches, several palaces and various bridges across the River Arno. The city is also home of the University of Pisa, which has a history going back to the 12th century and also has the mythic Napoleonic Scuola Normale Superiore di Pisa and Sant'Anna School of Advanced Studies as the best Superior Graduate Schools in Italy.
History
Ancient timesPisa lies at the junction of two rivers, the Arno and the Serchio, which form a laguna at the Tyrrhenian Sea.  The origin of the name, Pisa, is a mystery. While the origin of the city had remained unknown for centuries, the Pelasgi, the Greeks, the Etruscans, and the Ligurians had variously been proposed as founders of the city. Archeological remains from the 5th century BC confirmed the existence of a city at the sea, trading with Greeks and Gauls. The presence of an Etruscan necropolis, discovered during excavations in the {{lang}} in 1991, clarified its Etruscan origins.Ancient Roman authors referred to Pisa as an old city. Servius wrote that the Teuti, or Pelopes, the king of the Pisei, founded the town thirteen centuries before the start of the common era. Strabo referred Pisa's origins to the mythical Nestor, king of Pylos, after the fall of Troy. Virgil, in his Aeneid, states that Pisa was already a great center by the times described; the settlers from the Alpheus coast have been credited with the founding of the city in the 'Etruscan lands'. 
The maritime role of Pisa should have been already prominent if the ancient authorities ascribed to it the invention of the naval ram. Pisa took advantage of being the only port along the western coast from Genoa (then a small village) to Ostia. Pisa served as a base for Roman naval expeditions against Ligurians, Gauls and Carthaginians. In 180 BC, it became a Roman colony under Roman law, as {{lang}}. In 89 BC, {{lang}} became a municipium. Emperor Augustus fortified the colony into an important port and changed the name in {{lang}}.
Late Antiquity and Early Middle AgesDuring the later years of the Roman Empire, Pisa did not decline as much as the other cities of Italy, probably thanks to the complexity of its river system and its consequent ease of defence. In the 7th century Pisa helped Pope Gregory&nbsp;I by supplying numerous ships in his military expedition against the Byzantines of Ravenna: Pisa was the sole Byzantine centre of Tuscia to fall peacefully in Lombard hands, through assimilation with the neighbouring region where their trading interests were prevailing. Pisa began in this way its rise to the role of main port of the Upper Tyrrhenian Sea and became the main trading centre between Tuscany and Corsica, Sardinia and the southern coasts of France and Spain.{{Infobox World Heritage Site}}After Charlemagne had defeated the Lombards under the command of Desiderius in&nbsp;774, Pisa went through a crisis but soon recovered. Politically it became part of the duchy of Lucca. In 930 Pisa became the county centre (status it maintained until the arrival of Otto I) within the mark of Tuscia. Lucca was the capital but Pisa was the most important city, as in the middle of 10th century Liutprand of Cremona, bishop of Cremona, called Pisa {{lang}} ("capital of the province of Tuscia"), and one century later the marquis of Tuscia was commonly referred to as "marquis of Pisa". In 1003 Pisa was the protagonist of the first communal war in Italy, against Lucca of course. From the naval point of view, since the 9th century the emergence of the Saracen pirates urged the city to expand its fleet: in the following years this fleet gave the town an opportunity for more expansion. In 828 Pisan ships assaulted the coast of North Africa. In 871 they took part in the defence of Salerno from the Saracens. In 970 they gave also strong support to the Otto&nbsp;I's expedition, defeating a Byzantine fleet in front of Calabrese coasts.
11th century{{Main}}
The power of Pisa as a mighty maritime nation began to grow and reached its apex in the 11th century when it acquired traditional fame as one of the four main historical Maritime Republics of Italy ({{lang}}).At that time, the city was a very important commercial centre and controlled a significant Mediterranean merchant fleet and navy. It expanded its powers by the sack in 1005 of {{lang}} in the south of Italy. Pisa was in continuous conflict with the Saracens, who had their bases in Corsica, for control of the Mediterranean. In 1017 Sardinian Giudicati were  military supported by Pisa, in alliance with Genoa, to defeat the Saracen king Mugahid that settled a logistic base on the north  of Sardinia the year before. This victory gave Pisa the supremacy in the Tyrrhenian Sea. When the Pisans subsequently ousted the Genoese from Sardinia, a new conflict and rivalry was born between these mighty marine republics. Between 1030 and 1035, Pisa went on to successfully defeat several rival towns in Sicily and conquer Carthage in North Africa. In 1051–1052 the admiral Jacopo Ciurini conquered Corsica, provoking more resentment from the Genoese. In 1063 admiral Giovanni Orlando, coming to the aid of the Norman Roger I, took Palermo from the Saracen pirates. The gold treasure taken from the Saracens in Palermo allowed the Pisans to start the building of their cathedral and the other monuments which constitute the famous {{lang}}.In 1060 Pisa had to engage in their first battle with Genoa. The Pisan victory helped to consolidate its position in the Mediterranean. Pope Gregory VII recognised in 1077 the new "Laws and customs of the sea" instituted by the Pisans, and emperor Henry IV granted them the right to name their own consuls, advised by a Council of Elders. This was simply a confirmation of the present situation, because in those years the marquis had already been excluded from power. In 1092 Pope Urban II awarded Pisa the supremacy over Corsica and Sardinia, and at the same time raising the town to the rank of archbishopric.Pisa sacked the Tunisian city of Mahdia in 1088. Four years later Pisan and Genoese ships helped Alfonso VI of Castilla to push El Cid out of Valencia. A Pisan fleet of 120 ships also took part in the First Crusade and the Pisans were instrumental in the taking of Jerusalem in 1099. On their way to the Holy Land the ships did not miss the occasion to sack some Byzantine islands: the Pisan crusaders were led by their archbishop Daibert, the future patriarch of Jerusalem. Pisa and the other {{lang}} took advantage of the crusade to establish trading posts and colonies in the Eastern coastal cities of the Levant. In particular the Pisans founded colonies in Antiochia, Acre, Jaffa, Tripoli, Tyre, Latakia and Accone. They also had other possessions in Jerusalem and Caesarea, plus smaller colonies (with lesser autonomy) in Cairo, Alexandria and of course Constantinople, where the Byzantine Emperor Alexius I Comnenus granted them special mooring and trading rights. In all these cities the Pisans were granted privileges and immunity from taxation, but had to contribute to the defence in case of attack. In the 12th century the Pisan quarter in the Eastern part of Constantinople had grown to 1,000 people. For some years of that century Pisa was the most prominent merchant and military ally of the Byzantine Empire, overcoming Venice itself.
12th centuryIn 1113 Pisa and the Pope Paschal II set up, together with the count of Barcelona and other contingents from Provence and Italy (Genoese excluded), a war to free the Balearic Islands from the Moors: the queen and the king of Majorca were brought in chains to Tuscany. Even though the Almoravides soon reconquered the island, the booty taken helped the Pisans in their magnificent programme of buildings, especially the cathedral and Pisa gained a role of pre-eminence in the Western Mediterranean.In the following years the mighty Pisan fleet, led by archbishop Pietro Moriconi, drove away the Saracens after ferocious combats. Though short-lived, this success of Pisa in Spain increased the rivalry with Genoa. Pisa's trade with the Languedoc and Provence (Noli, Savona, Fréjus and Montpellier) were an obstacle to the Genoese interests in cities like Hyères, Fos, Antibes and Marseille.The war began in 1119 when the Genoese attacked several galleys on their way to the motherland, and lasted until 1133. The two cities fought each other on land and at sea, but hostilities were limited to raids and pirate-like assaults.In June 1135, Bernard of Clairvaux took a leading part in the Council of Pisa, asserting the claims of pope Innocent II against those of pope Anacletus II, who had been elected pope in 1130 with Norman support but was not recognised outside Rome. Innocent II resolved the conflict with Genoa, establishing the sphere of influence of Pisa and Genoa. Pisa could then, unhindered by Genoa, participate in the conflict of Innocent II against king Roger II of Sicily. Amalfi, one of the Maritime Republics (though already declining under Norman rule), was conquered on 6 August 1136: the Pisans destroyed the ships in the port, assaulted the castles in the surrounding areas and drove back an army sent by Roger from Aversa. This victory brought Pisa to the peak of its power and to a standing equal to Venice. Two years later its soldiers sacked Salerno.In the following years Pisa was one of the staunchest supporters of the Ghibelline party. This was much appreciated by Frederick I. He issued in 1162 and 1165 two important documents, with the following grants: apart from the jurisdiction over the Pisan countryside, the Pisans were granted freedom of trade in the whole Empire, the coast from Civitavecchia to Portovenere, a half of Palermo, Messina, Salerno and Naples, the whole of Gaeta, Mazara and Trapani, and a street with houses for its merchants in every city of the Kingdom of Sicily. Some of these grants were later confirmed by Henry VI, Otto IV and Frederick II. They marked the apex of Pisa's power, but also spurred the resentment of cities like Lucca, Massa, Volterra and Florence, who saw their aim to expand towards the sea thwarted. The clash with Lucca also concerned the possession of the castle of Montignoso and mainly the control of the {{lang}}, the main trade route between Rome and France. Last but not least, such a sudden and large increase of power by Pisa could only lead to another war with Genoa.Genoa had acquired a largely dominant position in the markets of Southern France. The war began presumably in 1165 on the Rhône, when an attack on a convoy, directed to some Pisan trade centres on the river, by the Genoese and their ally, the count of Toulouse failed. Pisa on the other hand was allied to Provence. The war continued until 1175 without significant victories. Another point of attrition was Sicily, where both the cities had privileges granted by Henry VI. In 1192, Pisa managed to conquer Messina. This episode was followed by a series of battles culminating in the Genoese conquest of Syracuse in 1204. Later, the trading posts in Sicily were lost when the new Pope Innocent III, though removing the excommunication cast over Pisa by his predecessor Celestine III, allied himself with the Guelph League of Tuscany, led by Florence. Soon he stipulated a pact with Genoa too, further weakening the Pisan presence in Southern Italy.To counter the Genoese predominance in the southern Tyrrhenian Sea, Pisa strengthened its relationship with their Spanish and French traditional bases (Marseille, Narbonne, Barcelona, etc.) and tried to defy the Venetian rule of the Adriatic Sea. In 1180 the two cities agreed to a non-aggression treaty in the Tyrrhenian and the Adriatic, but the death of Emperor Manuel Comnenus in Constantinople changed the situation.  Soon there were attacks on Venetian convoys. Pisa signed trade and political pacts with Ancona, Pula, Zara, Split and Brindisi: in 1195 a Pisan fleet reached Pola to defend its independence from Venice, but the Serenissima managed soon to reconquer the rebel sea town.One year later the two cities signed a peace treaty which resulted in favourable conditions for Pisa. But in 1199 the Pisans violated it by blockading the port of Brindisi in Puglia. In the following naval battle they were defeated by the Venetians. The war that followed ended in 1206 with a treaty in which Pisa gave up all its hopes to expand in the Adriatic, though it maintained the trading posts it had established in the area. From that point on the two cities were united against the rising power of Genoa and sometimes collaborated to increase the trading benefits in Constantinople.
13th centuryIn 1209 there were in Lerici two councils for a final resolution of the rivalry with Genoa. A twenty-year peace treaty was signed. But when in 1220 the emperor Frederick II confirmed his supremacy over the Tyrrhenian coast from Civitavecchia to Portovenere, the Genoese and Tuscan resentment against Pisa grew again. In the following years Pisa clashed with Lucca in Garfagnana and was defeated by the Florentines at Castel del Bosco. The strong Ghibelline position of Pisa brought this town diametrically against the Pope, who was in a strong dispute with the Empire. And indeed the pope tried to deprive the town of its dominions in northern Sardinia.In 1238 Pope Gregory IX formed an alliance between Genoa and Venice against the empire, and consequently against Pisa too. One year later he excommunicated Frederick II and called for an anti-Empire council to be held in Rome in 1241. On 3 May 1241, a combined fleet of Pisan and Sicilian ships, led by the Emperor's son Enzo, attacked a Genoese convoy carrying prelates from Northern Italy and France, next to the Isola del Giglio, in front of Tuscany: the Genoese lost 25 ships, while about thousand sailors, two cardinals and one bishop were taken prisoner. After this outstanding victory the council in Rome failed, but Pisa was excommunicated. This extreme measure was only removed in 1257. Anyway, the Tuscan city tried to take advantage of the favourable situation to conquer the Corsican city of Aleria and even lay siege to Genoa itself in 1243.The Ligurian republic of Genoa, however, recovered fast from this blow and won back Lerici, conquered by the Pisans some years earlier, in 1256.The great expansion in the Mediterranean and the prominence of the merchant class urged a modification in the city's institutes. The system with consuls was abandoned and in 1230 the new city rulers named a Capitano del Popolo ("People's Chieftain") as civil and military leader. In spite of these reforms, the conquered lands and the city itself were harassed by the rivalry between the two families of Della Gherardesca and Visconti. In 1237 the archbishop and the Emperor Frederick II intervened to reconcile the two rivals, but the strains did not cease. In 1254 the people rebelled and imposed twelve {{lang}} ("People's Elders") as their political representatives in the Commune. They also supplemented the legislative councils, formed of noblemen, with new People's Councils, composed by the main guilds and by the chiefs of the People's Companies. These had the power to ratify the laws of the Major General Council and the Senate.
DeclineThe decline began on 6 August 1284, when the numerically superior fleet of Pisa, under the command of Albertino Morosini, was defeated by the brilliant tactics of the Genoese fleet, under the command of Benedetto Zaccaria and Oberto Doria, in the dramatic naval Battle of Meloria. This defeat ended the maritime power of Pisa and the town never fully recovered: in 1290 the Genoese destroyed forever the Porto Pisano (Pisa's Port), and covered the land with salt. The region around Pisa did not permit the city to recover from the loss of thousands of sailors from the Meloria, while Liguria guaranteed enough sailors to Genoa. Goods continued to be traded, albeit in reduced quantity, but the end came when the River Arno started to change course, preventing the galleys from reaching the city's port up the river. It seems also that nearby area became infested with malaria. Within 1324 also Sardinia was entirely lost in favour of the Aragonese.Always Ghibelline, Pisa tried to build up its power in the course of the 14th century and even managed to defeat Florence in the Battle of Montecatini (1315), under the command of Uguccione della Faggiuola. Eventually, however, divided by internal struggles and weakened by the loss of its mercantile strength, Pisa was conquered by Florence in 1406. In 1409 Pisa was the seat of a council trying to set the question of the Great Schism. Furthermore in the 15th century, access to the sea became more and more difficult, as the port was silting up and was cut off from the sea. When in 1494 Charles VIII of France invaded the Italian states to claim the  Kingdom of Naples, Pisa grabbed the opportunity to reclaim its independence as the Second Pisan Republic.But the new freedom did not last long. After fifteen years of battles and sieges, Pisa was reconquered in 1509 by the Florentine troops led by Antonio da Filicaja, Averardo Salviati and Niccolò Capponi. Its role of major port of Tuscany went to Livorno. Pisa acquired a mainly, though secondary, cultural role spurred by the presence of the University of Pisa, created in 1343. Its decline is clearly shown by its population, which has remained almost constant since the Middle Ages.Pisa was the birthplace of the important early physicist Galileo Galilei. It is still the seat of an archbishopric; it has become a light industrial centre and a railway hub. It suffered repeated destruction during World War II.

Geography
ClimatePisa experiences a Mediterranean climate (Köppen climate classification Csa) characteristic of Central and Southern Italy.
mn
{{Weather box}}
Main sights




While the Leaning Tower is the most famous image of the city, it is one of many works of art and architecture in the city's {{lang}}, also known, since 20th century, as {{lang}} (Square of Miracles), to the north of the old town center. The {{lang}} also houses the {{lang}} (the Cathedral), the Baptistry and the {{lang}} (the monumental cemetery).Other interesting sights include:

Knights' Square ({{lang}}), where the {{lang}}, with its impressive façade designed by Giorgio Vasari may be seen.
In the same place is the church of {{lang}}, also by Vasari. It had originally a single nave; two more were added in the 17th century. It houses a bust by Donatello, and paintings by Vasari, Jacopo Ligozzi, Alessandro Fei, and Jacopo Chimenti da Empoli. It also contains spoils from the many naval battles between the Cavalieri (Knights of St. Stephan) and the Turks between the 16–18th century, including the Turkish battle pennant hoisted from Ali Pacha&#39;s flagship at the 1571 Battle of Lepanto.
Also close to the square is the small church of St. Sixtus. It was formally consecrated in 1133, but previously used as a seat of the most important notarial deeds of the town, also hosting the Council of Elders. It is today one of the best preserved early Romanesque buildings in town.
The church of St. Francis, designed by Giovanni di Simone, built after 1276. In 1343 new chapels were added and the church was elevated. It has a single nave and a notable belfry, as well as a 15th‑century cloister. It houses works by Jacopo da Empoli, Taddeo Gaddi and Santi di Tito. In the Gherardesca Chapel are buried Ugolino della Gherardesca and his sons.
Church of San Frediano, built by 1061, has a basilica interior with three aisles, with a crucifix from the 12th century. 16th century paintings were added during a restoration, including works by Ventura Salimbeni, Domenico Passignano, Aurelio Lomi, and Rutilio Manetti.
Church of San Nicola, built by 1097, was enlarged between 1297 and 1313 by the Augustinians, perhaps by the design of Giovanni Pisano. The octagonal belfry is from the second half of the 13th century. The paintings include the Madonna with Child by Francesco Traini (14th century) and St. Nicholas Saving Pisa from the Plague (15th century). Noteworthy are also the wood sculptures by Giovanni and Nino Pisano, and the Annunciation by Francesco di Valdambrino.
The small church of Santa Maria della Spina, attributed to Lupo di Francesco (1230), is another excellent Gothic building.
The church of San Paolo a Ripa d'Arno, founded around 952 and enlarged in the mid-12th century along lines similar to those of the Cathedral. It is annexed to the Romanesque Chapel of St. Agatha, with an unusual pyramidal cusp or peak.
The {{lang}}, a neighborhood where one can stroll beneath medieval arcades and the Lungarno, the avenues along the river Arno. It includes the Gothic-Romanesque church of San Michele in Borgo (990). Remarkably, there are at least two other leaning towers in the city, one at the southern end of central {{lang}}, the other halfway through the {{lang}} riverside promenade.
The Medici Palace, once a possession of the Appiano family, who ruled Pisa in 1392–1398. In 1400 the Medici acquired it, and Lorenzo de' Medici sojourned here.
The {{lang}} is Europe&#39;s oldest university botanical garden.
The {{lang}} (&#34;Royal Palace&#34;), once of the Caetani patrician family. Here Galileo Galilei showed to Grand Duke of Tuscany the planets he had discovered with his telescope. The edifice was erected in 1559 by Baccio Bandinelli for Cosimo I de Medici, and was later enlarged including other palaces.
{{lang}}, a Gothic building of the 14th century, is now the town hall. The interior shows frescoes boasting Pisa&#39;s sea victories.
{{lang}}, a Gothic building also known as {{lang}}, with its 15th century façade and remains of the ancient city walls dating back to before 1155. The name of the building comes from the coffee rooms of {{lang}}, historic meeting place founded on 1 September 1775.
The mural {{lang}}, the last public work of Keith Haring, on the rear wall of the convent of the Church of Sant'Antonio, painted in June 1989.

Pisa boasts several museums:

{{lang}}: exhibiting among others the original sculptures of Nicola Pisano and Giovanni Pisano and the treasures of the cathedral.
{{lang}}: showing the sinopias from the camposanto, the monumental cemetery. These are red ocher underdrawings for frescoes, made with reddish, greenish or brownish earth colour with water.
{{lang}}: exhibiting sculptures and painting from 12th century–15th century, among them the masterworks of Giovanni and Andrea Pisano, the Master of San Martino, Simone Martini, Nino Pisano and Masaccio.
{{lang}}: exhibiting the belongings of the families that lived in the palace: paintings, statues, armors, etc.
{{lang}}: exhibiting a collection of instruments used in science, between whose a pneumatic machine of Van Musschenbroek and a compass probably belonged to Galileo Galilei.
{{lang}}, located in the Certosa di Calci, outside the city. It houses one of the largest cetacean skeletons collection in Europe.

Pisa hosts the University of Pisa, especially renowned in the fields of Physics, Mathematics, Engineering and Computer Science, the {{lang}} and the {{lang}}, the Italian academic élite institutions, mostly for research and the education of graduate students.Construction of a new leaning tower of glass and steel 57 meters tall, containing offices and apartments was scheduled to start in summer 2004 and take 4 years. It was designed by Dante Oscar Benini and raised criticism.
Educational Institutions


The Sant'Anna School of Advanced Studies of Pisa or Scuola Superiore Sant'Anna is a special-statute public university located in Pisa, Italy, emerging from Scuola Normale Superiore di Pisa and operating in the field of applied sciences,  (Superior Graduate Schools in Italy i.e. Scuola Superiore Universitaria)
Located at: Scuola Superiore Sant'Anna, P.zza Martiri della Libertà, 33 - 56127 - Pisa (Italia) 

The Scuola Normale Superiore di Pisa i.e. Scuola Normale or Scuola Normale Superiore di Pisa, was founded in 1810, by Napoleonic decree, as a branch of the École Normale Supérieure of Paris. Recognized as a &#34;national university&#34; in 1862, one year after Italian unification, and named during that period as &#34;Normal School of the Kingdom of Italy&#34;. (Superior Graduate Schools in Italy i.e. Scuola Superiore Universitaria)
Located at: Scuola Normale Superiore di Pisa - Piazza dei Cavalieri, 7 - 56126 Pisa (Italia) 

The University of Pisa or Università di Pisa, is one of the oldest universities in Italy. It was formally founded on September 3, 1343 by an edict of Pope Clement VI, although there had been lectures on law in Pisa since the 11th century. The University has Europe&#39;s oldest academic botanical garden i.e. Orto botanico di Pisa, founded 1544.
Located at: Università di Pisa - Lungarno Pacinotti, 43 - 56126 Pisa (Italia)
Churches

Baptistry
San Francesco
San Frediano
San Michele in Borgo
San Nicola
San Paolo a Ripa d'Arno
San Paolo all'Orto
San Pietro a Grado
San Pietro in Vinculis
San Sisto
San Zeno
Santa Caterina
Santa Cristina
Santa Maria della Spina
Santo Sepolcro


Palaces, towers and villas

Palazzo del Collegio Puteano
Palazzo della Carovana
Palazzo delle Vedove
Torre dei Gualandi
Villa di Corliano


Notable people associated with PisaFor people born in Pisa, see People from the Province of Pisa; among notable non-natives long resident in the city:

Gaetano Bardini, tenor
Sergio Bertoni, Italian footballer
Andrea Bocelli, tenor
Andrea Buscemi, actor
Giancarlo Ceccarini, baritone
Giorgio Chiellini, Italian footballer
Enrico Fermi and Carlo Rubbia, physicists &#38; Nobel prize winners
Galileo Galilei, physicist
Antonio Pacinotti, physicist, inventor of the dynamo
Andrea Pisano, sculptor
Bruno Pontecorvo, physicist
Leonardo Fibonacci, mathematician
Giosuè Carducci, poet &#38; Nobel prize winner
Antonio Tabucchi, writer
Orazio Gentileschi, painter
Leo Ortolani, comic writer
Afro Poli, baritone
Gillo Pontecorvo, filmmaker
Marcello Rossi, baritone
Titta Ruffo, baritone
Jason Acuña, appears in Jackass
Carlo Azeglio Ciampi and Giovanni Gronchi, politicians, former Presidents of the Republic of Italy
Giuliano Amato, politician, former Premier and Minister of Interior Affairs
Giovanni Gentile, philosopher &#38; politician
Count Ugolino della Gherardesca, noble (see also Dante Alighieri)
Rustichello da Pisa, writer


Transport{{Expand section}}
AirportPisa is home to the Galileo Galilei Airport. The centre can be reached in 10 minutes by city bus — the bus line L.A.M. Rossa (Linea ad Alta Mobilità) connects the airport, the central railway station and Piazza dei Miracoli. Otherwise the centre can be reached in 5 minutes by train from Pisa Airport's railway station.
BusesLocal bus service in Pisa is managed by Compagnia Pisana Trasporti (CPT). Intercity buses depart from the main bus station in Piazza Sant'Antonio. There are also several privately run bus services going from the airport to Florence, Siena and other cities in Tuscany.
TrainsThe city is served by three railway stations: Pisa Centrale, Pisa Aeroporto and Pisa San Rossore.Pisa Centrale is the main railway station and is located along the Tyrrhenic railway line. It connects Pisa directly with several other important Italian cities such as Rome, Genoa, Turin, Naples, Livorno, Grosseto and Florence.Pisa Aeroporto connects the airport to the central railway station, as well as Firenze Santa Maria Novella railway station in the city of Florence. It is located next to the Galileo Galilei International Airport.Pisa San Rossore links the city with Lucca (25 minutes from Pisa) and Pistoia and is also reachable from Pisa Centrale. It is a minor railway station located near the Leaning Tower zone.
CarsPisa has two exits on the A11 Genoa-Livorno road, Pisa Nord and Pisa Centro-aeroporto.Pisa Centro leads visitors to the city centre.
Sports{{Expand section}}
Football is the main sport in Pisa; the local team, A.C. Pisa, currentlyas of June 2011 plays in the Italian Lega Pro Prima Divisione (formerly Serie C1), and has had a top flight history throughout the 1980s and the 1990s, featuring several world class players such as Diego Simeone, Christian Vieri and Dunga during this time.Shooting was one of the first sports to have their own association in Pisa. The Società del Tiro a Segno di Pisa  was founded on July 9, 1862. In 1885, they acquired their own training field. The shooting range was almost completely destroyed during World War II.
Cultural events

Capodanno pisano (25 March)
Gioco del Ponte (folklore)
Luminara di San Ranieri (16 June)
Regate delle Antiche Repubbliche Marinare
Premio Nazionale Letterario Pisa
Pisa Book Festival
Metarock
Internet Festival
Nessiáh (Jewish Cultural Festival, November)


International relations{{See also}}
Twin towns — Sister citiesPisa is twinned with:{{cite web}}



{{flagicon}} Kolding, Denmark
{{flagicon}} Santiago de Compostela 
{{flagicon}} Angers, France, since 1982
{{flagicon}} Acre, Israel, since 1998
{{flagicon}} Jericho, West Bank, since 2000
{{flagicon}} Niles, USA, since 1991
{{flagicon}} Coral Gables, Florida, United States
{{flagicon}} Unna, Germany, since 1996{{cite web}}
{{flagicon}} Cagliari, Italy
{{flagicon}} Ocala, USA http://www.ocalafl.org, United States (Sister City 2004){{cite web}}
 

References

Official Abitants statistics
Pisa Metropolitan Area


Bibliography


Notes{{Reflist}}
External links{{Commons}}

Portale di Pisa e provincia
Official site of the Pisa Tourist Board
{{Wikitravel}}
Official site of the Municipality of Pisa, including webcams
Moving Postcards of Pisa

{{Province of Pisa}}
{{Repubbliche Marinare}}
{{Use dmy dates}}



als:Pisa
ar:بيزا
be:Горад Піза
bcl:Pisa
bg:Пиза
bs:Pisa
br:Pisa (Italia)
ca:Pisa
cs:Pisa
co:Pisa
cy:Pisa
da:Pisa
de:Pisa
et:Pisa
el:Πίζα
eml:Pisa
es:Pisa
eo:Pizo (Italio)
ext:Pisa
eu:Pisa
fa:پیزا
fr:Pise
ga:Pisa
gl:Pisa
ko:피사
hi:पीसा
hr:Pisa
io:Pisa
id:Pisa
ia:Pisa
os:Пизæ
it:Pisa
he:פיזה
jv:Pisa
ka:პიზა
la:Pisae
lv:Piza
lb:Pisa
lt:Piza
lij:Pisa
lmo:Pisa
hu:Pisa
mk:Пиза
mr:पिसा
mn:Пиза
nl:Pisa (stad)
ja:ピサ
nap:Pisa
no:Pisa
nn:Pisa
oc:Pisa
pnb:پیسا
pms:Pisa
nds:Pisa
pl:Piza
pt:Pisa
ro:Pisa
qu:Pisa
ru:Пиза
scn:Pisa
simple:Pisa
sk:Pisa
sl:Pisa
sr:Пиза
sh:Pisa
fi:Pisa
sv:Pisa
tl:Lungsod ng Pisa
roa-tara:Pisa
tt:Пиза
th:ปิซา
tr:Pisa
uk:Піза
ug:Pisa
vec:Pixa
vi:Pisa
vo:Pisa
fiu-vro:Pisa
war:Pisa
zh-yue:比薩
zh:比萨