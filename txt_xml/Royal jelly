titleRoyal jelly is a honey bee secretion that is used in the nutrition of larvae and adult queens.Jung-Hoffmann L: Die Determination von Königin und Arbeiterin der Honigbiene. Z Bienenforsch 1966, 8:296-322. It is secreted from the glands in the hypopharynx of worker bees, and fed to all larvae in the colony.Graham, J. (ed.) (1992) The Hive and the Honey Bee (Revised Edition). Dadant & Sons.When worker bees decide to make a new queen, either because the old one is weakening, or was killed, they choose several small larvae and feed them with copious amounts of royal jelly in specially constructed queen cells. This type of feeding triggers the development of queen morphology, including the fully developed ovaries needed to lay eggs.Maleszka, R, Epigenetic integration of environmental and genomic signals in honey bees: the critical interplay of nutritional, brain and reproductive networks. Epigenetics. 2008, 3, 188-192.
Cultivation
Royal jelly is secreted from the glands in the heads of worker bees, and is fed to all bee larvae, whether they are destined to become drones (males), workers (sterile females) or queens (fertile females). After three days, the drone and worker larvae are no longer fed with royal jelly, but queen larvae continue to be fed this special substance throughout their development. It is harvested by humans by stimulating colonies with movable frame hives to produce queen bees. Royal jelly is collected from each individual queen cell (honeycomb) when the queen larvae are about four days old. It is collected from queen cells because these are the only cells in which large amounts are deposited; when royal jelly is fed to worker larvae, it is fed directly to them, and they consume it as it is produced, while the cells of queen larvae are "stocked" with royal jelly much faster than the larvae can consume it. Therefore, only in queen cells is the harvest of royal jelly practical.
A well-managed hive during a season of 5–6 months can produce approximately 500 g of royal jelly. Since the product is perishable, producers must have immediate access to proper cold storage (e.g., a household refrigerator or freezer) in which the royal jelly is stored until it is sold or conveyed to a collection centre. Sometimes honey or beeswax are added to the royal jelly, which is thought to aid its preservation.
Composition
Royal jelly is collected and sold as a dietary supplement for humans, claiming various health benefits because of components such as B-complex vitamins such as pantothenic acid (vitamin B5) and vitamin B6 (pyridoxine). The overall composition of royal jelly is 67% water, 12.5% crude protein, including small amounts of many different amino acids, and 11% simple sugars (monosaccharides), also including a relatively high amount (5%) of fatty acids. It also contains many trace minerals, some enzymes, antibacterial and antibiotic components, and trace amounts of vitamin C, but none of the fat-soluble vitamins, A, D, E and K.{{cite web}}
RoyalactinThe component of royal jelly that causes a bee to develop into a queen appears to be a single protein that has been called royalactin. Jelly which had been rendered inactive by prolonged storage had a fresh addition of each of the components subject to decay and was fed to bees; only jelly laced with royalactin caused the larvae to become queens.{{Cite doi}} Royalactin also induces similar phenotypical change in the fruitfly (Drosophila melanogaster), marked by increased body size and ovary development.
Epigenetic effects
The honey bee queens and workers represent one of the most striking examples of environmentally controlled phenotypic polymorphism. In spite of their identical clonal nature at the DNA level, they are strongly differentiated across a wide range of characteristics including anatomical and physiological differences, longevity of the queen, and reproductive capacity.Winston, M, The Biology of the Honey Bee, 1987, Harvard University Press Queens constitute the sexual caste and have large active ovaries, whereas workers have only rudimental inactive ovaries and are functionally sterile.  The queen/worker developmental divide is controlled epigenetically by differential feeding with royal jelly; this appears to be due specifically to the protein royalactin. A female larva destined to become a queen is fed large quantities of royal jelly; this triggers a cascade of molecular events resulting in development into a queen.  It has been shown that this phenomenon is mediated by an epigenetic modification of DNA known as CpG methylation.Kucharski R, Maleszka, J, Foret, S, Maleszka, R, Nutritional Control of Reproductive Status in Honeybees via DNA Methylation. Science. 2008 Mar 28;319(5871):1827-3 Silencing the expression of an enzyme that methylates DNA in newly hatched larvae led to a royal jelly-like effect on the larval developmental trajectory; the majority of individuals with reduced DNA methylation levels emerged as queens with fully developed ovaries. This finding suggests that DNA methylation in honey bees allows the expression of epigenetic information to be differentially altered by nutritional input.
Uses
Royal jelly has been reported as a possible immunomodulatory agent in Graves' disease.Erem C, Deger O, Ovali E, Barlak Y. The effects of royal jelly on autoimmunity in Graves' disease. Endocrine. 2006 Oct;30(2):175-83. It has also been reported to stimulate the growth of glial cells{{Cite journal}} and neural stem cells in the brain.{{Cite journal}} To date, there is preliminary evidence that it may have some cholesterol-lowering, anti-inflammatory, wound-healing, and antibiotic effects, though the last three of these effects are unlikely to be realized if ingested (due to the destruction of the substances involved through digestion, or neutralization via changes in pH).PDR Health, Royal Jelly. available online Research also suggests that the 10-Hydroxy-2-decenoic acid (10-HDA) found in royal jelly may inhibit the vascularization of tumors.{{cite web}}  Royal jelly has also been hypothesized to correct cholesterol level imbalances due to nicotine consumption.{{cite web}} There are also some preliminary experiments (on cells and lab animals) in which royal jelly may have some benefit regarding certain other diseases, though there is no solid evidence for those claims, and further experimentation and validation would be needed to prove any useful benefit. In holistic healing circles and popular alternative medicine folklore, royal jelly is believed to have anti aging properties stemming primarily from its amino acid content and broad spectrum of vitamins and minerals. Its reputation for providing fertility and libido assistance is also gaining traction within the holistic community, though little or no clinical research exists to support these assertions.Benefits of Royal Jelly, Royal Jelly. available online Royal jelly is also widely used as a component in skin care and natural beauty products. Royal jelly may cause allergic reactions in humans ranging from hives, asthma, to even fatal anaphylaxis.Leung R, Ho A, Chan J, Choy D, Lai CK. Royal jelly consumption and hypersensitivity in the community. Clin Exp Allergy. 1997 Mar;27(3):333-6. PMID 9088660.Takahama H, Shimazu T. Food-induced anaphylaxis caused by ingestion of royal jelly. J Dermatol. 2006 Jun;33(6):424-6.Lombardi C, Senna GE, Gatti B, Feligioni M, Riva G, Bonadonna P, Dama AR, Canonica GW, Passalacqua G. Allergic reactions to honey and royal jelly and their relationship with sensitization to compositae. Allergol Immunopathol (Madr). 1998 Nov-Dec;26(6):288-90.Thien FC, Leung R, Baldo BA, Weiner JA, Plomley R, Czarny D. Asthma and anaphylaxis induced by royal jelly. Clin Exp Allergy. 1996 Feb;26(2):216-22.Leung R, Thien FC, Baldo B, Czarny D. Royal jelly-induced asthma and anaphylaxis: clinical characteristics and immunologic correlations. J Allergy Clin Immunol. 1995 Dec;96(6 Pt 1):1004-7.Bullock RJ, Rohan A, Straatmans JA. Fatal royal jelly-induced asthma. Med J Aust. 1994 Jan 3;160(1):44. The incidence of allergic side effect in people that consume royal jelly is unknown. However, it has been suggested that the risk of having an allergy to royal jelly is higher in people who already have known allergies.
See also


Bee pollen
Bee propolis


Notes
{{reflist}}
References
{{refbegin}}

{{cite book}}
Ammon, R. and Zoch, E. (1957) Zur Biochemie des Futtersaftes der Bienenkoenigin. Arzneimittel Forschung 7: 699-702
Blum, M.S., Novak A.F. and Taber III, 5. (1959). 10-Hydroxy-decenoic acid, an antibiotic found in royal jelly. Science, 130 : 452-453
Bonomi, A. (1983) Acquisizioni in tema di composizione chimica e di attivita&#39; biologica della pappa reale. Apitalia, 10 (15): 7-13.
Braines, L.N. (1959). Royal jelly I. Inform. Bull. Inst. Pchelovodstva, 31 pp (with various articles)
Braines, L.N. (1960). Royal jelly II. Inform. Bull. Inst. Pchelovodstva, 40 pp.
Braines, L.N. (1962). Royal jelly III. Inform. Bull. Inst. Pchelovodstva, 40
Chauvin, R. and Louveaux, 1. (1956) Etdue macroscopique et microscopique de lagelee royale. L&#39;apiculteur.
Cho, Y.T. (1977). Studies on royal jelly and abnormal cholesterol and triglycerides. Amer. Bee 1., 117 : 36-38
De Belfever, B. (1958) La gelee royale des abeilles. Maloine, Paris.
Destrem, H. (1956) Experimentation de la gelee royale d&#39;abeille en pratique geriatrique (134 cas). Rev. Franc. Geront, 3.
Giordani, G. (1961). [Effect of royal jelly on chickens.] Avicoltura 30 (6): 114-120
Hattori N, Nomoto H, Fukumitsu H, Mishima S, Furukawa S. [Royal jelly and its unique fatty acid, 10-hydroxy-trans-2-decenoic acid, promote neurogenesis by neural stem/progenitor cells in vitro.] Biomed Res. 2007 Oct;28(5):261-6.
Hashimoto M, Kanda M, Ikeno K, Hayashi Y, Nakamura T, Ogawa Y, Fukumitsu H, Nomoto H, Furukawa S. (2005) Oral administration of royal jelly facilitates mRNA expression of glial cell line-derived neurotrophic factor and neurofilament H in the hippocampus of the adult mouse brain. Biosci Biotechnol Biochem. 2005 Apr;69(4):800-5.
Inoue, T. (1986). The use and utilization of royal jelly and the evaluation of the medical efficacy of royal jelly in Japan. Proceeding sof the XXXth International Congress of Apiculture, Nagoya, 1985, Apimondia, 444-447
Jean, E. (1956). A process of royal jelly absorption for its incorporation into assimilable substances. Fr. Pat., 1,118,123
Jacoli, G. (1956) Ricerche sperimentali su alcune proprieta&#39; biologiche della gelatina reale. Apicoltore d&#39;Italia, 23 (9-10): 211-214.
Jung-Hoffmann L: Die Determination von Königin und Arbeiterin der Honigbiene. Z Bienenforsch 1966, 8:296-322.
Karaali, A., Meydanoglu, F. and Eke, D. (1988) Studies on composition, freeze drying and storage of Turkish royal jelly. J. Apic. Res., 27 (3): 182-185.
Kucharski R, Maleszka, J, Foret, S, Maleszka, R, Nutritional Control of Reproductive Status in Honeybees via DNA Methylation. Science. 2008 Mar 28;319(5871):1827-3
Lercker, G., Capella, P., Conte, L.S., Ruini, F. and Giordani, G. (1982) Components of royal jelly: II. The lipid fraction, hydrocarbons and sterolds. J. Apic. Res. 21(3):178-184.
Lercker, G., Vecchi, M.A., Sabatini, A.G. and Nanetti, A. 1984. Controllo chimicoanalitico della gelatina reale. Riv. Merceol. 23 (1): 83-94.
Lercker, G., Savioli, S., Vecchi, M.A., Sabatini, A.G., Nanetti, A. and Piana, L. (1986) Carbohydrate Determination of Royal Jelly by High Resolution Gas Chromatography (HRGC). Food Chemistry, 19: 255-264.
Lercker, G., Caboni, M.F., Vecchi, M.A., Sabatini, A.G. and Nanetti, A. (1992) Caratterizzazione dei principali costituenti della gelatina reale. Apicoltura 8:11-21.
Maleszka, R, Epigenetic integration of environmental and genomic signals in honey bees: the critical interplay of nutritional, brain and reproductive networks. Epigenetics. 2008, 3, 188-192.
Nakamura, T. (1986) Quality standards of royal jelly for medical use. proceedings of the XXXth International Congress of Apiculture, Nagoya, 1985 Apimondia (1986) 462-464.
Rembold, H. (1965) Biological active sustance in royal jelly. Vitamins and hormones 23:359-382.
Salama, A., Mogawer, H.H. and El-Tohamy, M. 1977 Royal jelly a revelation or a fable. Egyptian Journal of Veterinary Science 14 (2): 95-102.
Takenaka, T. Nitrogen components and carboxylic acids of royal jelly. In Chemistry and biology of social insects (edited by Eder, J., Rembold, H.). Munich, German Federal Republic, Verlag J. Papemy (1987): 162-163.
Wagner, H., Dobler, I., Thiem, I. Effect of royal jelly on the peirpheral blood and survival rate of mice after irradiation of the entire body with X-rays. Radiobiologia Radiotherapia (1970) 11(3): 323-328.
Winston, M, The Biology of the Honey Bee, 1987, Harvard University Press
{{refend}}{{Dietary supplement}}{{DEFAULTSORT:Royal Jelly}}
{{Commons category}}

ar:هلام ملكي
az:Arı südü
be:Матачнае малачко
bg:Пчелно млечице
cs:Mateří kašička
da:Gelée royale
de:Gelée Royale
el:Βασιλικός πολτός
es:Jalea real
fa:شاه‌انگبین
fr:Gelée royale
gl:Xelea real
ko:로열 젤리
hr:Matična mliječ
it:Pappa reale
he:מזון מלכות
lt:Bičių pienelis
nl:Koninginnengelei
ja:ローヤルゼリー
no:Dronninggelé
pl:Mleczko pszczele
pt:Geleia real
ro:Lăptișor de matcă
ru:Маточное молочко
sl:Matični mleček
sr:Матични млеч
sh:Matična mliječ
sv:Bidrottninggelé
tr:Arı sütü
uk:Маточне молочко
vi:Sữa ong chúa
zh:蜂王漿