title


Germany is one of the world's top photovoltaics (PV) installers, with a solar PV capacity as of 2011 of almost 25&nbsp;gigawatts (GW). The German solar PV industry installed about 7.5&nbsp;GW in 2011,{{cite web}} and solar PV provided 18&nbsp;TW·h (billion kilowatt-hours) of electricity in 2011, about 3% of total electricity.{{cite web}} Some market analysts expect this could reach 25&nbsp;percent by 2050.Another Sunny Year for Solar PowerLarge PV power plants in Germany include Senftenberg Solarpark, Finsterwalde Solar Park, Lieberose Photovoltaic Park, Strasskirchen Solar Park, Waldpolenz Solar Park, and Köthen Solar Park. 
OverviewThe German solar PV industry installed 7.5&nbsp;GW in 2011, and solar PV provided 18&nbsp;TW·h of electricity in 2011, about 3% of total electricity. Price of PV systems has decreased more than 50% in 5&nbsp;years since 2006. Solar power in Germany has been growing considerably due to the country's feed-in tariffs for renewable energy which were introduced by the German Renewable Energy Act. {{As of}}, the FiT costs about 14&nbsp;billion euros (US$18&nbsp;billion) per year to subsidize new wind and solar installations. The cost is spread across all rate-payers in a surcharge of 3.6&nbsp;€ct (4.6&nbsp;¢) per kW·h.{{cite web}}The German government has set a target of 66 GW of installed solar PV capacity by 2030,{{cite web}}
to be reached with an annual increase of 2.5–3.5&nbsp;GW.{{cite web}} 
StatisticsIncreases in installed solar PV power capacity and generation in recent years is shown in the table below:{{cite web}} 

Year  1990  1991  1992  1993  1994  1995  1996  1997  1998  1999  2000  
Capacity (MW) 0.6  2.0  3.0  5.0  6.0  8.0  11  18  23  32  76  
Generation (GW·h) 0.6  1.6  3.2  5.8  8.0  11  16  26  32  42  64  
% of total electricity consumption &#60;0.001  &#60;0.001  0.001  0.001  0.002  0.002  0.003  0.005  0.006  0.008  0.01  
 
Year 2000  2001  2002  2003  2004  2005  2006  2007  2008  2009  2010  
Capacity (MW) 76  186  296  435  1,105  2,056  2,899  4,170  6,120  9,914  17,320 
Generation (GW·h) 64  76  162  313  556  1,282  2,220  3,075  4,420  6,578  12,000 
% of total electricity consumption 0.01  0.01  0.03  0.05  0.09  0.2  0.4  0.5  0.7  1.1  2.0 
 
Year 2010  2011  2012  2013  2014  2015  2016  2017  2018  2019  2020  
Capacity (GW) 17.3  24.8           
Generation (TW·h) 12  18           
% of total electricity consumption 2.0  3.2           

Photovoltaic power stations{{main}}
{{-}}

Largest German photovoltaic power stations (20 MW or larger)PV Resources.com (2009). World's largest photovoltaic power plants
PV Power station  Nominal PowerNote that nominal power may be AC or DC, depending on the plant. See AC-DC conundrum: Latest PV power-plant ratings follies put focus on reporting inconsistency (update) (MWp)  Production(Annual  GW·h)  Capacityfactor  Notes 
Finsterwalde Solar Park  80.7    Phase I completed 2009,  phase II and III 2010 Good Energies, NIBC Infrastructure Partners acquire Finsterwalde II and Finsterwalde IIIImplementation of the 39 MWp – „Solar Park Finsterwalde II and Finsterwalde III“ 
Senftenberg Solar ParkSolarServer: 78 MW of the world’s largest solar photovoltaic plant connected to grid in Senftenberg, Germany 78    Phase II and III completed 2011, another 70 MW phase planned 
Strasskirchen Solar Park  54  57  0.12   
Lieberose Photovoltaic Park  53  53  0.11  2009 Germany Turns On World's Biggest Solar Power ProjectLieberose solar farm becomes Germany's biggest, World's second-biggest 
Kothen Solar Park  45    2009 
Waldpolenz Solar Park  40   0.11  550,000 First Solar thin-film CdTe modules. Completed December 2008 Large photovoltaic plant in MuldentalkreisGermany's largest Solar parks connected to the grid (19 Dec 08) 
Reckahn Solar Park  36     
Tutow Solar Park  31    Completed in 2010 
Helmeringen Solar Park  25.7    Completed in 2010 
Finow Solar Park  24.5    Completed in 2010 
Pocking Solar Park  22     
Mengkofen Solar Park  21.7     
Rothenburg Solar Park  20     


Other notable photovoltaic (PV) power plantsWorld's largest photovoltaic power plants
DC Peak Power
 Location
 Description
 MW·h/year
 Capacity factor
 Coordinates
 
12 MW  Arnstein  1408 SOLON mover  (see Erlasee Solar Park) 
 14,000 MW·h  0.13  {{coord}}
 
  8.4 MW   Gottelborn Solar Park     
  6.3 MW  Mühlhausen  57,600 solar modules(see Bavaria Solarpark) 
 6,750 MW·h  0.12  {{coord}}
 
  6 MW   Rote Jahne Solar ParkConstruction Complete on 6 MW Thin-Film PV Installation in Germany Renewable Energy Access, 5 April 2007.
    
  5 MW  Bürstadt  30,000 BP Solar modules 
 4,200 MW·h  0.10  {{coord}}
 
  5 MW  Espenhain  33,500 Shell Solar modules 
 5,000 MW·h  0.11  {{coord}}
 
  4 MW  Merseburg  25,000 BP solar modules(see Geiseltalsee Solarpark) 
 3,400 MW·h  0.10  {{coord}}
 
  4 MW  Hemau  32,740 solar modules  3,900 MW·h  0.11  {{coord}}
 
  3.3 MW  Dingolfing  Solara, Sharp and Kyocera solar modules 
 3,050 MW·h  0.11  {{coord}}
 
  1.9 MW  Guenching   Sharp solar modules(see Bavaria Solarpark) 
 -   {{coord}}
 
  1.9 MW  Minihof  Sharp solar modules(see Bavaria Solarpark) 
 -   n.a.
 

Companies
Major German solar companies include:
{{div col}}

Aleo Solar
Bosch Solar Energy
Centrosolar
Centrotherm Photovoltaics
Concentrix Solar
Conergy
Flabeg AG
Gehrlicher Solar AG
IBC SOLAR
Juwi
Odersun
Phoenix Solar
Q-Cells
Roth & Rau
Schott AG
Singulus Technologies
SMA Solar Technology
Solar Millennium
SolarWorld
Solon SE
Sulfurcell
SunPower
{{div col end}}
See also{{Portal box}}

Renewable energy in Germany
Solar power in the European Union
German Solar Industry Association


References{{reflist}}
External links

Cloudy Germany a Powerhouse in Solar Energy, Washington Post, 2007
Southern Germany develops its PV Capacities
Cloudy Germany unlikely hotspot for solar power
Germany's sunny revolution
World's Biggest Solar Plant Goes Online in Germany
Official site about solar power and renewable Energy in the Emscher-Lippe-Region (German)
{{cite web}} 
{{cite web}}

 {{Solar power in Europe}}
{{Renewable energy by country}}
