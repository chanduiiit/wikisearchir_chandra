title{{stack}}A sphygmomanometer ({{IPAc-en}} {{respell}}) or blood pressure meter (also referred to as a sphygmometer) is a device used to measure blood pressure, composed of an inflatable cuff to restrict blood flow, and a mercury or mechanical manometer to measure the pressure. It is always used in conjunction with a means to determine at what pressure blood flow is just starting, and at what pressure it is unimpeded. Manual sphygmomanometers are used in conjunction with a stethoscope.The word comes from the Greek sphygmós (pulse), plus the scientific term manometer (pressure meter). The device was invented by Samuel Siegfried Karl Ritter von Basch in 1881.{{cite journal}} Scipione Riva-Rocci introduced a more easily used version in 1896. In 1901, Harvey Cushing modernized the device and popularized it within the medical community.A sphygmomanometer consists of an inflatable cuff, a measuring unit (the mercury manometer, or aneroid gauge), and inflation bulb and valve, for manual instruments.
Operation
In humans, the cuff is normally placed smoothly and snugly around an upper arm, at roughly the same vertical height as the heart while the subject is seated with the arm supported. Other sites of placement depend on species, it may include the flipper or tail. It is essential that the correct size of cuff is selected for the patient. Too small a cuff results in too high a pressure, while too large a cuff results in too low a pressure. For clinical measurements, it is imperative to measure both arms in the initial consultation, to determine if the pressure is significantly higher in one arm than the other. A difference of 10 mm Hg may be a sign of coarctation of the aorta. Records notes should be made as to which arm measures the highest. Afterwards, measure the higher reading arm. The cuff is inflated until the artery is completely occluded. Listening with a stethoscope to the brachial artery at the elbow, the examiner slowly releases the pressure in the cuff. As the pressure in the cuffs falls, a "whooshing" or pounding sound is heard (see Korotkoff sounds) when blood flow first starts again in the artery. The pressure at which this sound began is noted and recorded as the systolic blood pressure. The cuff pressure is further released until the sound can no longer be heard. This is recorded as the diastolic blood pressure. In noisy environments where auscultation is impossible (such as the scenes often encountered in emergency medicine), systolic blood pressure alone may be read by releasing the pressure until a radial pulse is palpated (felt). In veterinary medicine, auscultation is rarely of use, and palpation or visualization of pulse distal to the sphygmomanometer is used to detect systolic pressure.
Significance
By observing the mercury in the column while releasing the air pressure with a control valve, one can read the values of the blood pressure in mm Hg. The peak pressure in the arteries during the cardiac cycle is the systolic pressure, and the lowest pressure (at the resting phase of the cardiac cycle) is the diastolic pressure. A stethoscope is used in the auscultatory method. Systolic pressure (first phase) is identified with the first of the continuous Korotkoff sounds. Diastolic is identified at the moment the Korotkoff sounds disappear (fifth phase).Measurement of the blood pressure is carried out in the diagnosis and treatment of hypertension (high blood pressure), and in many other healthcare scenarios.
Types
There are three types of sphygmomanometers:

Digital with manual or automatic inflation. These are electronic, easy to operate, and practical in noisy environments. They measure mean arterial pressure (MAP) and use oscillometric detection to calculate systolic and diastolic values. In this sense, they do not actually measure the blood pressure, but rather derive the readings. Digital oscillometric monitors are also confronted with &#34;special conditions&#34; for which they are not designed to be used: arteriosclerosis; arrhythmia; preeclampsia; pulsus alternans; and pulsus paradoxus.
Digital portable finger blood pressure monitors with automatic inflation. These are more portable and easy to operate, although less accurate. They are the smallest blood pressure monitors.{{Citation needed}}
Manual. Ideally operated by a trained person, mercury manometers are considered to be the gold standard and cannot be decalibrated , they are consistently accurate. Due to their accuracy, they are often required in clinical trials of pharmaceuticals and for clinical evaluations of determining blood pressure for high risk patients including pregnant women.  Aneroid (mechanical types with a dial) are in common use but they require regular calibration checks, unlike a mercury manometer. The prime reason for such maintenance is their susceptibility to bumps which can alter their accuracy. Wall mounted and mobile aneroids avoid this shortcoming. The aneroid sphygmomanometer should be checked for accuracy (usually every 6 months) by using a mercury manometer, as the gold standard. The unit of measurement of blood pressure is millimeters of mercury (mmHg) and are usually given as an even number.{{Citation needed}} Manual sphygmomanometers require a stethoscope for auscultation. Although it is possible to obtain a basic reading through palpation, this only yields the systolic number.


See also

Seymour London - inventor of the first automated heart monitor


References{{reflist}}
External links{{Commons category}}

{{US patent reference}}
{{US patent reference}}

{{Health care}}


ar:مقياس ضغط الدم
bg:Сфигмоманометър
ca:Esfigmomanòmetre
de:Blutdruckmessgerät
es:Esfigmomanómetro
fa:فشارسنج خون
fr:Tensiomètre
hi:रक्तचापमापी
id:Sfigmomanometer
it:Sfigmomanometro
he:מד לחץ דם
nl:Bloeddrukmeter
ja:血圧計
pms:Sfigmomanòmeter
pl:Sfigmomanometr
pt:Esfigmomanômetro
ru:Сфигмоманометр
sl:Sfigmomanometer
fi:Verenpainemittari
tl:Espigmomanometro
tr:Tansiyon aleti
uk:Сфігмоманометр
vi:Máy đo huyết áp
zh:血压计