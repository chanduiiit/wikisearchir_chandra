title
Three Crosses Square ({{lang-pl}}, {{IPAc-pl}}, also Square of Three Crosses, Three Cross Square and Triple Cross Square) is an important square in the central district of Warsaw, Poland.  It lies on that city's Royal Route and links Nowy Świat (New World) Street, to the north, with Ujazdów Avenue to the south.  Much of the square's area is devoted to a major thoroughfare.
HistoryUntil the 18th century, the area now occupied by the square was little more than sparsely-populated open terrain south of the then-city limits of Warsaw.During the reign of King Augustus II the Strong, between 1724 and 1731, a "road to Calvary" (Stations of the Cross) was created, with the first station being located near the present square, and the last station next to Ujazdów Castle to the south. The first station featured two golden crosses.In 1752 Grand Marshal of the Crown Franciszek Bieliński erected nearby a statue of St. John of Nepomuk, likewise holding a cross. On account of the three crosses, the populace took to calling the area "Rozdroże złotych krzyży"—"the Crossroads of the Golden Crosses". In the Square stands the neoclassist St. Alexander's Church, designed 1818–25 by the Polish architect Chrystian Piotr Aigner."Aigner, Chrystian Piotr," Encyklopedia Polski, p. 12."Aigner, Chrystian Piotr," Encyklopedia Powszechna PWN (PWN Universal Encyclopedia), volume 1, p. 32. Originally the square itself likewise bore the name of Saint Alexander.The square takes its present name from three crosses: one atop St. Alexander's Church, and two atop columns several dozen meters away, facing the church's entrance. Contrary to common belief, there are more than three crosses in the square. Apart from the three mentioned above, there are two atop the northern and southern façades of the church, as well as one before the Institute for the Deaf (which was erected in 1827 and initially run by the Catholic Church).During the 1944 Warsaw Uprising, the square and most of the surrounding buildings were destroyed or deliberately demolished by the Germans. After 1945 the Institute for the Deaf was rebuilt, and the church was rebuilt to its pre-19th-century appearance.The square now hosts exclusive retail stores — Hugo Boss, Burberry, Church's, Ermenegildo Zegna, Max Mara, Coccinelle, W. Kruk, JM Weston, Franscesco Biasa, Escada, MAX & Co., Lacoste, Emporio Armani and Kenzo.  A DKNY shop is expected to open near the Square.Adjacent to the square is the seat of the Warsaw Stock Exchange, an HSBC Premiere office, and a Sheraton Hotel.
See also

Bolesław Prus


Notes{{reflist}}
References{{commons category}}

{{pl icon}} T. Jaroszewski, Chrystian Piotr Aigner, architekt warszawskiego klasycyzmu (Chrystian Piotr Aigner: Architect of Warsaw Classicism), Warsaw, 1970.
{{pl icon}} {{cite encyclopedia}}
{{pl icon}} {{cite encyclopedia}}

{{coord}}de:Plac Trzech Krzyży
es:Plaza de las Tres Cruces
pl:Plac Trzech Krzyży w Warszawie
zh:三十字广场